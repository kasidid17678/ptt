package com.app.ptt.notification.utils;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.app.ptt.notification.exception.ServiceException;

@Component
public class FileUtils {
	
	private static Logger logger = LogManager.getLogger(FileUtils.class);
	
	public void directoryExists(String directory){
	    File dir = new File(directory);
		if (! dir.exists()){
			dir.mkdirs();
		}
	}
	
	public Boolean fileExists(String filePath){
	    File dir = new File(filePath);
		return dir.exists();
	}
	
	public static void deleteDirectory(Path directory) throws IOException
	{
		if (Files.exists(directory))
		{
			Files.walkFileTree(directory, new SimpleFileVisitor<Path>()
			{
				@Override
				public FileVisitResult visitFile(Path path, BasicFileAttributes attr)
						throws IOException
				{
					Files.delete(path);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path path, IOException ex)
						throws IOException
				{
					Files.delete(path);
					return FileVisitResult.CONTINUE;
				}
			});
		}else {
			logger.info("directory not found");
		}
	}

    public String generateFileName(String prefix) {
		String fileName = "";
		try {

			Random random = new Random();
			int randDomInt = random.nextInt(900) + 100;
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");  
			LocalDateTime now = LocalDateTime.now();
			fileName = prefix + "_" + randDomInt + "_" + dtf.format(now);
			
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return fileName;
	}
}
