package com.app.ptt.notification.utils;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.app.ptt.notification.constants.Constants;


public class LatteUtil {
	private static Logger logger = LogManager.getLogger(LatteUtil.class);

	public static String dateThai(String strDate){
		String Months[] = {
		"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
		"พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
		"กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};
	
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int year=0,month=0,day=0;
		
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);
		
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);

		} catch (ParseException e) {
			e.printStackTrace();
			
		}

		return String.format("%s %s %s", day,Months[month],year);
	}
	
	//Check NULL and Empty String
	public static boolean isEmptyString(String a) {
		boolean b = false;
		try {
			if(null == a) {
				return true;
			}else {
				if(a.isEmpty()) {
					return true;
				}
			}
		}catch (Exception e) {
			logger.info(e.getMessage());
		}
		return b;
	}
	
	
	public static boolean isEmpty(String a) {
		boolean b = false;
		try {
			if(null == a) {
				return true;
			}else {
				if(a.isEmpty()) {
					return true;
				}
			}
		}catch (Exception e) {
			logger.info(e.getMessage());
		}
		return b;
	}
	
	public static boolean isEmpty(List<?> list) {
		boolean b = false;
		try {
			if(null == list) {
				return true;
			}else {
				if(list.isEmpty()) {
					return true;
				}
			}
		}catch (Exception e) {
			logger.info(e.getMessage());
		}
		return b;
	}
	
	public static boolean isEmpty(Object o) {
		boolean b = false;
		try {
			if(null == o) {
				return true;
			}
		}catch (Exception e) {
			logger.info(e.getMessage());
		}
		return b;
	}
	public static String setImageUrl(String imageName,int shopId) {
		try {
			
			StringBuilder fileDownloadUri = new StringBuilder();
			fileDownloadUri.append(Constants.SERVER_URL);
			fileDownloadUri.append(Constants.DOWNLOAD_FILE_CONTROLLER);
			fileDownloadUri.append(Constants.PATH_SHOP+"/");
			fileDownloadUri.append(shopId+"/");
			fileDownloadUri.append(imageName);
			
			return fileDownloadUri.toString();
		}catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}
	}
	
	public static String setQRImageUrl(String imageName,int shopId) {
		try {
			
			StringBuilder fileDownloadUri = new StringBuilder();
			fileDownloadUri.append(Constants.SERVER_URL);
			fileDownloadUri.append(Constants.DOWNLOAD_FILE_CONTROLLER);
			fileDownloadUri.append(Constants.PATH_QR+"/");
			fileDownloadUri.append(shopId+"/");
			fileDownloadUri.append(imageName);
			
			return fileDownloadUri.toString();
		}catch (Exception e) {
			logger.info(e.getMessage());
			return null;
		}
	}
	
	public static void setErrorToClass(int code , String message, String messageStatus ,Object obj) {
		 try {
			 Method setCode = obj.getClass().getMethod("setCode",int.class );
			 Method setMessage = obj.getClass().getMethod("setMessage", String.class);
			 Method setMessageStatus = obj.getClass().getMethod("setMessageStatus",String.class);
			 setCode.invoke(obj, code);
			 setMessage.invoke(obj, message);
			 setMessageStatus.invoke(obj, messageStatus);
		 }catch (Exception e) {
			 logger.info(e.getMessage());
		}
	}
	public static void merge(Object obj, Object update){
	    if(!obj.getClass().isAssignableFrom(update.getClass())){
	        return;
	    }

	    Method[] methods = obj.getClass().getMethods();

	    for(Method fromMethod: methods){
	        if(fromMethod.getDeclaringClass().equals(obj.getClass())
	                && fromMethod.getName().startsWith("get")){

	            String fromName = fromMethod.getName();
	            String toName = fromName.replace("get", "set");

	            try {
	                Method toMetod = obj.getClass().getMethod(toName, fromMethod.getReturnType());
	                Object value = fromMethod.invoke(update, (Object[])null);
	                if(value != null){
	                    toMetod.invoke(obj, value);
	                }
	            } catch (Exception e) {
	                logger.info(e.getMessage());
	            } 
	        }
	    }
	}
	
	//GENERATE RANDOM NUMBER FORFILE//
	public static String generateFileName(String prefix,int key) {
		String fileName = "";
		try {
			Random random = new Random();
			int randDomInt = random.nextInt(900) + 100;
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");  
			LocalDateTime now = LocalDateTime.now();
			if(0==key) 
				fileName = prefix+"_" + randDomInt + "_" + dtf.format(now);
			else
				fileName = prefix+key+"_" + randDomInt + "_" + dtf.format(now);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}

	 public static String getAgeByYear(String dob) throws ParseException{
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	      Date date = formatter.parse(dob);
	      //Converting obtained Date object to LocalDate object
	      Instant instant = date.toInstant();
	      ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
	      LocalDate givenDate = zone.toLocalDate();
	      //Calculating the difference between given date to current date.
	      Period period = Period.between(givenDate, LocalDate.now());
	      int age = period.getYears();
	      if(period.getMonths()>0 || period.getDays()>0) {
	    	  age++;
	      }
	     
	      return String.valueOf(age);
	      
	   }
	 
	 public static String getAgeByDate(String dob) throws ParseException{
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	      Date date = formatter.parse(dob);
	      //Converting obtained Date object to LocalDate object
	      Instant instant = date.toInstant();
	      ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
	      LocalDate givenDate = zone.toLocalDate();
	      //Calculating the difference between given date to current date.
	      Period period = Period.between(givenDate, LocalDate.now());

	      return String.valueOf(period.getYears());
	      
	   }

	
}
