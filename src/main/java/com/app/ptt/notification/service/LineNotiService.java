package com.app.ptt.notification.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.dao.LineNotiDAO;
import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.CampaignPointModel;
import com.app.ptt.notification.model.CustomerTransLogModel;
import com.app.ptt.notification.model.LineEventModel;
import com.app.ptt.notification.model.LineMessageModel;
import com.app.ptt.notification.model.LineNotiMessageModel;
import com.app.ptt.notification.model.NotificationModel;
import com.app.ptt.notification.model.PromotionModel;
import com.app.ptt.notification.model.ShopLineNotiModel;
import com.app.ptt.notification.model.ShopModel;
import com.app.ptt.notification.model.UserProfileModel;
import com.app.ptt.notification.model.lineRequestModel;
import com.app.ptt.notification.utils.LatteUtil;

@Service
public class LineNotiService {

	private static Logger logger = LogManager.getLogger(LineNotiService.class);

	@Autowired
	LineNotiDAO lineNotiDAO;

	public ShopLineNotiModel sendMessage(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);

			sendPost(lineNotiMessageModel, model.getToken());
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public static void sendPost(LineNotiMessageModel messageModel, String token) throws IOException { // LINE Notify
		// krzeVMlo7EJPR32sHlmWh9SF1TycK3REcyF6fkpnd6W test gen by pea'line
		logger.info("---------------sendPost line Noti------------------");

		String lineNoti = "https://notify-api.line.me/api/notify";
//		String urlParameters = "message=" + messageModel.getMessage();
		String urlParameters = "message=" + "tESTNotiTcheckRace";
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		URL obj = new URL(lineNoti);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestProperty("Authorization", "Bearer " + token);// line token generate
		// e.g. bearer token=
		// aXGgRLYKA0Gpyg8CGnovXqqq7qjmn3kpieaN3HgXsvV
		con.setDoOutput(true);
		con.setInstanceFollowRedirects(false);
		con.setUseCaches(false);
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		con.setRequestMethod("POST");
//		System.out.println(con);
		try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
			wr.write(postData);
		}
		int responseCode = con.getResponseCode();
		// System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			// print result
			logger.info(response.toString());
		} else {
			logger.info(responseCode);
		}
	}

	public ShopLineNotiModel registerNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();
		UserProfileModel userProfileModel = new UserProfileModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);
			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
				if (model.getCustomerProfileId() != 0) {
					userProfileModel = lineNotiDAO.getUserProfile(model.getCustomerProfileId());
					String Template = "";
					Template = lineNotiMessageModel.getMessage();
					model.setStatus("success");
					String db_string = new String(Template);

					db_string = db_string.replace("\\n", "\n");

					db_string = String.format(db_string, model.getShopName(), userProfileModel.getFirstName(),
							userProfileModel.getLastName(), "xxx"// point from register

					);
					lineNotiMessageModel.setMessage(db_string);
					sendPost(lineNotiMessageModel, model.getToken());
				}
			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public ShopLineNotiModel promotionNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();
		try {

//			lineNotiMessageModel = lineNotiDAO.getMessage(model);
//			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
//				if (model.getCustomerTransLogId() != 0) {
//					userProfileModel = lineNotiDAO.getUserProfile(model.getCustomerProfileId());
//					customerTransLogModel = lineNotiDAO.getPromotionDetail(model.getCustomerTransLogId());
//					model.setStatus("success");
//					String Template = "";
//					Template = lineNotiMessageModel.getMessage();
//					// แจ้งเตือนจากระบบ!\nคุณ %1$s %2$s\nเบอร์โทร %3$s\nได้ทำการใช้โปรโมชั่น
//					// %4$s\nรหัสการใช้งาน %5$s
//					String db_string = new String(Template);
//
//					db_string = db_string.replace("\\n", "\n");
//
//					db_string = String.format(db_string, customerTransLogModel.getPromotionName(),
//							userProfileModel.getFirstName(), userProfileModel.getLastName(),
//							customerTransLogModel.getPoint());
//					lineNotiMessageModel.setMessage(db_string);
					sendPost(lineNotiMessageModel, model.getToken());
//				}

//			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public ShopLineNotiModel campaignNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();
		UserProfileModel userProfileModel = new UserProfileModel();
		CustomerTransLogModel customerTransLogModel = new CustomerTransLogModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);
			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
				if (model.getCustomerTransLogId() != 0) {
					userProfileModel = lineNotiDAO.getUserProfile(model.getCustomerProfileId());
					customerTransLogModel = lineNotiDAO.getCampaignDetail(model.getCustomerTransLogId());
					model.setStatus("success");
					String Template = "";
					Template = lineNotiMessageModel.getMessage();
//				แจ้งเตือนจากระบบ!\nคุณ %1$s %2$s\nเบอร์โทร %3$s\nได้เพิ่ม Point โดย  %4$s \nจำนวน Point ที่ได้รับ %5$s\nรหัสการใช้งาน %6$s
					String db_string = new String(Template);

					db_string = db_string.replace("\\n", "\n");

					db_string = String.format(db_string, userProfileModel.getShopName(),
							userProfileModel.getFirstName(), userProfileModel.getLastName(),
//						customerTransLogModel.getCampaignName(),
							customerTransLogModel.getPointDeposit());
					lineNotiMessageModel.setMessage(db_string);
					sendPost(lineNotiMessageModel, model.getToken());
				}

			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public ShopLineNotiModel newShopNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();

		ShopModel shopModel = new ShopModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);
			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
				if (model.getShopId() != 0) {

					shopModel = lineNotiDAO.getShopInfo(model.getShopId());

					model.setStatus("success");
					String Template = "";
					Template = lineNotiMessageModel.getMessage();
//				แจ้งเตือนจากระบบ!\nร้าน  %1$s \n ได้เปิดตัวแล้ว
					String db_string = new String(Template);

					db_string = db_string.replace("\\n", "\n");

					db_string = String.format(db_string, shopModel.getShopName());

					lineNotiMessageModel.setMessage(db_string);
					sendPostWithImage(lineNotiMessageModel, model.getToken(),
							LatteUtil.setImageUrl(shopModel.getShopIcon(), model.getShopId()));
					lineNotiMessageModel.setMessage("QR Code");
					sendPostWithImage(lineNotiMessageModel, model.getToken(),
							LatteUtil.setQRImageUrl(shopModel.getQrImage(), model.getShopId()));
				}

			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public static void sendPostWithImage(LineNotiMessageModel messageModel, String token, String imgPath)
			throws IOException { // LINE Notify
		// krzeVMlo7EJPR32sHlmWh9SF1TycK3REcyF6fkpnd6W test gen by pea'line
		logger.info("---------------sendPost line Noti------------------");

//		String imgPath = "https://codecasterdesign.com/fileController/downloadFile/SHOP/1/SHO11_456_20200831140151.jpg";
		String lineNoti = "https://notify-api.line.me/api/notify";
		String urlParameters = "message=" + messageModel.getMessage();
		String imageParameter = "imageThumbnail=" + imgPath + "&imageFullsize=" + imgPath;
		urlParameters = urlParameters + "&" + imageParameter;
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

		int postDataLength = postData.length;
		URL obj = new URL(lineNoti);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestProperty("Authorization", "Bearer " + token);// line token generate
		// e.g. bearer token=
		// eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T
		con.setDoOutput(true);
		con.setInstanceFollowRedirects(false);
		con.setUseCaches(false);
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		con.setRequestMethod("POST");
//		System.out.println(con);
		try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
			wr.write(postData);
		}
		int responseCode = con.getResponseCode();

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			// print result
			logger.info(response.toString());
		} else {
			logger.info(responseCode);
		}
	}

	public ShopLineNotiModel newPromotionNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();

		PromotionModel proModel = new PromotionModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);
			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
				if (model.getPromotionId() != 0) {

					proModel = lineNotiDAO.getPromotionInfo(model.getPromotionId());

					model.setStatus("success");
					String Template = "";
					Template = lineNotiMessageModel.getMessage();
//				แจ้งเตือนจากระบบ!\nร้าน  %1$s \n ได้เพิ่ม Promotion %2$s แล้ว
					String db_string = new String(Template);

					db_string = db_string.replace("\\n", "\n");

					db_string = String.format(db_string, "Promotion_Creator_ID", proModel.getPromotionName(),
							proModel.getPoint(), proModel.getStartDate(), proModel.getEndDate());

					lineNotiMessageModel.setMessage(db_string);
					sendPostWithImage(lineNotiMessageModel, model.getToken(),
							LatteUtil.setQRImageUrl(proModel.getQrImage(), model.getShopId()));
				}

			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public ShopLineNotiModel newCampaignNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();

		CampaignPointModel camModel = new CampaignPointModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);
			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
				if (model.getCampaignPointId() != 0) {

					camModel = lineNotiDAO.getCampaignInfo(model.getCampaignPointId());

					model.setStatus("success");
					String Template = "";
					Template = lineNotiMessageModel.getMessage();
//				แจ้งเตือนจากระบบ!\nร้าน  %1$s \n ได้เพิ่ม Campaign %2$s แล้ว
					String db_string = new String(Template);

					db_string = db_string.replace("\\n", "\n");

					db_string = String.format(db_string, "[Creator_ID]", camModel.getPoint());

					lineNotiMessageModel.setMessage(db_string);
					sendPostWithImage(lineNotiMessageModel, model.getToken(),
							LatteUtil.setQRImageUrl(camModel.getQrImage(), model.getShopId()));
				}

			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public ShopLineNotiModel editUserNoti(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();

		UserProfileModel userModel = new UserProfileModel();
		try {

			lineNotiMessageModel = lineNotiDAO.getMessage(model);
			if (lineNotiMessageModel.getLineNotiStatus().equals("ACTIVE")) {
				if (model.getCustomerProfileId() != 0) {

					userModel = lineNotiDAO.getUserProfile(model.getCustomerProfileId());

					model.setStatus("success");
					String Template = "";
					Template = lineNotiMessageModel.getMessage();
//				แจ้งเตือนจากระบบ!\nร้าน  %1$s \n ได้เพิ่ม Campaign %2$s แล้ว
					String db_string = new String(Template);

					db_string = db_string.replace("\\n", "\n");

					db_string = String.format(db_string, userModel.getFirstName(), userModel.getLastName(),
							userModel.getUpdateBy());

					lineNotiMessageModel.setMessage(db_string);
					sendPost(lineNotiMessageModel, model.getToken());
				}

			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		return model;
	}

	public void lineBotNoti(NotificationModel model) throws IOException {
		// krzeVMlo7EJPR32sHlmWh9SF1TycK3REcyF6fkpnd6W test gen by pea'line
		logger.info("---------------sendPost line Noti------------------");

		String lineNoti = "https://api.line.me/v2/bot/message/reply";
		LineEventModel returnJson = new LineEventModel();
		List<LineMessageModel> listMessage = new ArrayList<LineMessageModel>();
		returnJson.setReplyToken(model.getEvents().get(0).getReplyToken());
		LineMessageModel text = new LineMessageModel();
		text.setText("tesst");
		text.setType("text");
		listMessage.add(text);

		returnJson.setMessages(listMessage);

		byte[] input = new Gson().toJson(returnJson).toString().getBytes("utf-8");
		URL obj = new URL(lineNoti);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestProperty("Authorization", "Bearer " + Constants.LINE_BOT_TOKEN);// line token generate
		// e.g. bearer token=
		// fjUSckNvRwiIv83U1wmQDU/uWB2KZcDrrorhqSVjQz2bmYfEsSxckSEv7THCdcpRKGg1oavJVm9PNBDD1ZPkfru2Xwjh6IEyU6RkI+OQ0BTvHPBm/eNqn/Nw9mLLigkXX/VXGazeUidqCKy/nfACnQdB04t89/1O/w1cDnyilFU=
		con.setDoOutput(true);
		con.setInstanceFollowRedirects(false);
		con.setUseCaches(false);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");

		con.setRequestMethod("POST");

		try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
			wr.write(input);
		}
		int responseCode = con.getResponseCode();
		// System.out.println("GET Response Code :: " + responseCode);
		logger.info(responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			logger.info(response.toString());
		} else {
			logger.info(responseCode);
		}

	}
	
	public void linePush(lineRequestModel request) throws IOException {

		logger.info("---------------sendPost line Noti------------------");

		String lineApi = "https://api.line.me/v2/bot/message/push";


		byte[] input = new Gson().toJson(request).toString().getBytes("utf-8");
		URL obj = new URL(lineApi);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestProperty("Authorization", "Bearer " + Constants.LINE_MESSAGE_API_ACCESS_TOKEN);
		
		con.setDoOutput(true);
		con.setInstanceFollowRedirects(false);
		con.setUseCaches(false);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");

		con.setRequestMethod("POST");

		try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
			wr.write(input);
		}
		int responseCode = con.getResponseCode();
		// System.out.println("GET Response Code :: " + responseCode);
		logger.info(responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			logger.info(response.toString());
		} else {
			logger.info(responseCode);
		}

	}

}
