package com.app.ptt.notification.service;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.dao.CommonDAO;
import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.ListboxModel;

@Service
public class CommonService {

	private static Logger logger = LogManager.getLogger(CommonService.class);

	@Autowired
	CommonDAO commonDao;

	public List<ListboxModel> listUserShop(int userId) {
		List<ListboxModel> listBox = null;
		try {
			listBox = commonDao.listUserShop(userId);
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return listBox;
	}

	public boolean sendMail(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;
		try {
			
			result = sendEmailWithAttachment(model);
				
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}

	public boolean sendEmailWithAttachment(EmailTemplateModel model)

	{
		try {
			String Template = "";
			JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
			MimeMessage msg = mailSender.createMimeMessage();
			msg.setHeader("Content-Type", "text/plain; charset=UTF-8");
			mailSender.setHost("smtp.gmail.com");
			mailSender.setPort(587);
			mailSender.setUsername(Constants.MAIL_USERNAME);
			mailSender.setPassword(Constants.MAIL_PASSWORD);
//			EmailTemplateModel emailContent = commonDao.getEmailBodyByID(model.getEmailId());
			Template ="Test email";
			
			Properties props = mailSender.getJavaMailProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.debug", "true");
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
			props.put("mail.enable_starttls_auto", "smtp.gmail.com");

			mailSender.setJavaMailProperties(props);
			// get Email Template
			MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");

			helper.setFrom("info@makemypro.com", "healthMeNoti");
//			helper.setTo(model.getEmail());
			helper.setTo("kasidid17678@gmail.com");
//			if (model.getEmailId().equals("1")) {
//				helper.setSubject("Test Email ");
//				Template = emailContent.getBodyMessageEmail();
//				logger.info("Test : " + model.getOrderNumber());
//				Template = String.format(Template, "pride"
//
//				);
//
//			}
//			else if(model.getEmailId().equals("2")) {
//				helper.setSubject("Checkrace รหัสผ่านใหม่ของท่าน");
//				Template = emailContent.getBodyMessageEmail();
//					Template = String.format(Template, 
//						model.getEmail(),
//						model.getNewPassword());
//			}
//			helper.setBcc("kasidid17678@gmail.com");
			helper.setText(Template, true);

			mailSender.send(msg);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return true;

	}

}
