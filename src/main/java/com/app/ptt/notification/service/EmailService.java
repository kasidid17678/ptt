package com.app.ptt.notification.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.dao.CommonDAO;
import com.app.ptt.notification.dao.EmailDAO;
import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.EventImageModel;
import com.app.ptt.notification.model.EventModel;
import com.app.ptt.notification.model.OrderDetailModel;
import com.app.ptt.notification.model.OrderModel;
import com.app.ptt.notification.model.OrderObjectModel;
import com.app.ptt.notification.model.TicketCountModel;
import com.app.ptt.notification.model.UserAddressModel;
import com.app.ptt.notification.model.pttModel;
import com.app.ptt.notification.redis.EventRedis;
import com.app.ptt.notification.redis.UserOrderRedisService;
import com.app.ptt.notification.utils.LatteUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Service
public class EmailService {

	private static Logger logger = LogManager.getLogger(EmailService.class);

	@Autowired
	EmailDAO emailDao;
	@Autowired
	UserOrderRedisService userOrderRedisService;
	@Autowired
	EventRedis eventRedis;
	@Autowired
	CommonDAO commonDAO;
	
	@Value("${path.emailTemplate}")
	private String pathEmailTemplate;
	
	@Value("${path.qrcode}")
	private String pathQrcode;

	private static DecimalFormat df = new DecimalFormat("##,##0.00");

	public boolean sendMailRemindPayment(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;
		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
		List<TicketCountModel> count = new ArrayList<TicketCountModel>();

		try {
			if (!LatteUtil.isEmpty(model)) {

				if (!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber())) {

					UserAddressModel addrModel = new UserAddressModel();
					OrderModel order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
					detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());

					if (!LatteUtil.isEmpty(detail)) {
						for (OrderDetailModel od : detail) {
							List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());

							for (int i = 0; i < orderObject.size(); i++) {
								if (orderObject.get(i).getShirtField().equals("ACTIVE")) {
									od.setShirtSize(orderObject.get(i).getOptionDetailName());
								}

							}

							od.setOrderObject(orderObject);

						}
					}

					count = countTicket(detail);
					model.setOrderModel(order);
					model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
					model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
//					model.getOrderModel().setCouponDiscount(couponDiscount);(df.format(model.getOrderModel().getNetAmount()));
					if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
						model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
					}

					model.getOrderModel().setEventImageUrl(detail.get(0).getEventImageName());
					model.getOrderModel().setEventName(detail.get(0).getEventNameEn());

					model.getOrderModel().setListDistance(new ArrayList<>());
					model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
					model.setListTicketCount(count);
					model.setEmail(order.getEmail());

					model.getOrderModel().setOrderDetail(detail);

					if (order.getShippingStatus().equals(Constants.INACTIVE)) {
						model.setAddress(order.getTicketLocation());
					} else {
						addrModel = emailDao.getAddress(order.getUserAddressId());
						String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
								+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
								+ addrModel.getZipcode();
						model.setAddress(address);
					}

				}

//				System.out.println("================================================");
//				System.out.println(new Gson().toJson(model));

				result = sendEmailWithAttachment(model);

			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}

	public boolean sendMailAfterPayment(EmailTemplateModel model) throws MessagingException, IOException {
//		boolean result = true;
//		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
//		List<TicketCountModel> count = new ArrayList<TicketCountModel>();
//		UserAddressModel addrModel = new UserAddressModel();
//		try {
//			if (!LatteUtil.isEmpty(model)) {
//				
//				if(!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber())) {
//
//					OrderModel order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
//					detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());
//					
//					for (OrderDetailModel od : detail) {
//						List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());
//						List<Integer> rem = new ArrayList<Integer>();
//						
////						if(od.getAgeGroup().equals("YEAR")) {
////							od.setAge(LatteUtil.getAgeByYear(od.getBirthDate()));
////						}else {
//							od.setAge(LatteUtil.getAgeByDate(od.getBirthDate()));
////						}
//						
//						for(int i=0;i<orderObject.size();i++) {
//							 if(orderObject.get(i).getObjectType().equals("country")) {
//								 od.setNationality(orderObject.get(i).getObjectValue());
//								 rem.add(i);
//							 }else if(orderObject.get(i).getObjectType().equals("contactPerson")) {
//								 od.setContactPerson(orderObject.get(i).getObjectValue());
//								 rem.add(i);
//							 }else if(orderObject.get(i).getObjectType().equals("contactNumber")) {
//								 od.setContactNumber(orderObject.get(i).getObjectValue());
//								 rem.add(i);
//							 }else if(orderObject.get(i).getObjectType().equals("shirtSize")) {
//								 od.setShirtSize(orderObject.get(i).getObjectValue());
//								 rem.add(i);
//							 }
//						}
//						
//						for(int i=rem.size()-1;i>=0;i--) {
//							int index = rem.get(i); 
//
//							orderObject.remove(index);
//						}
//						
//						od.setOrderObject(orderObject);
//					}
//					
//					count = countTicket(detail);
//					model.setOrderModel(order);
//					model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
//					model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
//
//					model.getOrderModel().setEventImage(detail.get(0).getEventImageName());
//					model.getOrderModel().setEventName(detail.get(0).getEventNameEn());
//
//					model.setListTicketCount(count);
//					model.setEmail(order.getEmail());
//
//					model.getOrderModel().setOrderDetail(detail);
//					
//					
//					if(order.getShippingStatus().equals(Constants.INACTIVE)) {
//						model.setAddress(order.getTicketLocation());
//					}else {
//						addrModel = emailDao.getAddress(order.getUserAddressId());
//						String address = addrModel.getAddress()+" " + addrModel.getSubDistrictCode()+" " + 
//								addrModel.getDistrictCode()+" " + addrModel.getProvinceCode()+" " + addrModel.getZipcode();
//						model.setAddress(address);
//					}
//				
//					
//				}
//				
//					result = sendEmailWithAttachment(model);
//				
//			} else {
//				return false;
//			}
//		} catch (Exception ex) {
//			logger.error(ex);
//			ex.printStackTrace();
//			return false;
//		}
//		return result;

		boolean result = true;
		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
		List<TicketCountModel> count = new ArrayList<TicketCountModel>();

		try {
			if (!LatteUtil.isEmpty(model)) {

				if (!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber())) {
					UserAddressModel addrModel = new UserAddressModel();
					OrderModel order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
					detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());

					if (!LatteUtil.isEmpty(detail)) {
						for (OrderDetailModel od : detail) {
							List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());

							for (int i = 0; i < orderObject.size(); i++) {
								if (orderObject.get(i).getShirtField().equals("ACTIVE")) {
									od.setShirtSize(orderObject.get(i).getOptionDetailName());
								}

							}

							od.setOrderObject(orderObject);

						}
					}

					count = countTicket(detail);
					model.setOrderModel(order);
					model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
					model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
					
					if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
						model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
					}
		
					model.getOrderModel().setEventImageUrl(detail.get(0).getEventImageName());
					model.getOrderModel().setEventName(detail.get(0).getEventNameEn());

					model.getOrderModel().setListDistance(new ArrayList<>());
					model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
					model.setListTicketCount(count);
					model.setEmail(order.getEmail());

					model.getOrderModel().setOrderDetail(detail);

					if (order.getShippingStatus().equals(Constants.INACTIVE)) {
						model.setAddress(order.getTicketLocation());
						model.getOrderModel().setQrCodePath(pathQrcode+order.getEventId()+"/"+order.getQrCode()+".png");
						if(!LatteUtil.isEmpty(model.getOrderModel().getOrderDetail())) {
							for(OrderDetailModel od:model.getOrderModel().getOrderDetail()) {
								od.setQrCodePath(pathQrcode+order.getEventId()+"/"+od.getQrCode()+".png");
							}
						}
					} else {
						addrModel = emailDao.getAddress(order.getUserAddressId());
						String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
								+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
								+ addrModel.getZipcode();
						model.setAddress(address);
					}
				}

//				System.out.println("================================================");
//				System.out.println(new Gson().toJson(model));

				result = sendEmailWithAttachment(model);

			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}

	public boolean sendMailResetPassword(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;

		try {
			if (!LatteUtil.isEmpty(model)) {

				result = sendEmailWithAttachment(model);

			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}

	public boolean sendMailPtt(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;
System.out.println("PTT");
		try {
			if (!LatteUtil.isEmpty(model)) {
				
				List<pttModel> list = commonDAO.listPtt();
				System.out.println("LISE "+new Gson().toJson(list));
				int i = 0;
				for(pttModel l :list) {
					EmailTemplateModel request = new EmailTemplateModel();
					request.setEmail(l.getEmail());
					request.setEmailId("5");
					request.setFolderLink(l.getGoogleDriveUrl());
					
					result = sendEmailWithAttachment(request);
					System.out.println(++i);
				}
		

				
			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}
	
	public boolean sendMailAdvertise(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;
		
		try {
			if (!LatteUtil.isEmpty(model)) {
				if (!LatteUtil.isEmpty(model.getListOrderDetailEmail())) {
					for (OrderDetailModel l : model.getListOrderDetailEmail()) {
						model.setEmail(l.getEmail());
						result = sendEmailWithAttachment(model);
					}
				}
				
			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}

	public List<OrderDetailModel> listFilteredOrderDetail(EmailTemplateModel model) {
		List<OrderDetailModel> listorderDetail = new ArrayList<OrderDetailModel>();
		try {
			if (!LatteUtil.isEmpty(model)) {
				listorderDetail = emailDao.listFilteredOrderDetail(model);
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
		}
		return listorderDetail;
	}

	public boolean sendEmailWithAttachment(EmailTemplateModel model)

	{
		try {
			
			System.out.println("Send Email");
			String Template = "";
			JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
			MimeMessage msg = mailSender.createMimeMessage();
			msg.setHeader("Content-Type", "text/plain; charset=UTF-8");
			mailSender.setHost("smtp.gmail.com");
			mailSender.setPort(587);
			mailSender.setUsername(Constants.MAIL_USERNAME);
			mailSender.setPassword(Constants.MAIL_PASSWORD);

//			mailSender.setUsername("codecaster.dev@gmail.com");
//			mailSender.setPassword("ipgtywmrlpylinsb");
			
			Properties props = mailSender.getJavaMailProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.debug", "true");
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
			props.put("mail.enable_starttls_auto", "smtp.gmail.com");

			mailSender.setJavaMailProperties(props);
			// get Email Template
			MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");

			helper.setFrom("ORHappyAnniversary", "OR Happy Anniversary");
			helper.setTo(model.getEmail());
//			helper.setTo("codecaster.line@gmail.com");
//			helper.setTo("rewat.satta@gmail.com");
//			helper.setTo("kasidid17678@gmail.com");
//			helper.setTo("atiwat.v.sam@gmail.com");
			
//			helper.setBcc("kasidid17678@gmail.com");
			
			if (model.getEmailId().equals("5")) {
			
				Template = readFile("mail.htm");
				helper.setSubject("4th OR Happy Anniversary :  ขอส่งมอบภาพสวย ๆ ของเพื่อน ๆ เนื่องในโอกาสครบรอบ 4 ปี โออาร์์");
				
				Template = Template.replace("[LINK]", LatteUtil.isEmpty(model.getFolderLink()) ? "-"
						: model.getFolderLink());

			}else if (model.getEmailId().equals("1")) {

				Template = readFile("/mail-1/mail-1-payment-alert.htm");

				helper.setSubject("Registration Submitted - " + model.getOrderModel().getEventName());

//			 
//				if(!LatteUtil.isEmpty(model.getOrderModel().getCustomerName())) {
//					Template = Template.replace("[customerName]", model.getOrderModel().getCustomerName());
//
//				}else {
//					Template = Template.replace("[customerName]", model.getOrderModel().getEmail());
//				}
//			
//				Template = Template.replace("[paymentStatus]", LatteUtil.isEmpty(model.getOrderModel().getOrderStatusDesc()) ?
//				"-" : model.getOrderModel().getOrderStatusDesc()
//				); 
//				Template = Template.replace("[couponDiscount]", LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt()) ?
//				"-" : df.format(model.getOrderModel().getDiscountAmt())
//				);
//
//				//URLMAIL
//				Template = Template.replace("[UrlMail1Visit]", Constants.URL_VISIT
//						);
//				Template = Template.replace("[UrlMail1Payment]", LatteUtil.isEmpty(model.getOrderModel().getOrderId()) ? 
//						Constants.URL_USER_ORDER : Constants.URL_USER_ORDER+"?id="+model.getOrderModel().getOrderId()
//						);
//				Template = Template.replace("[UrlMail1Detail]", LatteUtil.isEmpty(model.getOrderModel().getOrderId()) ? 
//						Constants.URL_USER_ORDER : Constants.URL_USER_ORDER+"?id="+model.getOrderModel().getOrderId()
//						);
//						
//	

				Template = Template.replace("[eventName]", LatteUtil.isEmpty(model.getOrderModel().getEventName()) ? "-"
						: model.getOrderModel().getEventName());

				Template = Template.replace("[organizationName]",
						LatteUtil.isEmpty(model.getOrderModel().getOrganizationName()) ? "-"
								: model.getOrderModel().getOrganizationName());

				Template = Template.replace("[provinceName]",
						LatteUtil.isEmpty(model.getOrderModel().getProvinceName()) ? "-"
								: model.getOrderModel().getProvinceName());

				Template = Template.replace("[orderNumber]",
						LatteUtil.isEmpty(model.getOrderModel().getOrderNumber()) ? "-"
								: model.getOrderModel().getOrderNumber());

				Template = Template.replace("[eventDate]", LatteUtil.isEmpty(model.getOrderModel().getEventDate()) ? "-"
						: LatteUtil.dateThai(model.getOrderModel().getEventDate()));

				Template = Template.replace("[eventImage]",
						LatteUtil.isEmpty(model.getOrderModel().getEventImageUrl()) ? "-"
								: model.getOrderModel().getEventImageUrl());

				
				
				String listDistancePart = "";

				for (int i = 0; i < model.getOrderModel().getListDistance().size(); i++) {

					if (i == 0) {
						String distance = readFile("/mail-1/mail-1-payment-alert-listDistance-red.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

					if (i == 1) {
						String distance = readFile("/mail-1/mail-1-payment-alert-listDistance-yellow.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

					if (i == 2) {
						String distance = readFile("/mail-1/mail-1-payment-alert-listDistance-green.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

					if (i == 3) {
						String distance = readFile("/mail-1/mail-1-payment-alert-listDistance-other.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

				}

				Template = Template.replace("[listDistance]", listDistancePart);

				String ticketPart = "";

				for (int i = 0; i < model.getListTicketCount().size(); i++) {

					String ticket = readFile("/mail-1/mail-1-payment-alert-count-ticket.htm");

					ticket = ticket.replace("[ticketName]",
							LatteUtil.isEmpty(model.getListTicketCount().get(i).getTicketNameTh()) ? "-"
									: model.getListTicketCount().get(i).getTicketNameTh());

					ticket = ticket.replace("[ticketCount]",
							LatteUtil.isEmpty(model.getListTicketCount().get(i).getCount()) ? "-"
									: Integer.toString(model.getListTicketCount().get(i).getCount()));

					ticket = ticket.replace("[ticketPrice]",
							LatteUtil.isEmpty(model.getListTicketCount().get(i).getNetAmt()) ? "0.00"
									: df.format(model.getListTicketCount().get(i).getNetAmt()));

					String namePart = "";

					for (int j = 0; j < model.getListTicketCount().get(i).getListName().size(); j++) {

						String name = readFile("/mail-1/mail-1-payment-alert-count-name.htm");

						name = name.replace("[name]",
								LatteUtil.isEmpty(model.getListTicketCount().get(i).getListName().get(j)) ? "-"
										: model.getListTicketCount().get(i).getListName().get(j));

						namePart = namePart + name;
					}

					ticket = ticket.replace("[listName]", namePart);
					ticketPart = ticketPart + ticket;
				}

				Template = Template.replace("[ticketCountPart]", ticketPart);

				Template = Template.replace("[amount]", LatteUtil.isEmpty(model.getOrderModel().getAmount()) ? "0.00"
						: df.format(model.getOrderModel().getAmount()));

				Template = Template.replace("[totalOrder]",
						LatteUtil.isEmpty(model.getOrderModel().getTotalOrder()) ? "-"
								: String.valueOf(model.getOrderModel().getTotalOrder()));

				Template = Template.replace("[netAmt]", LatteUtil.isEmpty(model.getOrderModel().getNetAmount()) ? "0.00"
						: df.format(model.getOrderModel().getNetAmount()));

				Template = Template.replace("[shippingAmount]",
						LatteUtil.isEmpty(model.getOrderModel().getShippingAmount()) ? "0.00"
								: df.format(model.getOrderModel().getShippingAmount()));

				Template = Template.replace("[paymentGatewayFee]",
						LatteUtil.isEmpty(model.getOrderModel().getPaymentGatewayFeeAmount()) ? "0.00"
								: df.format(model.getOrderModel().getPaymentGatewayFeeAmount()));
				
				
				if(model.getOrderModel().getShippingStatus().equals(Constants.ACTIVE)) {
					Template = Template.replace("[shipping_content]", Constants.SHIPPING_CONTENT_ACTIVE);
				}else {
					Template = Template.replace("[shipping_content]", Constants.SHIPPING_CONTENT_INACTIVE);
				}

				Template = Template.replace("[address]",
						LatteUtil.isEmpty(model.getAddress()) ? "-" : model.getAddress());

				String listOrderDetailPart = "";

				for (int i = 0; i < model.getOrderModel().getOrderDetail().size(); i++) {

					String orderDetail = readFile("/mail-1/mail-1-payment-alert-order0detail.htm");
					String name = "";
					Number index = i + 1;

					if (!LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getFirstName())) {

						if (!LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getLastName())) {
							name = model.getOrderModel().getOrderDetail().get(i).getFirstName() + " "
									+ model.getOrderModel().getOrderDetail().get(i).getLastName();
						}
					}

					orderDetail = orderDetail.replace("[fullName]", LatteUtil.isEmpty(name) ? "-" : name);

					orderDetail = orderDetail.replace("[count]", index.toString());

					orderDetail = orderDetail.replace("[genderDesc]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getGenderDesc()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getGenderDesc());

					orderDetail = orderDetail.replace("[ticketName]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getTicketNameEn()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getTicketNameEn());

					orderDetail = orderDetail.replace("[ageGroupDesc]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getAgeGroupDesc()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getAgeGroupDesc());

					orderDetail = orderDetail.replace("[nationality]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getNationality()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getNationality());

					orderDetail = orderDetail.replace("[idpsNumber]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getIdpsNumber()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getIdpsNumber());

					orderDetail = orderDetail.replace("[tel]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getTel()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getTel());

					orderDetail = orderDetail.replace("[shirtSize]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getShirtSize()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getShirtSize());

					orderDetail = orderDetail.replace("[birthDate]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getBirthDate()) ? "-"
									: LatteUtil.dateThai(model.getOrderModel().getOrderDetail().get(i).getBirthDate()));

					orderDetail = orderDetail.replace("[bloodGroup]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getBloodGroup()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getBloodGroup());

					orderDetail = orderDetail.replace("[sick]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getSick()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getSick());

					orderDetail = orderDetail.replace("[contactPerson]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getContactPerson()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getContactPerson());

					orderDetail = orderDetail.replace("[contactNumber]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getContactNumber()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getContactNumber());


					List<OrderObjectModel> listOrderObjectModel = model.getOrderModel().getOrderDetail().get(i)
							.getOrderObject();

					String listOrderObjectPartM = "";

					if (!LatteUtil.isEmpty(listOrderObjectModel)) {

						for (int j = 0; j < listOrderObjectModel.size(); j++) {
							String orderObjectM = readFile("/mail-1/mail-1-payment-alert-orderObject-m.htm");

							orderObjectM = orderObjectM.replace("[description1]",
									LatteUtil.isEmpty(listOrderObjectModel.get(j).getDescription()) ? ""
											: listOrderObjectModel.get(j).getDescription() + ":");
							orderObjectM = orderObjectM.replace("[optionDetailName1]",
									LatteUtil.isEmpty(listOrderObjectModel.get(j).getOptionDetailName()) ? ""
											: listOrderObjectModel.get(j).getOptionDetailName());

							listOrderObjectPartM = listOrderObjectPartM + orderObjectM;
						}

					}

					orderDetail = orderDetail.replace("[orderObjectM]", listOrderObjectPartM);
					listOrderDetailPart = listOrderDetailPart + orderDetail;

				}

				Template = Template.replace("[listOrderDetail]", listOrderDetailPart);

				String url = "https://race.checkrace.com/user-order?id=" + model.getOrderModel().getOrderId();
				Template = Template.replace("[paymentUrl]",
						LatteUtil.isEmpty(model.getOrderModel().getOrderId()) ? "https://race.checkrace.com"
								: url);
				//dicount content part
				String discountContent = "";

				if (!LatteUtil.isEmpty(model.getOrderModel().getCouponCode())) {

					 discountContent = readFile("/mail-1/mail-1-payment-alert-discount-amount.htm");
			
					discountContent = discountContent.replace("[couponCode]", 
							LatteUtil.isEmpty(model.getOrderModel().getCouponCode()) ? "" : model.getOrderModel().getCouponCode()
							);

					discountContent = discountContent.replace("[couponDiscount]", 
						LatteUtil.isEmpty(model.getOrderModel().getCouponDiscount()) ? "0.00" : model.getOrderModel().getCouponDiscount()
							);

					discountContent = discountContent.replace("[discountAmount]",
							LatteUtil.isEmpty(model.getOrderModel().getDiscountAmtDesc()) ? "0.00"
									: model.getOrderModel().getDiscountAmtDesc());
				}

				Template = Template.replace("[discountContent]", discountContent);
				
			} else if (model.getEmailId().equals("2")) {

				helper.setSubject("Payment Confirmation - " + model.getOrderModel().getEventName());
				Template = readFile("/mail-2/mail-2-payment-alert.htm");
//				Template = readFile("/mail-2-payment-confirm.htm");
//				
//				if(!LatteUtil.isEmpty(model.getOrderModel().getCustomerName())) {
//					Template = Template.replace("[customerName]", model.getOrderModel().getCustomerName());
//
//				}else {
//					Template = Template.replace("[customerName]", model.getOrderModel().getEmail());
//				}
//			
//				Template = Template.replace("[eventName]", LatteUtil.isEmpty(model.getOrderModel().getEventName()) ? "-"
//						: model.getOrderModel().getEventName()
//						);
//				Template = Template.replace("[orderNumber]", LatteUtil.isEmpty(model.getOrderModel().getOrderNumber()) ? "-"
//						: model.getOrderModel().getOrderNumber()
//						);
//				Template = Template.replace("[paymentStatus]", LatteUtil.isEmpty(model.getOrderModel().getOrderStatusDesc()) ? "-"
//						: model.getOrderModel().getOrderStatusDesc()
//						);
//				
//				Template = Template.replace("[eventImage]", LatteUtil.isEmpty(model.getOrderModel().getEventImage()) ? "-"
//						: model.getOrderModel().getEventImage()
//						);
//				
//				Template = Template.replace("[amount]", LatteUtil.isEmpty(model.getOrderModel().getAmountDesc()) ? "-"
//						: model.getOrderModel().getAmountDesc()
//						);
//				Template = Template.replace("[totalOrder]", LatteUtil.isEmpty(model.getOrderModel().getTotalOrder()) ? "-"
//						: String.valueOf(model.getOrderModel().getTotalOrder())
//						);
//				Template = Template.replace("[netAmt]", LatteUtil.isEmpty(model.getOrderModel().getNetAmountDesc()) ? "-"
//						: model.getOrderModel().getNetAmountDesc()
//						);
//				Template = Template.replace("[couponDiscount]", LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt()) ? "-"
//						: df.format(model.getOrderModel().getDiscountAmt())
//						);
//				
//				Template = Template.replace("[address]", LatteUtil.isEmpty(model.getAddress()) ? "-"
//						: model.getAddress()
//						);
//				//URLMAIL
//				Template = Template.replace("[UrlMail2Visit]", Constants.URL_VISIT
//						);
//				
//				String ticketPart = "" ;
//				for(int i=0;i<model.getListTicketCount().size();i++) {
//					String ticket = readFile("/mail-2-payment-confirm-info.htm");
//					ticket = ticket.replace("[ticketName]", LatteUtil.isEmpty(model.getListTicketCount().get(i).getTicketNameTh()) ? "-"
//							: model.getListTicketCount().get(i).getTicketNameTh()
//							);
//					ticket = ticket.replace("[ticketCount]", LatteUtil.isEmpty(model.getListTicketCount().get(i).getCount()) ? "-"
//							: Integer.toString(model.getListTicketCount().get(i).getCount())
//							); 
//					ticket = ticket.replace("[ticketPrice]", LatteUtil.isEmpty(model.getListTicketCount().get(i).getNetAmtDesc()) ? "-"
//							: model.getListTicketCount().get(i).getNetAmtDesc()
//							);
//					ticketPart = ticketPart + ticket ;
//				}
//				Template = Template.replace("[ticketCountPart]", ticketPart);
//				
//				String detailInfo = "" ;
//				for(int i=0;i<model.getOrderModel().getOrderDetail().size();i++) {
//					String detail = readFile("/mail-2-payment-confirm-info-detail.htm");
//					detail = detail.replace("[index]", String.valueOf(i+1));
//					detail = detail.replace("[ticketName]", LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getTicketNameTh()) ? "-"
//							: model.getOrderModel().getOrderDetail().get(i).getTicketNameTh()
//							 );
//					detail = detail.replace("[ticketAgeGroup]", 
//							(LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getAgeGroupDesc()) ? "-" 
//									: model.getOrderModel().getOrderDetail().get(i).getAgeGroupDesc() )
//							);
//					detail = detail.replace("[bibNumber]", (LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getSerialNumber()) ? "-" 
//							: model.getOrderModel().getOrderDetail().get(i).getSerialNumber())
//					);
//					detail = detail.replace("[shirtSize]", 
//							(LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getShirtSize()) ? "-" 
//									: model.getOrderModel().getOrderDetail().get(i).getShirtSize()
//							 ));
//					detail = detail.replace("[firstName]", LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getFirstName()) ? "-"
//							: model.getOrderModel().getOrderDetail().get(i).getFirstName()
//							 );
//					detail = detail.replace("[lastName]", LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getLastName()) ? "-"
//							: model.getOrderModel().getOrderDetail().get(i).getLastName()
//							 );
//					detail = detail.replace("[age]", LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getAge()) ? "-"
//							: model.getOrderModel().getOrderDetail().get(i).getAge()
//							 );
//					detail = detail.replace("[gender]", LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getGenderDesc()) ? "-"
//							: model.getOrderModel().getOrderDetail().get(i).getGenderDesc()
//							 );
//					detail = detail.replace("[bloodGroup]", 
//							(LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getBloodGroup()) ? "-" 
//									: model.getOrderModel().getOrderDetail().get(i).getBloodGroup()
//							 )
//							);
//					detail = detail.replace("[country]", 
//							(LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getNationality()) ? "-" 
//									: model.getOrderModel().getOrderDetail().get(i).getNationality()
//							 ));
//					detail = detail.replace("[contactPerson]", 
//							(LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getContactPerson()) ? "-" 
//							: model.getOrderModel().getOrderDetail().get(i).getContactPerson() )
//					);
//					detail = detail.replace("[contactNumber]", 
//							(LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getContactNumber()) ? "-" 
//							: model.getOrderModel().getOrderDetail().get(i).getContactNumber() )
//					);
//					
//							
//					detailInfo = detailInfo + detail ;
//					String optionDetail = "";
//					if(!LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getOrderObject())) {
//						for(int j=0;j<model.getOrderModel().getOrderDetail().get(i).getOrderObject().size();j++) {
//							//OPTION
//							String option = readFile("/mail-2-payment-confirm-info-option.htm");
//							option = option.replace("[option]", (LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getOrderObject().get(j).getDescription()) ? "-" 
//									: model.getOrderModel().getOrderDetail().get(i).getOrderObject().get(j).getDescription())
//									); 
//							option = option.replace("[information]", (LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getOrderObject().get(j).getObjectValue()) ? "-" 
//									: model.getOrderModel().getOrderDetail().get(i).getOrderObject().get(j).getObjectValue())
//									);  
//							optionDetail = optionDetail + option;
//						}
//						detailInfo = detailInfo.replace("[optionDetail]", optionDetail);
//					}
//					
//				}
//				Template = Template.replace("[detailInfo]", detailInfo);

				Template = Template.replace("[eventName]", LatteUtil.isEmpty(model.getOrderModel().getEventName()) ? "-"
						: model.getOrderModel().getEventName());

				Template = Template.replace("[organizationName]",
						LatteUtil.isEmpty(model.getOrderModel().getOrganizationName()) ? "-"
								: model.getOrderModel().getOrganizationName());

				Template = Template.replace("[provinceName]",
						LatteUtil.isEmpty(model.getOrderModel().getProvinceName()) ? "-"
								: model.getOrderModel().getProvinceName());

				Template = Template.replace("[orderNumber]",
						LatteUtil.isEmpty(model.getOrderModel().getOrderNumber()) ? "-"
								: model.getOrderModel().getOrderNumber());

				Template = Template.replace("[eventDate]", LatteUtil.isEmpty(model.getOrderModel().getEventDate()) ? "-"
						: LatteUtil.dateThai(model.getOrderModel().getEventDate()));

				Template = Template.replace("[eventImage]",
						LatteUtil.isEmpty(model.getOrderModel().getEventImageUrl()) ? "-"
								: model.getOrderModel().getEventImageUrl());
				
				String listDistancePart = "";

				for (int i = 0; i < model.getOrderModel().getListDistance().size(); i++) {

					if (i == 0) {
						String distance = readFile("/mail-2/mail-2-payment-alert-listDistance-red.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

					if (i == 1) {
						String distance = readFile("/mail-2/mail-2-payment-alert-listDistance-yellow.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

					if (i == 2) {
						String distance = readFile("/mail-2/mail-2-payment-alert-listDistance-green.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

					if (i == 3) {
						String distance = readFile("/mail-2/mail-2-payment-alert-listDistance-other.htm");
						distance = distance.replace("[distance]",
								LatteUtil.isEmpty(model.getOrderModel().getListDistance().get(i)) ? "-"
										: model.getOrderModel().getListDistance().get(i));

						listDistancePart = listDistancePart + distance;
					}

				}

				Template = Template.replace("[listDistance]", listDistancePart);

				String ticketPart = "";

				for (int i = 0; i < model.getListTicketCount().size(); i++) {

					String ticket = readFile("/mail-2/mail-2-payment-alert-count-ticket.htm");

					ticket = ticket.replace("[ticketName]",
							LatteUtil.isEmpty(model.getListTicketCount().get(i).getTicketNameTh()) ? "-"
									: model.getListTicketCount().get(i).getTicketNameTh());

					ticket = ticket.replace("[ticketCount]",
							LatteUtil.isEmpty(model.getListTicketCount().get(i).getCount()) ? "-"
									: Integer.toString(model.getListTicketCount().get(i).getCount()));

					ticket = ticket.replace("[ticketPrice]",
							LatteUtil.isEmpty(model.getListTicketCount().get(i).getNetAmt()) ? "0.00"
									: df.format(model.getListTicketCount().get(i).getNetAmt()));

					String namePart = "";

					
						for (int j = 0; j < model.getListTicketCount().get(i).getListName().size(); ++j) {

							String name = readFile("/mail-2/mail-2-payment-alert-count-name.htm");

							name = name.replace("[name]",
									LatteUtil.isEmpty(model.getListTicketCount().get(i).getListName().get(j)) ? "-"
											: model.getListTicketCount().get(i).getListName().get(j));

							namePart = namePart + name;
						}
					
				

					ticket = ticket.replace("[listName]", namePart);
					ticketPart = ticketPart + ticket;
				}

				Template = Template.replace("[ticketCountPart]", ticketPart);

				Template = Template.replace("[amount]", LatteUtil.isEmpty(model.getOrderModel().getAmount()) ? "0.00"
						: df.format(model.getOrderModel().getAmount()));

				Template = Template.replace("[totalOrder]",
						LatteUtil.isEmpty(model.getOrderModel().getTotalOrder()) ? "-"
								: String.valueOf(model.getOrderModel().getTotalOrder()));

				Template = Template.replace("[netAmt]", LatteUtil.isEmpty(model.getOrderModel().getNetAmount()) ? "0.00"
						: df.format(model.getOrderModel().getNetAmount()));

				Template = Template.replace("[shippingAmount]",
						LatteUtil.isEmpty(model.getOrderModel().getShippingAmount()) ? "0.00"
								: df.format(model.getOrderModel().getShippingAmount()));

				Template = Template.replace("[paymentGatewayFee]",
						LatteUtil.isEmpty(model.getOrderModel().getPaymentGatewayFeeAmount()) ? "0.00"
								: df.format(model.getOrderModel().getPaymentGatewayFeeAmount()));
				
				if(model.getOrderModel().getShippingStatus().equals(Constants.ACTIVE)) {
					Template = Template.replace("[shipping_content]", Constants.SHIPPING_CONTENT_ACTIVE);
					Template = Template.replace("[qrcodeTemplate]", "");
				}else {
					Template = Template.replace("[shipping_content]", Constants.SHIPPING_CONTENT_INACTIVE);
					
					String qrcode = readFile("/mail-2/mail-2-payment-alert-qrcode.htm");
					qrcode = qrcode.replace("[qrcode]", 
							LatteUtil.isEmpty(model.getOrderModel().getQrCodePath()) ? "-" : model.getOrderModel().getQrCodePath());
					Template = Template.replace("[qrcodeTemplate]", qrcode );
					
				}

				Template = Template.replace("[address]",
						LatteUtil.isEmpty(model.getAddress()) ? "-" : model.getAddress());
				
				
//				
				

				String listOrderDetailPart = "";

				for (int i = 0; i < model.getOrderModel().getOrderDetail().size(); i++) {

					String orderDetail = readFile("/mail-2/mail-2-payment-alert-order0detail.htm");
					String name = "";
					Number index = i + 1;

					if (!LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getFirstName())) {

						if (!LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getLastName())) {
							name = model.getOrderModel().getOrderDetail().get(i).getFirstName() + " "
									+ model.getOrderModel().getOrderDetail().get(i).getLastName();
						}
					}

					orderDetail = orderDetail.replace("[fullName]", LatteUtil.isEmpty(name) ? "-" : name);

					orderDetail = orderDetail.replace("[count]", index.toString());

					orderDetail = orderDetail.replace("[genderDesc]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getGenderDesc()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getGenderDesc());

					orderDetail = orderDetail.replace("[ticketName]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getTicketNameEn()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getTicketNameEn());

					orderDetail = orderDetail.replace("[ageGroupDesc]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getAgeGroupDesc()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getAgeGroupDesc());

					orderDetail = orderDetail.replace("[nationality]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getNationality()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getNationality());

					orderDetail = orderDetail.replace("[idpsNumber]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getIdpsNumber()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getIdpsNumber());

					orderDetail = orderDetail.replace("[tel]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getTel()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getTel());

					orderDetail = orderDetail.replace("[shirtSize]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getShirtSize()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getShirtSize());

					orderDetail = orderDetail.replace("[birthDate]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getBirthDate()) ? "-"
									: LatteUtil.dateThai(model.getOrderModel().getOrderDetail().get(i).getBirthDate()));

					orderDetail = orderDetail.replace("[bloodGroup]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getBloodGroup()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getBloodGroup());

					orderDetail = orderDetail.replace("[sick]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getSick()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getSick());

					orderDetail = orderDetail.replace("[contactPerson]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getContactPerson()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getContactPerson());

					orderDetail = orderDetail.replace("[contactNumber]",
							LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getContactNumber()) ? "-"
									: model.getOrderModel().getOrderDetail().get(i).getContactNumber());

					if(model.getOrderModel().getShippingStatus().equals(Constants.ACTIVE)) {
						orderDetail = orderDetail.replace("[qrcodeOrderDetailTemplate]", "");
						orderDetail = orderDetail.replace("[qrcodeOrderDetailTemplatePc]", "" );
						orderDetail = orderDetail.replace("[qrcodeOrderDetailTemplateMobile]", "" );
					}else {
					
						String qrcodeOrderDetailPc = readFile("/mail-2/mail-2-payment-alert-qrcode-order-detail-pc.htm");
						qrcodeOrderDetailPc = qrcodeOrderDetailPc.replace("[qrcode]", 
								LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getQrCodePath()) ? "-"
										: model.getOrderModel().getOrderDetail().get(i).getQrCodePath());
						orderDetail = orderDetail.replace("[qrcodeOrderDetailTemplatePc]", qrcodeOrderDetailPc );
						
						String qrcodeOrderDetailMobile = readFile("/mail-2/mail-2-payment-alert-qrcode-order-detail-m.htm");
						qrcodeOrderDetailMobile = qrcodeOrderDetailMobile.replace("[qrcode]", 
								LatteUtil.isEmpty(model.getOrderModel().getOrderDetail().get(i).getQrCodePath()) ? "-"
										: model.getOrderModel().getOrderDetail().get(i).getQrCodePath());
						orderDetail = orderDetail.replace("[qrcodeOrderDetailTemplateMobile]", qrcodeOrderDetailMobile );
						

					}

					
//					List<OrderObjectModel> array = new ArrayList<OrderObjectModel>();
//					List<List<OrderObjectModel>> resultOutput = new ArrayList<List<OrderObjectModel>>();
//					
//					List<OrderObjectModel> listOrderObjectModel =  model.getOrderModel().getOrderDetail().get(i).getOrderObject();
//					for(int j=0 ; j <= listOrderObjectModel.size() ; j++) {
//						
//						if(array.size() <= 1) {
//							
//							if(j == listOrderObjectModel.size()) {
//								resultOutput.add(array);
//							}else {
//								 array.add(model.getOrderModel().getOrderDetail().get(i).getOrderObject().get(j));
//							}
//							
//						}else {
//							resultOutput.add(array);
//							array = new ArrayList<OrderObjectModel>();
//							array.add(model.getOrderModel().getOrderDetail().get(i).getOrderObject().get(j));
//						}
//					}

//					String listOrderObjectPart = "" ;
//					String listOrderObjectPartM = "" ;
//					
//					if(!LatteUtil.isEmpty(resultOutput)) {
//						
//						for(int o = 0 ; o < resultOutput.size() ; o++) {
//							
//							
//							String orderObject = readFile("/mail-2/mail-2-payment-alert-orderObject.htm");
//							String orderObjectM = readFile("/mail-2/mail-2-payment-alert-orderObject-m.htm");
//							
//							array = resultOutput.get(o);
//							
//							if(!LatteUtil.isEmpty(array)) {
//								
//								if(array.size() == 1 ) {
//									OrderObjectModel opt = new OrderObjectModel();
//									opt.setDescription("");
//									opt.setOptionDetailName("");
//									array.add(opt);
//								}
//								
//								for(int x = 0 ; x < array.size() ; x++) {
//								
//										
//									if(x == 0 ) {
//										orderObject = orderObject.replace("[description1]", LatteUtil.isEmpty(array.get(x).getDescription()) 
//											? "" :  array.get(x).getDescription() + ":");
//										orderObject = orderObject.replace("[optionDetailName1]", LatteUtil.isEmpty(array.get(x).getOptionDetailName()) 
//											? "" :  array.get(x).getOptionDetailName());
//										
//										orderObjectM = orderObjectM.replace("[description1]", LatteUtil.isEmpty(array.get(x).getDescription()) 
//											? "" :  array.get(x).getDescription() + ":");
//										orderObjectM = orderObjectM.replace("[optionDetailName1]", LatteUtil.isEmpty(array.get(x).getOptionDetailName()) 
//											? "" :  array.get(x).getOptionDetailName());
//									}
//									
//									if(x == 1 ) {
//										orderObject = orderObject.replace("[description2]", LatteUtil.isEmpty(array.get(x).getDescription()) 
//											? "" :  array.get(x).getDescription() + ":");
//										orderObject = orderObject.replace("[optionDetailName2]", LatteUtil.isEmpty(array.get(x).getOptionDetailName()) 
//											? "" :  array.get(x).getOptionDetailName());
//										
//										orderObjectM = orderObjectM.replace("[description2]", LatteUtil.isEmpty(array.get(x).getDescription()) 
//											? "" :  array.get(x).getDescription() + ":");
//										orderObjectM = orderObjectM.replace("[optionDetailName2]", LatteUtil.isEmpty(array.get(x).getOptionDetailName()) 
//											? "" :  array.get(x).getOptionDetailName());
//									}
//									
//								}
//								
//								listOrderObjectPartM = listOrderObjectPartM + orderObjectM;
//								
//								listOrderObjectPart = listOrderObjectPart + orderObject;
//								
//							}
//							
//							
//							
//						}
//						
//					}

//					orderDetail = orderDetail.replace("[orderObject]", listOrderObjectPart);
//					orderDetail = orderDetail.replace("[orderObjectM]", listOrderObjectPartM);

					List<OrderObjectModel> listOrderObjectModel = model.getOrderModel().getOrderDetail().get(i)
							.getOrderObject();

					String listOrderObjectPartM = "";

					if (!LatteUtil.isEmpty(listOrderObjectModel)) {

						for (int j = 0; j < listOrderObjectModel.size(); j++) {
							String orderObjectM = readFile("/mail-2/mail-2-payment-alert-orderObject-m.htm");

							orderObjectM = orderObjectM.replace("[description1]",
									LatteUtil.isEmpty(listOrderObjectModel.get(j).getDescription()) ? ""
											: listOrderObjectModel.get(j).getDescription() + ":");
							orderObjectM = orderObjectM.replace("[optionDetailName1]",
									LatteUtil.isEmpty(listOrderObjectModel.get(j).getOptionDetailName()) ? ""
											: listOrderObjectModel.get(j).getOptionDetailName());

							listOrderObjectPartM = listOrderObjectPartM + orderObjectM;
						}

					}

					orderDetail = orderDetail.replace("[orderObjectM]", listOrderObjectPartM);

					listOrderDetailPart = listOrderDetailPart + orderDetail;

				}

				Template = Template.replace("[listOrderDetail]", listOrderDetailPart);

				String url = "https://race.checkrace.com/user-order?id=" + model.getOrderModel().getOrderId();
				Template = Template.replace("[paymentUrl]",
						LatteUtil.isEmpty(model.getOrderModel().getOrderId()) ? "https://race.checkrace.com"
								: url);

				//dicount content part
				String discountContent = "";

				if (!LatteUtil.isEmpty(model.getOrderModel().getCouponCode())) {

					 discountContent = readFile("/mail-2/mail-2-payment-alert-discount-amount.htm");
			
					discountContent = discountContent.replace("[couponCode]", 
							LatteUtil.isEmpty(model.getOrderModel().getCouponCode()) ? "" : model.getOrderModel().getCouponCode()
							);

					discountContent = discountContent.replace("[couponDiscount]", 
						LatteUtil.isEmpty(model.getOrderModel().getCouponDiscount()) ? "0.00" : model.getOrderModel().getCouponDiscount()
							);

					discountContent = discountContent.replace("[discountAmount]",
							LatteUtil.isEmpty(model.getOrderModel().getDiscountAmtDesc()) ? "0.00"
									: model.getOrderModel().getDiscountAmtDesc());
				}

				Template = Template.replace("[discountContent]", discountContent);
				
			} else if (model.getEmailId().equals("3")) {
				helper.setSubject("รหัสผ่านไหม่ Checkrace ");
				Template = readFile("/mail-3-reset-password.htm");

				Template = Template.replace("[newPassword]",
						(LatteUtil.isEmpty(model.getNewPassword()) ? "-" : model.getNewPassword()));

			} else if (model.getEmailId().equals("4")) {
				helper.setSubject("ประชาสัมพัน ");
				StringBuilder text = new StringBuilder();

				text.append(model.getMessage());
				if(!LatteUtil.isEmpty(model.getImg1())) {
					text.append(model.getImg1());
					}
				if(!LatteUtil.isEmpty(model.getImg2())) {
					text.append(model.getImg2());
				}
			
				Template = text.toString();

			}
			
			
			helper.setText(Template, true);

			mailSender.send(msg);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return true;

	}

	public String readFile(String path) {
		StringBuilder contentBuilder = new StringBuilder();
		BufferedReader in = null;
		try {

//			  in = new BufferedReader(new FileReader("D:/Codecaster/1_Checkrace/email/email_template" + path));
			in = new BufferedReader(new FileReader(pathEmailTemplate + path));
			String str;
			while ((str = in.readLine()) != null) {
				contentBuilder.append(str);
			}
			in.close();
		} catch (IOException e) {
			logger.info(e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return contentBuilder.toString();
	}

	public List<TicketCountModel> countTicket(List<OrderDetailModel> detail) {
		List<TicketCountModel> countTicket = new ArrayList<TicketCountModel>();
		List<Long> count = new ArrayList<Long>();
		int num = 0;
		for (int i = 0; i < detail.size(); i++) {

			if (count.size() == 0) {
				count.add(detail.get(i).getTicketId());
			} else if (count.get(num) != detail.get(i).getTicketId()) {
				count.add(detail.get(i).getTicketId());
				++num;
			}
		}

		for (int i = 0; i < count.size(); i++) {
			int total = 0;
			TicketCountModel ticket = new TicketCountModel();
			ticket.setListName(new ArrayList<>());
			for (int j = 0; j < detail.size(); j++) {
				if (count.get(i) == detail.get(j).getTicketId()) {
					++total;
					ticket.setTicketNameEn(detail.get(j).getTicketNameEn());
					ticket.setTicketNameTh(detail.get(j).getTicketNameTh());
					ticket.setTicketPrice(detail.get(j).getTicketPrice());
					ticket.getListName().add(detail.get(j).getFirstName() + " " + detail.get(j).getLastName());
				}
			}

			ticket.setCount(total);
			ticket.setTicketId(count.get(i));
			long netAmt = ticket.getTicketPrice() * ticket.getCount();

			ticket.setNetAmt(netAmt);
			ticket.setNetAmtDesc(String.valueOf(ticket.getNetAmt()));
			countTicket.add(ticket);
		}
		return countTicket;
	}

	public List<EmailTemplateModel> getListOrderNumber() {
		List<EmailTemplateModel> listOrder = new ArrayList<EmailTemplateModel>();
		listOrder = emailDao.getListOrderNumber();

		return listOrder;
	}

	public boolean reSendEmail(OrderModel orderModel) {
		EmailTemplateModel template = new EmailTemplateModel();
		Boolean result = false;
		try {
			if (orderModel.getOrderStatus().equals(Constants.ORDER_STATUS_PENDING_PAYMENT)) {
				template.setEmailId(Constants.REMIND_PAYMENT);
				template.setOrderModel(orderModel);
				sendMailRemindPayment(template);
				result = true;
			} else if (orderModel.getOrderStatus().equals(Constants.ORDER_STATUS_PAID)) {
				template.setEmailId(Constants.AFTER_PAYMENT);
				template.setOrderModel(orderModel);
				sendMailAfterPayment(template);
				result = true;
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean reSendSelectedEmail(OrderModel orderModel) {
		EmailTemplateModel template = new EmailTemplateModel();
		Boolean result = false;
		template.setEmail(orderModel.getEmail());
		try {
			if (orderModel.getOrderStatus().equals(Constants.ORDER_STATUS_PENDING_PAYMENT)) {
				template.setEmailId(Constants.REMIND_PAYMENT);
				template.setOrderModel(orderModel);
				sendSelectedMailRemindPayment(template);
				result = true;
			} else if (orderModel.getOrderStatus().equals(Constants.ORDER_STATUS_PAID)) {
				template.setEmailId(Constants.AFTER_PAYMENT);
				template.setOrderModel(orderModel);
				sendSelectedMailAfterPayment(template);
				result = true;
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean sendSelectedMailRemindPayment(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;
		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
		List<TicketCountModel> count = new ArrayList<TicketCountModel>();

		try {
			if (!LatteUtil.isEmpty(model)) {

				if (!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber())) {

					UserAddressModel addrModel = new UserAddressModel();
					OrderModel order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
					detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());

					if (!LatteUtil.isEmpty(detail)) {
						for (OrderDetailModel od : detail) {
							List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());

							for (int i = 0; i < orderObject.size(); i++) {
								if (orderObject.get(i).getShirtField().equals("ACTIVE")) {
									od.setShirtSize(orderObject.get(i).getOptionDetailName());
								}

							}

							od.setOrderObject(orderObject);

						}
					}

					count = countTicket(detail);
					model.setOrderModel(order);
					model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
					model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));

					if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
						model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
					}
					
					model.getOrderModel().setEventImageUrl(detail.get(0).getEventImageName());
					model.getOrderModel().setEventName(detail.get(0).getEventNameEn());

					model.getOrderModel().setListDistance(new ArrayList<>());
					model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
					model.setListTicketCount(count);
					model.setEmail(model.getEmail());

					model.getOrderModel().setOrderDetail(detail);

					if (order.getShippingStatus().equals(Constants.INACTIVE)) {
						model.setAddress(order.getTicketLocation());
					} else {
						addrModel = emailDao.getAddress(order.getUserAddressId());
						String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
								+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
								+ addrModel.getZipcode();
						model.setAddress(address);
					}

				}
				result = sendEmailWithAttachment(model);
			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}

	public boolean sendSelectedMailAfterPayment(EmailTemplateModel model) throws MessagingException, IOException {

		boolean result = true;
		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
		List<TicketCountModel> count = new ArrayList<TicketCountModel>();

		try {
			if (!LatteUtil.isEmpty(model)) {

				if (!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber())) {
					UserAddressModel addrModel = new UserAddressModel();
					OrderModel order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
					detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());

					if (!LatteUtil.isEmpty(detail)) {
						for (OrderDetailModel od : detail) {
							List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());

							for (int i = 0; i < orderObject.size(); i++) {
								if (orderObject.get(i).getShirtField().equals("ACTIVE")) {
									od.setShirtSize(orderObject.get(i).getOptionDetailName());
								}

							}

							od.setOrderObject(orderObject);

						}
					}

					count = countTicket(detail);
					model.setOrderModel(order);
					model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
					model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));

					if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
						model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
					}
					
					
					model.getOrderModel().setEventImageUrl(detail.get(0).getEventImageName());
					model.getOrderModel().setEventName(detail.get(0).getEventNameEn());

					model.getOrderModel().setListDistance(new ArrayList<>());
					model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
					model.setListTicketCount(count);
					model.setEmail(model.getEmail());
					
					model.getOrderModel().setOrderDetail(detail);

					if (order.getShippingStatus().equals(Constants.INACTIVE)) {
						model.setAddress(order.getTicketLocation());
					} else {
						addrModel = emailDao.getAddress(order.getUserAddressId());
						String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
								+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
								+ addrModel.getZipcode();
						model.setAddress(address);
					}

				}

				result = sendEmailWithAttachment(model);

			} else {
				return false;
			}
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}
	
	public boolean sendMailRemindPaymentRedis(EmailTemplateModel model) throws MessagingException, IOException {
		boolean result = true;
		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
		List<TicketCountModel> count = new ArrayList<TicketCountModel>();

		try {

			ObjectMapper mapper = new ObjectMapper();
			String listUserOrderStr = "";
			String eventStr = "";
			OrderModel order = new OrderModel();
			EventModel event = new EventModel();
			List<OrderModel> listUserOrder = new ArrayList<>();
			if (!LatteUtil.isEmpty(model)) {

				if (!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber()) && !LatteUtil.isEmpty(model.getOrderModel().getUserId())) {

					UserAddressModel addrModel = new UserAddressModel();
					
					listUserOrderStr = userOrderRedisService.findById(String.valueOf(model.getOrderModel().getUserId()));
					if (!LatteUtil.isEmpty(listUserOrderStr)) {
						listUserOrder = mapper.readValue(listUserOrderStr, new TypeReference<List<OrderModel>>() {});
						for(OrderModel o:listUserOrder) {
							if(o.getOrderNumber().equals(model.getOrderModel().getOrderNumber())) {
								order = o;
								if(!LatteUtil.isEmpty(order.getOrderDetail())) {
									for(OrderDetailModel od : order.getOrderDetail()) {
										od.setTicketNameTh(od.getTicketName());
										od.setGenderDesc(od.getGender().equals(Constants.MALE) ? "ชาย " : "หญิง" );
									}
								}
							}
						}
						//For redis 
						eventStr = eventRedis.findById(model.getOrderModel().getEventUrl());
						if (!LatteUtil.isEmpty(eventStr)) {
							event = mapper.readValue(eventStr, EventModel.class);
						}
						
						detail = order.getOrderDetail();
						
						count = countTicket(detail);
						model.setOrderModel(order);
						model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
						model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
						if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
							model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
						}
						if(!LatteUtil.isEmpty(order.getEventImage())) {
							for(EventImageModel i:order.getEventImage()) {
								if(i.getImageType().equals(Constants.IMAGE_BANNER)) {
									model.getOrderModel().setEventImageUrl(i.getImageUrl());
								}
							}
						}else {
							model.getOrderModel().setEventImageUrl("-");
						}
						
						model.getOrderModel().setEventName(event.getEventNameEn());
						model.getOrderModel().setOrganizationName(event.getOrganizationName());
						model.getOrderModel().setProvinceName(event.getProvinceDescTh());
						model.getOrderModel().setEventDate(LatteUtil.dateThai(event.getEventDate()));
						
						model.getOrderModel().setListDistance(new ArrayList<>());
						model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
						model.setListTicketCount(count);

						model.getOrderModel().setOrderDetail(detail);

						if (order.getShippingStatus().equals(Constants.INACTIVE)) {
							model.setAddress(order.getTicketLocation());
						} else {
							addrModel = emailDao.getAddress(order.getUserAddressId());
							String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
									+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
									+ addrModel.getZipcode();
							model.setAddress(address);
						}
						//
					}else {
		
							order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
							detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());
		
							if (!LatteUtil.isEmpty(detail)) {
								for (OrderDetailModel od : detail) {
									List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());
		
									for (int i = 0; i < orderObject.size(); i++) {
										if (orderObject.get(i).getShirtField().equals("ACTIVE")) {
											od.setShirtSize(orderObject.get(i).getOptionDetailName());
										}
		
									}
		
									od.setOrderObject(orderObject);
		
								}
							}
		
							count = countTicket(detail);
							model.setOrderModel(order);
							model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
							model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
							if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
								model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
							}
		
							model.getOrderModel().setEventImageUrl(detail.get(0).getEventImageName());
							model.getOrderModel().setEventName(detail.get(0).getEventNameEn());
		
							model.getOrderModel().setListDistance(new ArrayList<>());
							model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
							model.setListTicketCount(count);
							model.setEmail(order.getEmail());
		
							model.getOrderModel().setOrderDetail(detail);
		
							if (order.getShippingStatus().equals(Constants.INACTIVE)) {
								model.setAddress(order.getTicketLocation());
							} else {
								addrModel = emailDao.getAddress(order.getUserAddressId());
								String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
										+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
										+ addrModel.getZipcode();
								model.setAddress(address);
							}
						
					}
					result = sendEmailWithAttachment(model);
				}
				
			} else {
				return false;
			}
			
		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}
	
	public boolean sendMailAfterPaymentRedis(EmailTemplateModel model) throws MessagingException, IOException {

		boolean result = true;
		List<OrderDetailModel> detail = new ArrayList<OrderDetailModel>();
		List<TicketCountModel> count = new ArrayList<TicketCountModel>();

		try {
			ObjectMapper mapper = new ObjectMapper();
			String listUserOrderStr = "";
			String eventStr = "";
			OrderModel order = new OrderModel();
			EventModel event = new EventModel();
			List<OrderModel> listUserOrder = new ArrayList<>();
			UserAddressModel addrModel = new UserAddressModel();
			
			if (!LatteUtil.isEmpty(model)) {

				if (!LatteUtil.isEmpty(model.getOrderModel().getOrderNumber()) && !LatteUtil.isEmpty(model.getOrderModel().getUserId())) {

					
					
					listUserOrderStr = userOrderRedisService.findById(String.valueOf(model.getOrderModel().getUserId()));
					if (!LatteUtil.isEmpty(listUserOrderStr)) {
						listUserOrder = mapper.readValue(listUserOrderStr, new TypeReference<List<OrderModel>>() {});
						for(OrderModel o:listUserOrder) {
							if(o.getOrderNumber().equals(model.getOrderModel().getOrderNumber())) {
								order = o;
								if(!LatteUtil.isEmpty(order.getOrderDetail())) {
									for(OrderDetailModel od : order.getOrderDetail()) {
										od.setTicketNameTh(od.getTicketName());
										od.setGenderDesc(od.getGender().equals(Constants.MALE) ? "ชาย " : "หญิง" );
									}
								}
							}
						}
						//For redis 
						eventStr = eventRedis.findById(model.getOrderModel().getEventUrl());
						if (!LatteUtil.isEmpty(eventStr)) {
							event = mapper.readValue(eventStr, EventModel.class);
						}
						
						detail = order.getOrderDetail();
						////////////////////

						count = countTicket(detail);
						model.setOrderModel(order);
						model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
						model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
						if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
							model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
						}
						if(!LatteUtil.isEmpty(order.getEventImage())) {
							for(EventImageModel i:order.getEventImage()) {
								if(i.getImageType().equals(Constants.IMAGE_BANNER)) {
									model.getOrderModel().setEventImageUrl(i.getImageUrl());
								}
							}
						}else {
							model.getOrderModel().setEventImageUrl("-");
						}
						
						model.getOrderModel().setEventName(event.getEventNameEn());
						model.getOrderModel().setOrganizationName(event.getOrganizationName());
						model.getOrderModel().setProvinceName(event.getProvinceDescTh());
						model.getOrderModel().setEventDate(event.getEventDate());
						
						model.getOrderModel().setListDistance(new ArrayList<>());
						model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
						model.setListTicketCount(count);

						model.getOrderModel().setOrderDetail(detail);

						if (order.getShippingStatus().equals(Constants.INACTIVE)) {
							model.setAddress(order.getTicketLocation());
						} else {
							addrModel = emailDao.getAddress(order.getUserAddressId());
							String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
									+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
									+ addrModel.getZipcode();
							model.setAddress(address);
						}
						//
					}else {

							addrModel = new UserAddressModel();
							order = emailDao.getOrder(model.getOrderModel().getOrderNumber());
							detail = emailDao.getOrderDetail(model.getOrderModel().getOrderNumber());
		
							if (!LatteUtil.isEmpty(detail)) {
								for (OrderDetailModel od : detail) {
									List<OrderObjectModel> orderObject = emailDao.listOrderObject(od.getOrderDetailId());
		
									for (int i = 0; i < orderObject.size(); i++) {
										if (orderObject.get(i).getShirtField().equals("ACTIVE")) {
											od.setShirtSize(orderObject.get(i).getOptionDetailName());
										}
		
									}
		
									od.setOrderObject(orderObject);
		
								}
							}
		
							count = countTicket(detail);
							model.setOrderModel(order);
							model.getOrderModel().setAmountDesc(df.format(model.getOrderModel().getAmount()));
							model.getOrderModel().setNetAmountDesc(df.format(model.getOrderModel().getNetAmount()));
							
							if(!LatteUtil.isEmpty(model.getOrderModel().getDiscountAmt())) {
								model.getOrderModel().setDiscountAmtDesc(df.format(model.getOrderModel().getDiscountAmt()));
							}
				
							model.getOrderModel().setEventImageUrl(detail.get(0).getEventImageName());
							model.getOrderModel().setEventName(detail.get(0).getEventNameEn());
		
							model.getOrderModel().setListDistance(new ArrayList<>());
							model.getOrderModel().setListDistance(emailDao.listDistance(model.getOrderModel().getEventId()));
							model.setListTicketCount(count);
							model.setEmail(order.getEmail());
		
							model.getOrderModel().setOrderDetail(detail);
		
							if (order.getShippingStatus().equals(Constants.INACTIVE)) {
								model.setAddress(order.getTicketLocation());
							} else {
								addrModel = emailDao.getAddress(order.getUserAddressId());
								String address = addrModel.getAddress() + " " + addrModel.getSubDistrictCode() + " "
										+ addrModel.getDistrictCode() + " " + addrModel.getProvinceCode() + " "
										+ addrModel.getZipcode();
								model.setAddress(address);
							}
						
										
						
					}
					result = sendEmailWithAttachment(model);
				}
				
			} else {
				return false;
			}

		} catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			return false;
		}
		return result;
	}
	
	public boolean importShareLink(MultipartFile file) throws IOException {

		Workbook workbook = new XSSFWorkbook(file.getInputStream());
		Sheet firstSheet = workbook.getSheetAt(0);

		
		int id = 1;
		int link = 5;

		for (int i = 3; i <= firstSheet.getLastRowNum(); i++) {
			Row nextRow = firstSheet.getRow(i);
			
			
			String pttId = nextRow.getCell(id).toString();
			String shareLink = !LatteUtil.isEmpty(nextRow.getCell(link)) ?nextRow.getCell(link).toString():"";

//			System.out.println(pttId +" , "+ shareLink);
			commonDAO.clearLink();
			if(!LatteUtil.isEmpty(pttId)&&!LatteUtil.isEmpty(shareLink)) {
				commonDAO.importShareLink(pttId,shareLink);
			}
			
			
			}
		
		workbook.close();

		return true;
	}
	
}
