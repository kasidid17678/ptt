package com.app.ptt.notification.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.app.ptt.notification.utils.LatteUtil;

@Configuration
@PropertySource("classpath:application-${spring.profiles.active}.properties")
public class JWTFilterChain extends OncePerRequestFilter {
	
	@Value("${gateway.port}")
	public String gateWayPort;
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain fc) throws ServletException, IOException {
    	try {
			

    		String forwardPort = (String) request.getHeader("x-forwarded-port");
    		
    		if(!LatteUtil.isEmpty(forwardPort)) {
    			String[] listForwardPort = forwardPort.split("\\s*,\\s*");
	    		for (String s : listForwardPort) {
					if(s.equals(gateWayPort)) {
						forwardPort = s;
						break;
					}
				}
    		}
    	   
    		if(!gateWayPort.equals(forwardPort)) 
    			((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Authorization (Application)");
			else
    			fc.doFilter(request, response);
    		
		} catch (Exception e) {
			((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Exception Invalid Authorization (Application)");
			e.printStackTrace();
		}
    		
    }
    
}
