package com.app.ptt.notification.model;

import java.util.Date;
import java.util.List;


public class EventModel {
	
	private Integer eventId;
	private Integer organizationId;
	private String eventType;
	private String eventNameTh;
	private String eventNameEn;
	private String eventDesc;
	private String eventUrl;
	private String eventDate;
	private String registerStartDate;
	private String registerEndDate;
	private String eventStatus;
	private String shippingStatus;
	private Double shippingFee;
	private Double additionalShippingFee;
	private String ticketLocation;
	private String ticketLocationEn;
	private List<EventImageModel> eventImage;
	private List<OptionModel> listOption;
	private List<OptionModel> listField;
	private List<TicketModel> listTicket;
	private List<TicketFieldModel> listTicketField;
	private List<EventSectionModel> listEventSection;
	//ADD FIELD POINT//
	private String organizationName;
	private List<ProvinceModel> listProvince;
	private List<ListboxModel> listDay;
	private List<ListboxModel> listMonth;
	private List<ListboxModel> listYear;
	
	private String imageName;
	private String imageUrl;
	private String ageGroup;
	private String tagName;
	private String registerType;
	private List<EventPaymentGatewayModel> listEventPaymentGateway;
	private List<EventModel> listRecomend;
	private List<EventModel> listRace;
	private List<EventModel> listTrail;
	private List<EventModel> listPost;
	private List<EventTagModel> listEventTag;
	
	private String rules;
	private String weaverInfo;
	private String provinceCode;
	
	private String provinceDescTh;
	private String provinceDescEn;
	private String eventTypeDescTh;
	private String eventTypeDescEn;
	
	private Integer fileId;
	
	private List<PriceTypeModel> listPriceType;
	
	private List<String> listDistance;

	private EventSectionModel general;
	private EventSectionModel fee;
	private EventSectionModel benefit;
	private EventSectionModel reward;
	
	private String eventMonth;
	
	private String publish;
	
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventNameTh() {
		return eventNameTh;
	}

	public void setEventNameTh(String eventNameTh) {
		this.eventNameTh = eventNameTh;
	}

	public String getEventNameEn() {
		return eventNameEn;
	}

	public void setEventNameEn(String eventNameEn) {
		this.eventNameEn = eventNameEn;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public String getEventUrl() {
		return eventUrl;
	}

	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getRegisterStartDate() {
		return registerStartDate;
	}

	public void setRegisterStartDate(String registerStartDate) {
		this.registerStartDate = registerStartDate;
	}

	public String getRegisterEndDate() {
		return registerEndDate;
	}

	public void setRegisterEndDate(String registerEndDate) {
		this.registerEndDate = registerEndDate;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getShippingStatus() {
		return shippingStatus;
	}

	public void setShippingStatus(String shippingStatus) {
		this.shippingStatus = shippingStatus;
	}
	public Double getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(Double shippingFee) {
		this.shippingFee = shippingFee;
	}

	public Double getAdditionalShippingFee() {
		return additionalShippingFee;
	}

	public void setAdditionalShippingFee(Double additionalShippingFee) {
		this.additionalShippingFee = additionalShippingFee;
	}

	public String getTicketLocation() {
		return ticketLocation;
	}

	public void setTicketLocation(String ticketLocation) {
		this.ticketLocation = ticketLocation;
	}

	public String getTicketLocationEn() {
		return ticketLocationEn;
	}

	public void setTicketLocationEn(String ticketLocationEn) {
		this.ticketLocationEn = ticketLocationEn;
	}

	public List<EventImageModel> getEventImage() {
		return eventImage;
	}

	public void setEventImage(List<EventImageModel> eventImage) {
		this.eventImage = eventImage;
	}

	public List<OptionModel> getListOption() {
		return listOption;
	}

	public void setListOption(List<OptionModel> listOption) {
		this.listOption = listOption;
	}

	public List<OptionModel> getListField() {
		return listField;
	}

	public void setListField(List<OptionModel> listField) {
		this.listField = listField;
	}

	public List<TicketModel> getListTicket() {
		return listTicket;
	}

	public void setListTicket(List<TicketModel> listTicket) {
		this.listTicket = listTicket;
	}

	public List<TicketFieldModel> getListTicketField() {
		return listTicketField;
	}

	public void setListTicketField(List<TicketFieldModel> listTicketField) {
		this.listTicketField = listTicketField;
	}

	public List<EventSectionModel> getListEventSection() {
		return listEventSection;
	}

	public void setListEventSection(List<EventSectionModel> listEventSection) {
		this.listEventSection = listEventSection;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public List<ProvinceModel> getListProvince() {
		return listProvince;
	}

	public void setListProvince(List<ProvinceModel> listProvince) {
		this.listProvince = listProvince;
	}

	public List<ListboxModel> getListDay() {
		return listDay;
	}

	public void setListDay(List<ListboxModel> listDay) {
		this.listDay = listDay;
	}

	public List<ListboxModel> getListMonth() {
		return listMonth;
	}

	public void setListMonth(List<ListboxModel> listMonth) {
		this.listMonth = listMonth;
	}

	public List<ListboxModel> getListYear() {
		return listYear;
	}

	public void setListYear(List<ListboxModel> listYear) {
		this.listYear = listYear;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getAgeGroup() {
		return ageGroup;
	}

	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public List<EventPaymentGatewayModel> getListEventPaymentGateway() {
		return listEventPaymentGateway;
	}

	public void setListEventPaymentGateway(List<EventPaymentGatewayModel> listEventPaymentGateway) {
		this.listEventPaymentGateway = listEventPaymentGateway;
	}

	public List<EventModel> getListRecomend() {
		return listRecomend;
	}

	public void setListRecomend(List<EventModel> listRecomend) {
		this.listRecomend = listRecomend;
	}

	public List<EventModel> getListRace() {
		return listRace;
	}

	public void setListRace(List<EventModel> listRace) {
		this.listRace = listRace;
	}

	public List<EventModel> getListTrail() {
		return listTrail;
	}

	public void setListTrail(List<EventModel> listTrail) {
		this.listTrail = listTrail;
	}

	public List<EventModel> getListPost() {
		return listPost;
	}

	public void setListPost(List<EventModel> listPost) {
		this.listPost = listPost;
	}

	public List<EventTagModel> getListEventTag() {
		return listEventTag;
	}

	public void setListEventTag(List<EventTagModel> listEventTag) {
		this.listEventTag = listEventTag;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public String getWeaverInfo() {
		return weaverInfo;
	}

	public void setWeaverInfo(String weaverInfo) {
		this.weaverInfo = weaverInfo;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceDescTh() {
		return provinceDescTh;
	}

	public void setProvinceDescTh(String provinceDescTh) {
		this.provinceDescTh = provinceDescTh;
	}

	public String getProvinceDescEn() {
		return provinceDescEn;
	}

	public void setProvinceDescEn(String provinceDescEn) {
		this.provinceDescEn = provinceDescEn;
	}

	public String getEventTypeDescTh() {
		return eventTypeDescTh;
	}

	public void setEventTypeDescTh(String eventTypeDescTh) {
		this.eventTypeDescTh = eventTypeDescTh;
	}

	public String getEventTypeDescEn() {
		return eventTypeDescEn;
	}

	public void setEventTypeDescEn(String eventTypeDescEn) {
		this.eventTypeDescEn = eventTypeDescEn;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public List<PriceTypeModel> getListPriceType() {
		return listPriceType;
	}

	public void setListPriceType(List<PriceTypeModel> listPriceType) {
		this.listPriceType = listPriceType;
	}

	public List<String> getListDistance() {
		return listDistance;
	}

	public void setListDistance(List<String> listDistance) {
		this.listDistance = listDistance;
	}

	public EventSectionModel getGeneral() {
		return general;
	}

	public void setGeneral(EventSectionModel general) {
		this.general = general;
	}

	public EventSectionModel getFee() {
		return fee;
	}

	public void setFee(EventSectionModel fee) {
		this.fee = fee;
	}

	public EventSectionModel getBenefit() {
		return benefit;
	}

	public void setBenefit(EventSectionModel benefit) {
		this.benefit = benefit;
	}

	public EventSectionModel getReward() {
		return reward;
	}

	public void setReward(EventSectionModel reward) {
		this.reward = reward;
	}

	public String getRegisterType() {
		return registerType;
	}

	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}

	public String getEventMonth() {
		return eventMonth;
	}

	public void setEventMonth(String eventMonth) {
		this.eventMonth = eventMonth;
	}

	public String getPublish() {
		return publish;
	}

	public void setPublish(String publish) {
		this.publish = publish;
	}

	
	
	//ADD FIELD POINT//

	


	
}
