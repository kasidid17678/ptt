package com.app.ptt.notification.model;

import java.util.List;

public class LineEventModel {
	
	private String type;
	private String replyToken;
	private String timestamp;
	
	private LineSourceModel source;
	private LineMessageModel message;
	private List<LineMessageModel> messages;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReplyToken() {
		return replyToken;
	}
	public void setReplyToken(String replyToken) {
		this.replyToken = replyToken;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public LineSourceModel getSource() {
		return source;
	}
	public void setSource(LineSourceModel source) {
		this.source = source;
	}
	public LineMessageModel getMessage() {
		return message;
	}
	public void setMessage(LineMessageModel message) {
		this.message = message;
	}
	public List<LineMessageModel> getMessages() {
		return messages;
	}
	public void setMessages(List<LineMessageModel> messages) {
		this.messages = messages;
	}
	
	  
}
