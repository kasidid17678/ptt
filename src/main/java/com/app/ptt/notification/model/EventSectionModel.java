package com.app.ptt.notification.model;

import java.util.List;

public class EventSectionModel {

	private int eventSectionId;
	private int eventId;
	private String sectionName;
	private String mainHeader;
	private String detailHeader;
	private String status;
	private String mainHeaderEn;
	private String detailHeaderEn;
	
	List<EventSectionImage> listSectionImage;
	List<EventSectionContent> listSectionContent;
	
	private String languageStatus;
	
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getMainHeader() {
		return mainHeader;
	}
	public void setMainHeader(String mainHeader) {
		this.mainHeader = mainHeader;
	}
	public String getDetailHeader() {
		return detailHeader;
	}
	public void setDetailHeader(String detailHeader) {
		this.detailHeader = detailHeader;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<EventSectionImage> getListSectionImage() {
		return listSectionImage;
	}
	public void setListSectionImage(List<EventSectionImage> listSectionImage) {
		this.listSectionImage = listSectionImage;
	}
	public List<EventSectionContent> getListSectionContent() {
		return listSectionContent;
	}
	public void setListSectionContent(List<EventSectionContent> listSectionContent) {
		this.listSectionContent = listSectionContent;
	}
	public int getEventSectionId() {
		return eventSectionId;
	}
	public void setEventSectionId(int eventSectionId) {
		this.eventSectionId = eventSectionId;
	}
	public String getLanguageStatus() {
		return languageStatus;
	}
	public void setLanguageStatus(String languageStatus) {
		this.languageStatus = languageStatus;
	}
	public String getMainHeaderEn() {
		return mainHeaderEn;
	}
	public void setMainHeaderEn(String mainHeaderEn) {
		this.mainHeaderEn = mainHeaderEn;
	}
	public String getDetailHeaderEn() {
		return detailHeaderEn;
	}
	public void setDetailHeaderEn(String detailHeaderEn) {
		this.detailHeaderEn = detailHeaderEn;
	}
	
	
	
}
