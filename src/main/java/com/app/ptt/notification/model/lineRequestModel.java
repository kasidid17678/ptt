package com.app.ptt.notification.model;

import java.util.List;

public class lineRequestModel {
	
	private String to;
	private List<LineMessageModel> messages;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public List<LineMessageModel> getMessages() {
		return messages;
	}
	public void setMessages(List<LineMessageModel> messages) {
		this.messages = messages;
	}
	


}
