package com.app.ptt.notification.model;

import java.util.ArrayList;
import java.util.List;


public class TicketOptionModel {
	private long ticketOptionId;
	private long eventId;
	private long ticketId;
	private long optionId;
	private String ticketOptionStatus;
	private List<OptionDetailModel> listOptionDetail;
	private String fieldName;
	private String fieldLabel;
	private String fieldPlaceholder;
	private String fieldLimit;
	private String fieldType;
	private String fieldItemSeq;
	private String field;
	private String fieldRequired;
////////////////

	private String fieldLabelEn;
	private String fieldPlaceholderEn;
	private String shirtField;
	 
	public long getTicketOptionId() {
		return ticketOptionId;
	}
	public void setTicketOptionId(long ticketOptionId) {
		this.ticketOptionId = ticketOptionId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public long getOptionId() {
		return optionId;
	}
	public void setOptionId(long optionId) {
		this.optionId = optionId;
	}
	public String getTicketOptionStatus() {
		return ticketOptionStatus;
	}
	public void setTicketOptionStatus(String ticketOptionStatus) {
		this.ticketOptionStatus = ticketOptionStatus;
	}
	public List<OptionDetailModel> getListOptionDetail() {
		return listOptionDetail;
	}
	public void setListOptionDetail(List<OptionDetailModel> listOptionDetail) {
		this.listOptionDetail = listOptionDetail;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldItemSeq() {
		return fieldItemSeq;
	}
	public void setFieldItemSeq(String fieldItemSeq) {
		this.fieldItemSeq = fieldItemSeq;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getFieldRequired() {
		return fieldRequired;
	}
	public void setFieldRequired(String fieldRequired) {
		this.fieldRequired = fieldRequired;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public String getFieldPlaceholder() {
		return fieldPlaceholder;
	}
	public void setFieldPlaceholder(String fieldPlaceholder) {
		this.fieldPlaceholder = fieldPlaceholder;
	}
	public String getFieldLimit() {
		return fieldLimit;
	}
	public void setFieldLimit(String fieldLimit) {
		this.fieldLimit = fieldLimit;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getFieldLabelEn() {
		return fieldLabelEn;
	}
	public void setFieldLabelEn(String fieldLabelEn) {
		this.fieldLabelEn = fieldLabelEn;
	}
	public String getFieldPlaceholderEn() {
		return fieldPlaceholderEn;
	}
	public void setFieldPlaceholderEn(String fieldPlaceholderEn) {
		this.fieldPlaceholderEn = fieldPlaceholderEn;
	}
	public String getShirtField() {
		return shirtField;
	}
	public void setShirtField(String shirtField) {
		this.shirtField = shirtField;
	}
	
}
