package com.app.ptt.notification.model;


public class TicketFieldModel {
	private long ticketFieldId;
	private long eventId;
	private long ticketId;
	private String fieldName;
	private String fieldLabel;
	private String fieldPlaceholder;
	private int fieldLimit;
	private String fieldType;
	private int fieldRequired;
	private int fieldItemSeq;
	
	public long getTicketFieldId() {
		return ticketFieldId;
	}
	public void setTicketFieldId(long ticketFieldId) {
		this.ticketFieldId = ticketFieldId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public String getFieldPlaceholder() {
		return fieldPlaceholder;
	}
	public void setFieldPlaceholder(String fieldPlaceholder) {
		this.fieldPlaceholder = fieldPlaceholder;
	}
	public int getFieldLimit() {
		return fieldLimit;
	}
	public void setFieldLimit(int fieldLimit) {
		this.fieldLimit = fieldLimit;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public int getFieldRequired() {
		return fieldRequired;
	}
	public void setFieldRequired(int fieldRequired) {
		this.fieldRequired = fieldRequired;
	}
	public int getFieldItemSeq() {
		return fieldItemSeq;
	}
	public void setFieldItemSeq(int fieldItemSeq) {
		this.fieldItemSeq = fieldItemSeq;
	}

}
