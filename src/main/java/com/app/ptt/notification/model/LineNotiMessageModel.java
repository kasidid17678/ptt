package com.app.ptt.notification.model;

public class LineNotiMessageModel {

	 private int lineNotiMessageId;
	 private String messageCode;
	 private String messageSubject;
	 private String messageDesc;
	 private String message;
	 private String messageStatus;
	 private String createDate;
	 private String createBy;
	 private String lineNotiStatus;
	 
	public int getLineNotiMessageId() {
		return lineNotiMessageId;
	}
	public void setLineNotiMessageId(int lineNotiMessageId) {
		this.lineNotiMessageId = lineNotiMessageId;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessageSubject() {
		return messageSubject;
	}
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	public String getMessageDesc() {
		return messageDesc;
	}
	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageStatus() {
		return messageStatus;
	}
	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getLineNotiStatus() {
		return lineNotiStatus;
	}
	public void setLineNotiStatus(String lineNotiStatus) {
		this.lineNotiStatus = lineNotiStatus;
	}
	
	

}
