package com.app.ptt.notification.model;

public class ListboxModel {
 
 private int listboxId;
 private String name;
 private String description;
 private String value1;
 private String value2;
 private String value3;
 private String value4;
 private String listboxGroup;
 private String listboxType;
 private String status;
 private String createDate;
 private String createBy;
 private String updateDate;
 private String updateBy;
 private String remark;
 private int seq;
 //////////////////

	private int count;
	private String imageUrl;
	private String fileId;
	private String fileName;
 

 public int getListboxId() {
		return listboxId;
	}
	public void setListboxId(int listboxId) {
		this.listboxId = listboxId;
	}
public String getName() {
  return name;
 }
 public void setName(String name) {
  this.name = name;
 }
 public String getDescription() {
  return description;
 }
 public void setDescription(String description) {
  this.description = description;
 }
 public String getValue1() {
  return value1;
 }
 public void setValue1(String value1) {
  this.value1 = value1;
 }
 public String getValue2() {
  return value2;
 }
 public void setValue2(String value2) {
  this.value2 = value2;
 }
 public String getValue3() {
  return value3;
 }
 public void setValue3(String value3) {
  this.value3 = value3;
 }
 public String getValue4() {
  return value4;
 }
 public void setValue4(String value4) {
  this.value4 = value4;
 }
 public String getListboxGroup() {
  return listboxGroup;
 }
 public void setListboxGroup(String listboxGroup) {
  this.listboxGroup = listboxGroup;
 }
 public String getListboxType() {
  return listboxType;
 }
 public void setListboxType(String listboxType) {
  this.listboxType = listboxType;
 }
 public String getStatus() {
  return status;
 }
 public void setStatus(String status) {
  this.status = status;
 }
 public String getCreateDate() {
  return createDate;
 }
 public void setCreateDate(String createDate) {
  this.createDate = createDate;
 }
 public String getUpdateDate() {
  return updateDate;
 }
 public void setUpdateDate(String updateDate) {
  this.updateDate = updateDate;
 }
 public String getCreateBy() {
  return createBy;
 }
 public void setCreateBy(String createBy) {
  this.createBy = createBy;
 }
 public String getUpdateBy() {
  return updateBy;
 }
 public void setUpdateBy(String updateBy) {
  this.updateBy = updateBy;
 }
 public String getRemark() {
  return remark;
 }
 public void setRemark(String remark) {
  this.remark = remark;
 }
 public int getSeq() {
  return seq;
 }
 public void setSeq(int seq) {
  this.seq = seq;
 }
public int getCount() {
	return count;
}
public void setCount(int count) {
	this.count = count;
}
public String getImageUrl() {
	return imageUrl;
}
public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
}
public String getFileId() {
	return fileId;
}
public void setFileId(String fileId) {
	this.fileId = fileId;
}
public String getFileName() {
	return fileName;
}
public void setFileName(String fileName) {
	this.fileName = fileName;
}

 
}