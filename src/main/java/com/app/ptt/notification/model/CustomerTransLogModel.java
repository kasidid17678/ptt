package com.app.ptt.notification.model;

public class CustomerTransLogModel {
	
	private int customerTransLogId;
	private int customerProfileId;
	private int promotionId;
	private String promotionType;
	private int campaignPointId;
	private String transactionCode;
	private String transType;
	private int pointDeposit;
	private int pointWithdraw;
	private int currentPoint;
	private int userId ;
	private int shopId;
	private String status;
	private String pointDesc;
	private String promotionCode;
	private String createBy;
	private String createDate;
	private String serialCode ;
	private String updateBy;
	private String promotionName;
	private String campaignName;
	private String point;
	
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public int getCustomerTransLogId() {
		return customerTransLogId;
	}
	public void setCustomerTransLogId(int customerTransLogId) {
		this.customerTransLogId = customerTransLogId;
	}
	public int getCustomerProfileId() {
		return customerProfileId;
	}
	public void setCustomerProfileId(int customerProfileId) {
		this.customerProfileId = customerProfileId;
	}
	public int getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	public String getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	public int getCampaignPointId() {
		return campaignPointId;
	}
	public void setCampaignPointId(int campaignPointId) {
		this.campaignPointId = campaignPointId;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public int getPointDeposit() {
		return pointDeposit;
	}
	public void setPointDeposit(int pointDeposit) {
		this.pointDeposit = pointDeposit;
	}
	public int getPointWithdraw() {
		return pointWithdraw;
	}
	public void setPointWithdraw(int pointWithdraw) {
		this.pointWithdraw = pointWithdraw;
	}
	public int getCurrentPoint() {
		return currentPoint;
	}
	public void setCurrentPoint(int currentPoint) {
		this.currentPoint = currentPoint;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPointDesc() {
		return pointDesc;
	}
	public void setPointDesc(String pointDesc) {
		this.pointDesc = pointDesc;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getSerialCode() {
		return serialCode;
	}
	public void setSerialCode(String serialCode) {
		this.serialCode = serialCode;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	

}
