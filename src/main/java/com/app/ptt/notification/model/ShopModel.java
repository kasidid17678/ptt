package com.app.ptt.notification.model;

import java.util.List;

public class ShopModel {
	
	private int shopId;
	private String shopName;
	private String ownerName;
	private String ownerLastname;
	private String officerTel;
	private String shopType;
	private String address;
	private String subDistrict;
	private String district;
	private String province;
	private String zipcode;
	private String status;
	private String remark;
	private String createDate;
	private String createBy;
	private String updateDate;
	private String updateBy;
	private String email;
	private String lineOfficialAcc;
	private String lineOfficialAccQr;
	private String lineNotificationToken;
	private List<ShopLineNotiModel> listLineNoti;
	
	private String qrImage;
	private String shopBanner;
	private String shopIcon;
	
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOwnerLastname() {
		return ownerLastname;
	}
	public void setOwnerLastname(String ownerLastname) {
		this.ownerLastname = ownerLastname;
	}
	public String getOfficerTel() {
		return officerTel;
	}
	public void setOfficerTel(String officerTel) {
		this.officerTel = officerTel;
	}
	public String getShopType() {
		return shopType;
	}
	public void setShopType(String shopType) {
		this.shopType = shopType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSubDistrict() {
		return subDistrict;
	}
	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLineOfficialAcc() {
		return lineOfficialAcc;
	}
	public void setLineOfficialAcc(String lineOfficialAcc) {
		this.lineOfficialAcc = lineOfficialAcc;
	}
	public String getLineOfficialAccQr() {
		return lineOfficialAccQr;
	}
	public void setLineOfficialAccQr(String lineOfficialAccQr) {
		this.lineOfficialAccQr = lineOfficialAccQr;
	}
	public String getLineNotificationToken() {
		return lineNotificationToken;
	}
	public void setLineNotificationToken(String lineNotificationToken) {
		this.lineNotificationToken = lineNotificationToken;
	}
	public List<ShopLineNotiModel> getListLineNoti() {
		return listLineNoti;
	}
	public void setListLineNoti(List<ShopLineNotiModel> listLineNoti) {
		this.listLineNoti = listLineNoti;
	}
	public String getQrImage() {
		return qrImage;
	}
	public void setQrImage(String qrImage) {
		this.qrImage = qrImage;
	}
	public String getShopBanner() {
		return shopBanner;
	}
	public void setShopBanner(String shopBanner) {
		this.shopBanner = shopBanner;
	}
	public String getShopIcon() {
		return shopIcon;
	}
	public void setShopIcon(String shopIcon) {
		this.shopIcon = shopIcon;
	}
	

}
