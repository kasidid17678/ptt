package com.app.ptt.notification.model;

public class EventSectionContent {
	
	private int eventSectionContentId;
	private int eventSectionId;
	private int eventId;
	private String content;
	private String contentEn;
	private String status;
	private int seq;
	private String languageStatus;
	
	public int getEventSectionContentId() {
		return eventSectionContentId;
	}
	public void setEventSectionContentId(int eventSectionContentId) {
		this.eventSectionContentId = eventSectionContentId;
	}
	public int getEventSectionId() {
		return eventSectionId;
	}
	public void setEventSectionId(int eventSectionId) {
		this.eventSectionId = eventSectionId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getLanguageStatus() {
		return languageStatus;
	}
	public void setLanguageStatus(String languageStatus) {
		this.languageStatus = languageStatus;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getContentEn() {
		return contentEn;
	}
	public void setContentEn(String contentEn) {
		this.contentEn = contentEn;
	}
	
	
}
