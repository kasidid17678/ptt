package com.app.ptt.notification.model;


public class EventImageModel {
	private long eventImageId;
	private long eventId;
	private long fileId;
	private String imageType;
	private String imageName;
	private String imageUrl;
	private String statusDesc;
	private String imageTypeDesc;
	private String Status;
	private long fileSize;
	private String provinceCode;
	
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getImageTypeDesc() {
		return imageTypeDesc;
	}
	public void setImageTypeDesc(String imageTypeDesc) {
		this.imageTypeDesc = imageTypeDesc;
	}
	public long getEventImageId() {
		return eventImageId;
	}
	public void setEventImageId(long eventImageId) {
		this.eventImageId = eventImageId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	
	
	
	
}
