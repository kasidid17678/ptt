package com.app.ptt.notification.model;

public class pttModel {

	private int id	;
	private String pttId	;
	private String firstName	;
	private String lastName	;
	private String email	;
	private String googleDriveUrl	;
	private String status	;
	private String createDate	;
	private String createBy	;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPttId() {
		return pttId;
	}
	public void setPttId(String pttId) {
		this.pttId = pttId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGoogleDriveUrl() {
		return googleDriveUrl;
	}
	public void setGoogleDriveUrl(String googleDriveUrl) {
		this.googleDriveUrl = googleDriveUrl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	
	
}
