package com.app.ptt.notification.model;

import java.util.List;

public class TicketCountModel {

	private long ticketId;
	private int count;
	private String eventNameTh;
	private String eventNameEn;
	private String ticketNameTh;
	private String ticketNameEn;
	private long netAmt;
	private long ticketPrice;
	private String netAmtDesc;
	
	private List<String> listName;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getEventNameTh() {
		return eventNameTh;
	}
	public void setEventNameTh(String eventNameTh) {
		this.eventNameTh = eventNameTh;
	}
	public String getEventNameEn() {
		return eventNameEn;
	}
	public void setEventNameEn(String eventNameEn) {
		this.eventNameEn = eventNameEn;
	}
	public String getTicketNameTh() {
		return ticketNameTh;
	}
	public void setTicketNameTh(String ticketNameTh) {
		this.ticketNameTh = ticketNameTh;
	}
	public String getTicketNameEn() {
		return ticketNameEn;
	}
	public void setTicketNameEn(String ticketNameEn) {
		this.ticketNameEn = ticketNameEn;
	}

	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public long getNetAmt() {
		return netAmt;
	}
	public void setNetAmt(long netAmt) {
		this.netAmt = netAmt;
	}
	public long getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(long ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public String getNetAmtDesc() {
		return netAmtDesc;
	}
	public void setNetAmtDesc(String netAmtDesc) {
		this.netAmtDesc = netAmtDesc;
	}
	public List<String> getListName() {
		return listName;
	}
	public void setListName(List<String> listName) {
		this.listName = listName;
	}
	
	
	
	
}
