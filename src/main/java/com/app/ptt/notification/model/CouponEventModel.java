package com.app.ptt.notification.model;

public class CouponEventModel {

	private int couponEventId;
	private int couponId;
	private int eventId;
	
	private String status;
	private String createDate;
	private String createBy;
	private String updateDate;
	private String updateBy;
	private Boolean statusBoolean;
	public int getCouponEventId() {
		return couponEventId;
	}
	public void setCouponEventId(int couponEventId) {
		this.couponEventId = couponEventId;
	}
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Boolean getStatusBoolean() {
		return statusBoolean;
	}
	public void setStatusBoolean(Boolean statusBoolean) {
		this.statusBoolean = statusBoolean;
	}
	
	
	
}
