package com.app.ptt.notification.model;

public class CouponCodeModel {
	private int couponCodeId;
	private int couponId;
	private String couponCode;
	private String status;
	private String createDate;
	private String createBy;
	private int eventId;
	
	public int getCouponCodeId() {
		return couponCodeId;
	}
	public void setCouponCodeId(int couponCodeId) {
		this.couponCodeId = couponCodeId;
	}
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	
	
	
}
