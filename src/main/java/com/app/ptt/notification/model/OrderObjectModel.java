package com.app.ptt.notification.model;


public class OrderObjectModel {
	
	private long orderObjectId;
	private long orderDetailId;
	private long orderId;
	private String objectType;
	private long ticketFieldId;
	private long optionId;
	private String objectValue;
	private String description;
	private String objectStatus;
	
	private String fieldName;
	private String fieldValue;
	private String optionDetailName;
	private String optionDetailNameEn;
	private String shirtField;
	
	
	public String getShirtField() {
		return shirtField;
	}
	public void setShirtField(String shirtField) {
		this.shirtField = shirtField;
	}
	public long getOrderObjectId() {
		return orderObjectId;
	}
	public void setOrderObjectId(long orderObjectId) {
		this.orderObjectId = orderObjectId;
	}
	public long getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public long getTicketFieldId() {
		return ticketFieldId;
	}
	public void setTicketFieldId(long ticketFieldId) {
		this.ticketFieldId = ticketFieldId;
	}
	public long getOptionId() {
		return optionId;
	}
	public void setOptionId(long optionId) {
		this.optionId = optionId;
	}
	public String getObjectValue() {
		return objectValue;
	}
	public void setObjectValue(String objectValue) {
		this.objectValue = objectValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getObjectStatus() {
		return objectStatus;
	}
	public void setObjectStatus(String objectStatus) {
		this.objectStatus = objectStatus;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getOptionDetailName() {
		return optionDetailName;
	}
	public void setOptionDetailName(String optionDetailName) {
		this.optionDetailName = optionDetailName;
	}
	public String getOptionDetailNameEn() {
		return optionDetailNameEn;
	}
	public void setOptionDetailNameEn(String optionDetailNameEn) {
		this.optionDetailNameEn = optionDetailNameEn;
	}
	
	
//	private long orderObjectId;
//	private long orderDetailId;
//	private long orderId;
//	private String objectType;
//	private long ticketFieldId;
//	private long optionId;
//	private String objectValue;
//	private String description;
//	private String objectStatus;
//	
//	public long getOrderObjectId() {
//		return orderObjectId;
//	}
//	public void setOrderObjectId(long orderObjectId) {
//		this.orderObjectId = orderObjectId;
//	}
//	public long getOrderDetailId() {
//		return orderDetailId;
//	}
//	public void setOrderDetailId(long orderDetailId) {
//		this.orderDetailId = orderDetailId;
//	}
//	public long getOrderId() {
//		return orderId;
//	}
//	public void setOrderId(long orderId) {
//		this.orderId = orderId;
//	}
//	public String getObjectType() {
//		return objectType;
//	}
//	public void setObjectType(String objectType) {
//		this.objectType = objectType;
//	}
//	
//	public long getOptionId() {
//		return optionId;
//	}
//	public void setOptionId(long optionId) {
//		this.optionId = optionId;
//	}
//	public String getObjectValue() {
//		return objectValue;
//	}
//	public void setObjectValue(String objectValue) {
//		this.objectValue = objectValue;
//	}
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	public String getObjectStatus() {
//		return objectStatus;
//	}
//	public void setObjectStatus(String objectStatus) {
//		this.objectStatus = objectStatus;
//	}
//	public long getTicketFieldId() {
//		return ticketFieldId;
//	}
//	public void setTicketFieldId(long ticketFieldId) {
//		this.ticketFieldId = ticketFieldId;
//	}

}
