package com.app.ptt.notification.model;

public class EventPaymentGatewayModel {

	private long eventPaymentGatewayId;
	private long eventId;
	private String paymentGateway;
	private double platformFee;
	private String platformUnit;
	private String gatewayStatus;
	private String createDate;
	private String createBy;
	public long getEventPaymentGatewayId() {
		return eventPaymentGatewayId;
	}
	public void setEventPaymentGatewayId(long eventPaymentGatewayId) {
		this.eventPaymentGatewayId = eventPaymentGatewayId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public String getPaymentGateway() {
		return paymentGateway;
	}
	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}
	public double getPlatformFee() {
		return platformFee;
	}
	public void setPlatformFee(double platformFee) {
		this.platformFee = platformFee;
	}
	public String getPlatformUnit() {
		return platformUnit;
	}
	public void setPlatformUnit(String platformUnit) {
		this.platformUnit = platformUnit;
	}
	public String getGatewayStatus() {
		return gatewayStatus;
	}
	public void setGatewayStatus(String gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	

	
}
