package com.app.ptt.notification.model;

import java.util.List;


public class TicketModel {
	private long ticketId;
	private long eventId;
	private String ticketType;
	private String ticketNameTh;
	private String ticketNameEn;
	private String ticketDesc;
	private int ticketAmount;
	private String startTime;
	private String endTime;
	private int addressStatus;
	private TicketOptionModel ticketOption;
	private TicketPriceModel ticketPrice;
	private List<TicketPriceModel> listTicketPrice;
	private List<TicketOptionModel> listTicketField;
	private List<TicketOptionModel> listTicketOption;
	private String generateCode;
	////////////////

	private String ticketStatus;
	private Double distance;
	private String statusDesc;
	private String ticketNameThDesc;
	private String ticketNameEnDesc;
	
	private String priceStartDate;
	private String priceEndDate;
	private String priceStartTime;
	private String priceEndTime;
	private String priceTypeDesc;
	private String priceType;
	
	private String price;
	private String updateType;
	
	private int balanceTicket;
	
	private List<TicketAgeGroupModel> listTicketAgeGroup;
	
	public TicketModel() {
		
	}

	public TicketModel(long eventId) {
		this.eventId = eventId;
	}
	
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public String getTicketNameTh() {
		return ticketNameTh;
	}
	public void setTicketNameTh(String ticketNameTh) {
		this.ticketNameTh = ticketNameTh;
	}
	public String getTicketNameEn() {
		return ticketNameEn;
	}
	public void setTicketNameEn(String ticketNameEn) {
		this.ticketNameEn = ticketNameEn;
	}
	public String getTicketDesc() {
		return ticketDesc;
	}
	public void setTicketDesc(String ticketDesc) {
		this.ticketDesc = ticketDesc;
	}
	public int getTicketAmount() {
		return ticketAmount;
	}
	public void setTicketAmount(int ticketAmount) {
		this.ticketAmount = ticketAmount;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getAddressStatus() {
		return addressStatus;
	}
	public void setAddressStatus(int addressStatus) {
		this.addressStatus = addressStatus;
	}
	public TicketOptionModel getTicketOption() {
		return ticketOption;
	}
	public void setTicketOption(TicketOptionModel ticketOption) {
		this.ticketOption = ticketOption;
	}
	public TicketPriceModel getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(TicketPriceModel ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public List<TicketPriceModel> getListTicketPrice() {
		return listTicketPrice;
	}
	public void setListTicketPrice(List<TicketPriceModel> listTicketPrice) {
		this.listTicketPrice = listTicketPrice;
	}
	public String getGenerateCode() {
		return generateCode;
	}
	public void setGenerateCode(String generateCode) {
		this.generateCode = generateCode;
	}


	public List<TicketOptionModel> getListTicketOption() {
		return listTicketOption;
	}

	public void setListTicketOption(List<TicketOptionModel> listTicketOption) {
		this.listTicketOption = listTicketOption;
	}

	public List<TicketOptionModel> getListTicketField() {
		return listTicketField;
	}

	public void setListTicketField(List<TicketOptionModel> listTicketField) {
		this.listTicketField = listTicketField;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public String getTicketNameThDesc() {
		return ticketNameThDesc;
	}

	public void setTicketNameThDesc(String ticketNameThDesc) {
		this.ticketNameThDesc = ticketNameThDesc;
	}

	public String getTicketNameEnDesc() {
		return ticketNameEnDesc;
	}

	public void setTicketNameEnDesc(String ticketNameEnDesc) {
		this.ticketNameEnDesc = ticketNameEnDesc;
	}

	public String getPriceStartDate() {
		return priceStartDate;
	}

	public void setPriceStartDate(String priceStartDate) {
		this.priceStartDate = priceStartDate;
	}

	public String getPriceEndDate() {
		return priceEndDate;
	}

	public void setPriceEndDate(String priceEndDate) {
		this.priceEndDate = priceEndDate;
	}

	public String getPriceStartTime() {
		return priceStartTime;
	}

	public void setPriceStartTime(String priceStartTime) {
		this.priceStartTime = priceStartTime;
	}

	public String getPriceEndTime() {
		return priceEndTime;
	}

	public void setPriceEndTime(String priceEndTime) {
		this.priceEndTime = priceEndTime;
	}

	public String getPriceTypeDesc() {
		return priceTypeDesc;
	}

	public void setPriceTypeDesc(String priceTypeDesc) {
		this.priceTypeDesc = priceTypeDesc;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public int getBalanceTicket() {
		return balanceTicket;
	}

	public void setBalanceTicket(int balanceTicket) {
		this.balanceTicket = balanceTicket;
	}

	public List<TicketAgeGroupModel> getListTicketAgeGroup() {
		return listTicketAgeGroup;
	}

	public void setListTicketAgeGroup(List<TicketAgeGroupModel> listTicketAgeGroup) {
		this.listTicketAgeGroup = listTicketAgeGroup;
	}



//	public String getPriceType() {
//		return priceType;
//	}
//
//	public void setPriceType(String priceType) {
//		this.priceType = priceType;
//	}
	
	
}
