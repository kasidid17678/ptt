package com.app.ptt.notification.model;

import java.math.BigDecimal;
import java.sql.Date;

public class TicketPriceModel {
	private long ticketPriceId;
	private long eventId;
	private long ticketId;
	private String priceType;
	private Date startDate;
	private Date endDate;
	private BigDecimal price;
	private BigDecimal platformFee;
	private BigDecimal paymentGatewayFee;
	private BigDecimal vat;
	private String priceStatus;
	private int ticketAmount;
	private long oldTicketPriceId;
	private int oldTicketAmount;
	///////////////
	
	private String priceTypeDesc;
	private String startTime;
	private String endTime;
	
	private String oldPriceStatus;
	
	private String updateType;
	private int balanceTicket;
	
	public long getTicketPriceId() {
		return ticketPriceId;
	}
	public void setTicketPriceId(long ticketPriceId) {
		this.ticketPriceId = ticketPriceId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public String getPriceType() {
		return priceType;
	}
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPlatformFee() {
		return platformFee;
	}
	public void setPlatformFee(BigDecimal platformFee) {
		this.platformFee = platformFee;
	}
	public BigDecimal getPaymentGatewayFee() {
		return paymentGatewayFee;
	}
	public void setPaymentGatewayFee(BigDecimal paymentGatewayFee) {
		this.paymentGatewayFee = paymentGatewayFee;
	}
	public BigDecimal getVat() {
		return vat;
	}
	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}
	public String getPriceStatus() {
		return priceStatus;
	}
	public void setPriceStatus(String priceStatus) {
		this.priceStatus = priceStatus;
	}
	public int getTicketAmount() {
		return ticketAmount;
	}
	public void setTicketAmount(int ticketAmount) {
		this.ticketAmount = ticketAmount;
	}
	public long getOldTicketPriceId() {
		return oldTicketPriceId;
	}
	public void setOldTicketPriceId(long oldTicketPriceId) {
		this.oldTicketPriceId = oldTicketPriceId;
	}
	public int getOldTicketAmount() {
		return oldTicketAmount;
	}
	public void setOldTicketAmount(int oldTicketAmount) {
		this.oldTicketAmount = oldTicketAmount;
	}
	public String getPriceTypeDesc() {
		return priceTypeDesc;
	}
	public void setPriceTypeDesc(String priceTypeDesc) {
		this.priceTypeDesc = priceTypeDesc;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getOldPriceStatus() {
		return oldPriceStatus;
	}
	public void setOldPriceStatus(String oldPriceStatus) {
		this.oldPriceStatus = oldPriceStatus;
	}
	public String getUpdateType() {
		return updateType;
	}
	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}
	public int getBalanceTicket() {
		return balanceTicket;
	}
	public void setBalanceTicket(int balanceTicket) {
		this.balanceTicket = balanceTicket;
	}

	
	
}
