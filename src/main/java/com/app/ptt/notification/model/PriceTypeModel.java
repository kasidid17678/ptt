package com.app.ptt.notification.model;

import java.util.List;

public class PriceTypeModel {

	private String priceType;
	private String priceTypeDesc;
	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	
	
	private List<TicketModel> listTicket;
	
	public String getPriceType() {
		return priceType;
	}
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}
	public List<TicketModel> getListTicket() {
		return listTicket;
	}
	public void setListTicket(List<TicketModel> listTicket) {
		this.listTicket = listTicket;
	}
	public String getPriceTypeDesc() {
		return priceTypeDesc;
	}
	public void setPriceTypeDesc(String priceTypeDesc) {
		this.priceTypeDesc = priceTypeDesc;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
	
}
