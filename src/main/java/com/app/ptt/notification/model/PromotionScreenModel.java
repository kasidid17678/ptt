package com.app.ptt.notification.model;

import java.util.List;



public class PromotionScreenModel {
	private int shopId;
	private int promotionId;
	private int promotionImageId;
	private int promotionContentId;
	private String content;
	private String imageName;
	private String imageDesc;
	private String imageType;
	private String imageUrl;
	private String screenType; // IMAGE LIST_IMAGE CONTENT
	private int promotionScreenId;
	private List<PromotionImageModel> listImage; // ONLY SCREEN_TYPE "LIST_IMAGE"

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public int getPromotionImageId() {
		return promotionImageId;
	}

	public void setPromotionImageId(int promotionImageId) {
		this.promotionImageId = promotionImageId;
	}

	public int getPromotionContentId() {
		return promotionContentId;
	}

	public void setPromotionContentId(int promotionContentId) {
		this.promotionContentId = promotionContentId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageDesc() {
		return imageDesc;
	}

	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getScreenType() {
		return screenType;
	}

	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public List<PromotionImageModel> getListImage() {
		return listImage;
	}

	public void setListImage(List<PromotionImageModel> listImage) {
		this.listImage = listImage;
	}

	public int getPromotionScreenId() {
		return promotionScreenId;
	}

	public void setPromotionScreenId(int promotionScreenId) {
		this.promotionScreenId = promotionScreenId;
	}
	
}
