package com.app.ptt.notification.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;



public class OrderModel {
	private long orderId;
	private long userId;
	private String orderNumber;
	private int totalOrder;
	private BigDecimal amount;

	private BigDecimal vat;
	private BigDecimal paymentGatewayFee;
	private String shippingStatus;
	private BigDecimal shippingAmount;

	private long userAddressId;
	private BigDecimal netAmount;
	private String paymentChannel;
	private String orderStatus;
	private String orderStatusDesc;
	private String qrCode;
	private String qrPaymentCode;
	private List<OrderDetailModel> orderDetail;
	private int eventId;
	private String eventName;
	private int ticketId;
	private String imageSlip;
	private String insuranceStatus;
	private String invoiceNo;
	private String comment;
	private Date dateFrom;
	private Date dateTo;
	
	private String createDate;
	private String paymentDate;
	private String amountDesc;
	private String shippingAmountDesc;
	private String netAmountDesc;
	private String shippingStatusDesc;
	
	private String actionBy;

	// FOR SEARCH CRITERIA //
	
	private String tel;
	private String customerName;
	private String email;

	private int couponId;
	private int couponCodeId;
	private String couponCode;
	private String couponMode;
	private double discountAmt;
	private String discountAmtDesc;
	private String address;
	private String provinceDesc;
	private String districtDesc;
	private String subDistrictDesc;
	private String zipcode;
	
	private List<EventImageModel> eventImage;
	
	private String eventImageUrl;
	
	
	private String organizationName;
	private String provinceName;
	private String eventDate;
	
	
	
	private List<String> listDistance;
	
////////////////////////////

	private String orderStatusDescEn;

	private String eventNameEn;
	
	private String provinceDescEn;
	private String districtDescEn;
	private String subDistrictDescEn;
	
	private EventModel eventModel;
	private String eventUrl;
	private String eventBannerUrl;
	
	private String imageName;
	
	private String ticketName;
	private String ticketLocation;
	private String qrPaymentUrl;
	private String paymentExpireDate;
	private int emailId;
	private List<TicketCountModel> listTicketCount;
	
	private String couponDiscount;
	private CouponModel couponModel; 
	private CouponCodeModel couponCodeModel; 
	
	private String paymentGatewayFeeUnit; 
	private BigDecimal paymentGatewayFeeAmount;
	
	private PaymentDirectTransModel paymentDirectTransModel;
	private String unitRate;
	
	private double totalAmount;
	
	private long fileId;
	
	private String couponType;
	private double discountAmount;
	
	private String racepackStatus;
	
	
	private String registerType;
	
	private String orderGenerateCode;
	
	private String trackingNumber;
	private String trackingProvider;
	
	private String refCode;
	private String createBy;
	private String updateDate;
	private String updateBy;
	private String qrCodePath;
	
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getVat() {
		return vat;
	}
	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}
	public BigDecimal getPaymentGatewayFee() {
		return paymentGatewayFee;
	}
	public void setPaymentGatewayFee(BigDecimal paymentGatewayFee) {
		this.paymentGatewayFee = paymentGatewayFee;
	}
	public String getShippingStatus() {
		return shippingStatus;
	}
	public void setShippingStatus(String shippingStatus) {
		this.shippingStatus = shippingStatus;
	}
	public BigDecimal getShippingAmount() {
		return shippingAmount;
	}
	public void setShippingAmount(BigDecimal shippingAmount) {
		this.shippingAmount = shippingAmount;
	}
	public long getUserAddressId() {
		return userAddressId;
	}
	public void setUserAddressId(long userAddressId) {
		this.userAddressId = userAddressId;
	}
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}
	public String getPaymentChannel() {
		return paymentChannel;
	}
	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public String getQrPaymentCode() {
		return qrPaymentCode;
	}
	public void setQrPaymentCode(String qrPaymentCode) {
		this.qrPaymentCode = qrPaymentCode;
	}

	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public String getOrderStatusDesc() {
		return orderStatusDesc;
	}
	public void setOrderStatusDesc(String orderStatusDesc) {
		this.orderStatusDesc = orderStatusDesc;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getImageSlip() {
		return imageSlip;
	}
	public void setImageSlip(String imageSlip) {
		this.imageSlip = imageSlip;
	}
	public String getInsuranceStatus() {
		return insuranceStatus;
	}
	public void setInsuranceStatus(String insuranceStatus) {
		this.insuranceStatus = insuranceStatus;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getAmountDesc() {
		return amountDesc;
	}
	public void setAmountDesc(String amountDesc) {
		this.amountDesc = amountDesc;
	}
	public String getNetAmountDesc() {
		return netAmountDesc;
	}
	public void setNetAmountDesc(String netAmountDesc) {
		this.netAmountDesc = netAmountDesc;
	}
	public String getShippingAmountDesc() {
		return shippingAmountDesc;
	}
	public void setShippingAmountDesc(String shippingAmountDesc) {
		this.shippingAmountDesc = shippingAmountDesc;
	}
	public String getShippingStatusDesc() {
		return shippingStatusDesc;
	}
	public void setShippingStatusDesc(String shippingStatusDesc) {
		this.shippingStatusDesc = shippingStatusDesc;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public List<OrderDetailModel> getOrderDetail() {
		return orderDetail;
	}
	public void setOrderDetail(List<OrderDetailModel> orderDetail) {
		this.orderDetail = orderDetail;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getCouponDiscount() {
		return couponDiscount;
	}
	public void setCouponDiscount(String couponDiscount) {
		this.couponDiscount = couponDiscount;
	}
	public String getTicketLocation() {
		return ticketLocation;
	}
	public void setTicketLocation(String ticketLocation) {
		this.ticketLocation = ticketLocation;
	}
	public int getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(int totalOrder) {
		this.totalOrder = totalOrder;
	}
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public int getCouponCodeId() {
		return couponCodeId;
	}
	public void setCouponCodeId(int couponCodeId) {
		this.couponCodeId = couponCodeId;
	}
	public String getCouponMode() {
		return couponMode;
	}
	public void setCouponMode(String couponMode) {
		this.couponMode = couponMode;
	}
	public double getDiscountAmt() {
		return discountAmt;
	}
	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}
	public String getDiscountAmtDesc() {
		return discountAmtDesc;
	}
	public void setDiscountAmtDesc(String discountAmtDesc) {
		this.discountAmtDesc = discountAmtDesc;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvinceDesc() {
		return provinceDesc;
	}
	public void setProvinceDesc(String provinceDesc) {
		this.provinceDesc = provinceDesc;
	}
	public String getDistrictDesc() {
		return districtDesc;
	}
	public void setDistrictDesc(String districtDesc) {
		this.districtDesc = districtDesc;
	}
	public String getSubDistrictDesc() {
		return subDistrictDesc;
	}
	public void setSubDistrictDesc(String subDistrictDesc) {
		this.subDistrictDesc = subDistrictDesc;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getEventUrl() {
		return eventUrl;
	}
	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}
	public String getEventBannerUrl() {
		return eventBannerUrl;
	}
	public void setEventBannerUrl(String eventBannerUrl) {
		this.eventBannerUrl = eventBannerUrl;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	public String getQrPaymentUrl() {
		return qrPaymentUrl;
	}
	public void setQrPaymentUrl(String qrPaymentUrl) {
		this.qrPaymentUrl = qrPaymentUrl;
	}
	public String getPaymentExpireDate() {
		return paymentExpireDate;
	}
	public void setPaymentExpireDate(String paymentExpireDate) {
		this.paymentExpireDate = paymentExpireDate;
	}
	public int getEmailId() {
		return emailId;
	}
	public void setEmailId(int emailId) {
		this.emailId = emailId;
	}
	public List<TicketCountModel> getListTicketCount() {
		return listTicketCount;
	}
	public void setListTicketCount(List<TicketCountModel> listTicketCount) {
		this.listTicketCount = listTicketCount;
	}
	public String getPaymentGatewayFeeUnit() {
		return paymentGatewayFeeUnit;
	}
	public void setPaymentGatewayFeeUnit(String paymentGatewayFeeUnit) {
		this.paymentGatewayFeeUnit = paymentGatewayFeeUnit;
	}

	public List<EventImageModel> getEventImage() {
		return eventImage;
	}
	public void setEventImage(List<EventImageModel> eventImage) {
		this.eventImage = eventImage;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public List<String> getListDistance() {
		return listDistance;
	}
	public void setListDistance(List<String> listDistance) {
		this.listDistance = listDistance;
	}
	public BigDecimal getPaymentGatewayFeeAmount() {
		return paymentGatewayFeeAmount;
	}
	public void setPaymentGatewayFeeAmount(BigDecimal paymentGatewayFeeAmount) {
		this.paymentGatewayFeeAmount = paymentGatewayFeeAmount;
	}
	public String getOrderStatusDescEn() {
		return orderStatusDescEn;
	}
	public void setOrderStatusDescEn(String orderStatusDescEn) {
		this.orderStatusDescEn = orderStatusDescEn;
	}
	public String getEventNameEn() {
		return eventNameEn;
	}
	public void setEventNameEn(String eventNameEn) {
		this.eventNameEn = eventNameEn;
	}
	public String getProvinceDescEn() {
		return provinceDescEn;
	}
	public void setProvinceDescEn(String provinceDescEn) {
		this.provinceDescEn = provinceDescEn;
	}
	public String getDistrictDescEn() {
		return districtDescEn;
	}
	public void setDistrictDescEn(String districtDescEn) {
		this.districtDescEn = districtDescEn;
	}
	public String getSubDistrictDescEn() {
		return subDistrictDescEn;
	}
	public void setSubDistrictDescEn(String subDistrictDescEn) {
		this.subDistrictDescEn = subDistrictDescEn;
	}
	public String getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(String unitRate) {
		this.unitRate = unitRate;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public String getRacepackStatus() {
		return racepackStatus;
	}
	public void setRacepackStatus(String racepackStatus) {
		this.racepackStatus = racepackStatus;
	}
	public String getRegisterType() {
		return registerType;
	}
	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}
	public String getOrderGenerateCode() {
		return orderGenerateCode;
	}
	public void setOrderGenerateCode(String orderGenerateCode) {
		this.orderGenerateCode = orderGenerateCode;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getTrackingProvider() {
		return trackingProvider;
	}
	public void setTrackingProvider(String trackingProvider) {
		this.trackingProvider = trackingProvider;
	}
	public String getRefCode() {
		return refCode;
	}
	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}
	public String getEventImageUrl() {
		return eventImageUrl;
	}
	public void setEventImageUrl(String eventImageUrl) {
		this.eventImageUrl = eventImageUrl;
	}
	public EventModel getEventModel() {
		return eventModel;
	}
	public void setEventModel(EventModel eventModel) {
		this.eventModel = eventModel;
	}
	public CouponModel getCouponModel() {
		return couponModel;
	}
	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}
	public CouponCodeModel getCouponCodeModel() {
		return couponCodeModel;
	}
	public void setCouponCodeModel(CouponCodeModel couponCodeModel) {
		this.couponCodeModel = couponCodeModel;
	}
	public PaymentDirectTransModel getPaymentDirectTransModel() {
		return paymentDirectTransModel;
	}
	public void setPaymentDirectTransModel(PaymentDirectTransModel paymentDirectTransModel) {
		this.paymentDirectTransModel = paymentDirectTransModel;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getQrCodePath() {
		return qrCodePath;
	}
	public void setQrCodePath(String qrCodePath) {
		this.qrCodePath = qrCodePath;
	}
	


	
	
}
