package com.app.ptt.notification.model;

import java.util.List;

public class OptionModel {
	private long optionId;
	private long eventId;
	private long ticketId;
	private long fileId;
	private String optionType;
	private String optionStatus;
	private String imageName;
	private String fieldName;
	private String fieldLabel;
	private String fieldPlaceholder;
	private int fieldLimit;
	private String fieldType;
	private int fieldRequired;
	private int fieldItemSeq;
	private String field;
	private String optionTypeDesc;
	private String optionStatusDesc;
	private String fieldLabelEn;
	private String fieldPlaceholderEn;
	private String shirtField;
	
	public String getOptionStatusDesc() {
		return optionStatusDesc;
	}
	public void setOptionStatusDesc(String optionStatusDesc) {
		this.optionStatusDesc = optionStatusDesc;
	}
	public String getOptionTypeDesc() {
		return optionTypeDesc;
	}
	public void setOptionTypeDesc(String optionTypeDesc) {
		this.optionTypeDesc = optionTypeDesc;
	}
	private List<OptionDetailModel> listOptionDetail;
	
	public long getOptionId() {
		return optionId;
	}
	public void setOptionId(long optionId) {
		this.optionId = optionId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public String getOptionType() {
		return optionType;
	}
	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	public String getOptionStatus() {
		return optionStatus;
	}
	public void setOptionStatus(String optionStatus) {
		this.optionStatus = optionStatus;
	}
	
	public List<OptionDetailModel> getListOptionDetail() {
		return listOptionDetail;
	}
	public void setListOptionDetail(List<OptionDetailModel> listOptionDetail) {
		this.listOptionDetail = listOptionDetail;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public String getFieldPlaceholder() {
		return fieldPlaceholder;
	}
	public void setFieldPlaceholder(String fieldPlaceholder) {
		this.fieldPlaceholder = fieldPlaceholder;
	}
	public int getFieldLimit() {
		return fieldLimit;
	}
	public void setFieldLimit(int fieldLimit) {
		this.fieldLimit = fieldLimit;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public int getFieldRequired() {
		return fieldRequired;
	}
	public void setFieldRequired(int fieldRequired) {
		this.fieldRequired = fieldRequired;
	}
	public int getFieldItemSeq() {
		return fieldItemSeq;
	}
	public void setFieldItemSeq(int fieldItemSeq) {
		this.fieldItemSeq = fieldItemSeq;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public String getFieldLabelEn() {
		return fieldLabelEn;
	}
	public void setFieldLabelEn(String fieldLabelEn) {
		this.fieldLabelEn = fieldLabelEn;
	}
	public String getFieldPlaceholderEn() {
		return fieldPlaceholderEn;
	}
	public void setFieldPlaceholderEn(String fieldPlaceholderEn) {
		this.fieldPlaceholderEn = fieldPlaceholderEn;
	}
	public String getShirtField() {
		return shirtField;
	}
	public void setShirtField(String shirtField) {
		this.shirtField = shirtField;
	}
	
	
	
}
