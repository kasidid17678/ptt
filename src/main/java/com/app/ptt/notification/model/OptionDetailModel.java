package com.app.ptt.notification.model;

public class OptionDetailModel {
	private long optionDetailId;
	private long optionId;
	private long eventId;
	private String optionDetailName;
	private String value1;
	private String value2;
	private String status;
	
	private String addStatus;
	private String statusDesc;
	private String fieldName;
	private String optionDetailNameEn;
	
	private String fieldLabelEn;
	
	public long getOptionDetailId() {
		return optionDetailId;
	}
	public void setOptionDetailId(long optionDetailId) {
		this.optionDetailId = optionDetailId;
	}
	public long getOptionId() {
		return optionId;
	}
	public void setOptionId(long optionId) {
		this.optionId = optionId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public String getOptionDetailName() {
		return optionDetailName;
	}
	public void setOptionDetailName(String optionDetailName) {
		this.optionDetailName = optionDetailName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public String getAddStatus() {
		return addStatus;
	}
	public void setAddStatus(String addStatus) {
		this.addStatus = addStatus;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getOptionDetailNameEn() {
		return optionDetailNameEn;
	}
	public void setOptionDetailNameEn(String optionDetailNameEn) {
		this.optionDetailNameEn = optionDetailNameEn;
	}
	public String getFieldLabelEn() {
		return fieldLabelEn;
	}
	public void setFieldLabelEn(String fieldLabelEn) {
		this.fieldLabelEn = fieldLabelEn;
	}
	
	
	
}
