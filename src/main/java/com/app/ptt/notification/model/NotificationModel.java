package com.app.ptt.notification.model;

import java.util.List;

public class NotificationModel {
	
	private String destination; 
	private List<LineEventModel> events;
	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public List<LineEventModel> getEvents() {
		return events;
	}
	public void setEvents(List<LineEventModel> events) {
		this.events = events;
	}
	
	
}
