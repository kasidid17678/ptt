package com.app.ptt.notification.model;

public class TicketAgeGroupModel {

	private long ticketAgeGroupId;
	private long ticketId;
	private String ageGroupDesc;
	private String gender;
	private long minAgeRange;
	private long maxAgeRange;
	private long sequence;
	
	public long getTicketAgeGroupId() {
		return ticketAgeGroupId;
	}
	public void setTicketAgeGroupId(long ticketAgeGroupId) {
		this.ticketAgeGroupId = ticketAgeGroupId;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getMinAgeRange() {
		return minAgeRange;
	}
	public void setMinAgeRange(long minAgeRange) {
		this.minAgeRange = minAgeRange;
	}
	public long getMaxAgeRange() {
		return maxAgeRange;
	}
	public void setMaxAgeRange(long maxAgeRange) {
		this.maxAgeRange = maxAgeRange;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	public String getAgeGroupDesc() {
		return ageGroupDesc;
	}
	public void setAgeGroupDesc(String ageGroupDesc) {
		this.ageGroupDesc = ageGroupDesc;
	}
	
	
	
}
