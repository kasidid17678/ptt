package com.app.ptt.notification.model;

public class ResponseBase<T> {
	private String success;
	private String message;
	private T result;
	
	public void setSuccessResponse(String message) {
		this.result = null;
		this.success = "true";
		this.message = message;
	}
	
	public void setSuccessResponse(String message, T result) {
		this.result = result;
		this.success = "true";
		this.message = message;
	}
	
	public void setErrorResponse(String message) {
		this.result = null;
		this.success = "false";
		this.message = message;
	}
	
	public void setErrorResponse(String message, T result) {
		this.result = result;
		this.success = "false";
		this.message = message;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}
	
}
