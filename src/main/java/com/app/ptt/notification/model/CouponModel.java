package com.app.ptt.notification.model;

import java.util.List;

public class CouponModel {

	private int couponId;
	private int eventId;
	private int ticketId;
	private String couponType;
	private String couponMode;
	private String prefix;
	private String suffix;
	private double discountRate;
	private String unitRate;
	private int maxRegister;
	private int qty;
	private String startDate;
	private String expireDate;
	private String status;
	private String descTh;
	private String descEn;
	private String createDate;
	private String createBy;
	private String updateDate;
	private String updateBy;
	private String couponCode;
	private String netAmt;
	private String ticketPrice;
	private double discountAmt;
	private String discountAmtDesc;
	private int couponCodeId;
	private String couponCodeStatus;
	private double amount;
	private String amountDesc;
	
	private List<CouponCodeModel> listCouponCode;
	private int suffixLength;
	private List<CouponEventModel> listCouponEvent;
	
	
	private double discountRateBaht;
	private double discountRatePercent;
	
	
	public int getCouponId() {
		return couponId;
	}
	public void setCouponId(int couponId) {
		this.couponId = couponId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public String getCouponMode() {
		return couponMode;
	}
	public void setCouponMode(String couponMode) {
		this.couponMode = couponMode;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public String getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(String unitRate) {
		this.unitRate = unitRate;
	}
	public int getMaxRegister() {
		return maxRegister;
	}
	public void setMaxRegister(int maxRegister) {
		this.maxRegister = maxRegister;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescTh() {
		return descTh;
	}
	public void setDescTh(String descTh) {
		this.descTh = descTh;
	}
	public String getDescEn() {
		return descEn;
	}
	public void setDescEn(String descEn) {
		this.descEn = descEn;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public String getNetAmt() {
		return netAmt;
	}
	public void setNetAmt(String netAmt) {
		this.netAmt = netAmt;
	}
	public String getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	
	public int getCouponCodeId() {
		return couponCodeId;
	}
	public void setCouponCodeId(int couponCodeId) {
		this.couponCodeId = couponCodeId;
	}
	public String getCouponCodeStatus() {
		return couponCodeStatus;
	}
	public void setCouponCodeStatus(String couponCodeStatus) {
		this.couponCodeStatus = couponCodeStatus;
	}
	public List<CouponCodeModel> getListCouponCode() {
		return listCouponCode;
	}
	public void setListCouponCode(List<CouponCodeModel> listCouponCode) {
		this.listCouponCode = listCouponCode;
	}
	public int getSuffixLength() {
		return suffixLength;
	}
	public void setSuffixLength(int suffixLength) {
		this.suffixLength = suffixLength;
	}
	public List<CouponEventModel> getListCouponEvent() {
		return listCouponEvent;
	}
	public void setListCouponEvent(List<CouponEventModel> listCouponEvent) {
		this.listCouponEvent = listCouponEvent;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getAmountDesc() {
		return amountDesc;
	}
	public void setAmountDesc(String amountDesc) {
		this.amountDesc = amountDesc;
	}
	public String getDiscountAmtDesc() {
		return discountAmtDesc;
	}
	public void setDiscountAmtDesc(String discountAmtDesc) {
		this.discountAmtDesc = discountAmtDesc;
	}
	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}
	public double getDiscountAmt() {
		return discountAmt;
	}
	public double getDiscountRate() {
		return discountRate;
	}
	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}
	public double getDiscountRateBaht() {
		return discountRateBaht;
	}
	public void setDiscountRateBaht(double discountRateBaht) {
		this.discountRateBaht = discountRateBaht;
	}
	public double getDiscountRatePercent() {
		return discountRatePercent;
	}
	public void setDiscountRatePercent(double discountRatePercent) {
		this.discountRatePercent = discountRatePercent;
	}

	
	
	
	
	
}
