
package com.app.ptt.notification.model;
import java.util.List;


public class PromotionModel {
	
	private int promotionId ;
	private int shopId ;
	private String promotionCode;
	private String promotionName;
	private String promotionType;
	private String image;
	private String icon;
	private String description;
	private String point;
	private String status;
	private String createDate;
	private String createBy;
	private String updateDate;
	private String updateBy;
	private String bindScreen;
	private String startDate;
	private String endDate;
	private int limitUser;
	private String imageUrl;
	private String iconUrl;
	private int  customerProfileId;
	private int customerPoint;
	private int userId;
	private String qrImage;
	private String shopName;
	
	private List<PromotionScreenModel> listScreen ;
	
	
	public int getCustomerProfileId() {
		return customerProfileId;
	}
	public void setCustomerProfileId(int customerProfileId) {
		this.customerProfileId = customerProfileId;
	}
	public int getCustomerPoint() {
		return customerPoint;
	}
	public void setCustomerPoint(int customerPoint) {
		this.customerPoint = customerPoint;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public int getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public String getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getBindScreen() {
		return bindScreen;
	}
	public void setBindScreen(String bindScreen) {
		this.bindScreen = bindScreen;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getLimitUser() {
		return limitUser;
	}
	public void setLimitUser(int limitUser) {
		this.limitUser = limitUser;
	}
	public List<PromotionScreenModel> getListScreen() {
		return listScreen;
	}
	public void setListScreen(List<PromotionScreenModel> listScreen) {
		this.listScreen = listScreen;
	}
	public String getQrImage() {
		return qrImage;
	}
	public void setQrImage(String qrImage) {
		this.qrImage = qrImage;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
}
