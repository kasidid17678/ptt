package com.app.ptt.notification.model;

public class EventSectionImage {
	
	private int eventSectionImageId;
	private int eventSectionId;
	private int eventId;
	private long fileId;
	private String imageName;
	private String imageTitle;
	private String imageDesc;
	private String status;
	private String imageUrl;
	
	public int getEventSectionImageId() {
		return eventSectionImageId;
	}
	public void setEventSectionImageId(int eventSectionImageId) {
		this.eventSectionImageId = eventSectionImageId;
	}
	public int getEventSectionId() {
		return eventSectionId;
	}
	public void setEventSectionId(int eventSectionId) {
		this.eventSectionId = eventSectionId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getImageTitle() {
		return imageTitle;
	}
	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}
	public String getImageDesc() {
		return imageDesc;
	}
	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	
	
	
}
