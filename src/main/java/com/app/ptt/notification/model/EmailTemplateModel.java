package com.app.ptt.notification.model;

import java.math.BigDecimal;
import java.util.List;

public class EmailTemplateModel {
	private String userName;
	private String firstName;
	private String lastName;
	private String setTo;
	private String setFrom;
	private String emaiTemplate;
	private String newPassword;
	// Email Template
	private String emailId;
	private String emailName;
	private String subjectEmail;
	private String bodyMessageEmail;
	private String emailType;
	private String email;
	private BigDecimal netAmt;

	private String eventImage;
	
	private int userId;
	
	private OrderModel orderModel;
	private List<TicketCountModel> listTicketCount;
	private String address;
	
//	MAIL4
	private String gender;
	private String maxAge;
	private String minAge;
	private String message;
	private String img1;
	private String img2;
	private List<OrderDetailModel> listOrderDetailEmail;
	
	private String folderLink;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSetTo() {
		return setTo;
	}

	public void setSetTo(String setTo) {
		this.setTo = setTo;
	}

	public String getSetFrom() {
		return setFrom;
	}

	public void setSetFrom(String setFrom) {
		this.setFrom = setFrom;
	}

	public String getEmaiTemplate() {
		return emaiTemplate;
	}

	public void setEmaiTemplate(String emaiTemplate) {
		this.emaiTemplate = emaiTemplate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmailName() {
		return emailName;
	}

	public void setEmailName(String emailName) {
		this.emailName = emailName;
	}

	public String getSubjectEmail() {
		return subjectEmail;
	}

	public void setSubjectEmail(String subjectEmail) {
		this.subjectEmail = subjectEmail;
	}

	public String getBodyMessageEmail() {
		return bodyMessageEmail;
	}

	public void setBodyMessageEmail(String bodyMessageEmail) {
		this.bodyMessageEmail = bodyMessageEmail;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getNetAmt() {
		return netAmt;
	}

	public void setNetAmt(BigDecimal netAmt) {
		this.netAmt = netAmt;
	}

	public OrderModel getOrderModel() {
		return orderModel;
	}

	public void setOrderModel(OrderModel orderModel) {
		this.orderModel = orderModel;
	}

	public String getEventImage() {
		return eventImage;
	}

	public void setEventImage(String eventImage) {
		this.eventImage = eventImage;
	}

	public List<TicketCountModel> getListTicketCount() {
		return listTicketCount;
	}

	public void setListTicketCount(List<TicketCountModel> listTicketCount) {
		this.listTicketCount = listTicketCount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	public List<OrderDetailModel> getListOrderDetailEmail() {
		return listOrderDetailEmail;
	}

	public void setListOrderDetailEmail(List<OrderDetailModel> listOrderDetailEmail) {
		this.listOrderDetailEmail = listOrderDetailEmail;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}

	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFolderLink() {
		return folderLink;
	}

	public void setFolderLink(String folderLink) {
		this.folderLink = folderLink;
	}

	

}