package com.app.ptt.notification.model;


public class UserAddressModel {
	private long userAddressId;
	private long userId;
	private String address;
	private String provinceCode;
	private String districtCode;
	private String subDistrictCode;
	private String zipcode;
	private String addressStatus;
	
	public long getUserAddressId() {
		return userAddressId;
	}
	public void setUserAddressId(long userAddressId) {
		this.userAddressId = userAddressId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public String getSubDistrictCode() {
		return subDistrictCode;
	}
	public void setSubDistrictCode(String subDistrictCode) {
		this.subDistrictCode = subDistrictCode;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getAddressStatus() {
		return addressStatus;
	}
	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}
	
}
