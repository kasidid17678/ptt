package com.app.ptt.notification.model;

import java.sql.Date;
import java.util.List;



public class OrderDetailModel {
	private long orderDetailId;
	private long orderId;
	private long eventId;
	private String eventName;
	private long ticketId;
	private String ticketName;
	private String orderNumber;
	private String firstName;
	private String middleName;
	private String lastName;
	private String birthDate;
	private String gender;
	private String email;
	private String tel;
	private String idpsNumber;
	private String bloodGroup;
	private String sick;
	private String qrCode;
	private String serialNumber;
	private String orderDetailStatus;
	private String orderStatusDesc;
	private List<OrderObjectModel> orderObject;
	private String eventUrl;
	private String remark;
	private String actionBy;
	private String ticketNameTh;
	private String ticketNameEn;
	private String eventNameTh;
	private String eventNameEn;
	private String eventImageName;
	private long ticketPrice;
	private String nationality;
	private String contactPerson;
	private String contactNumber;
	private String shirtSize;
	private List<OrderRevisionHistoryModel> listRevisionHistory;
	private String ageGroup;
	private String ageGroupDesc;
	private String age;
	private String genderDesc;
	private String fileId;
	
////////////////////////
	
	private String runnerName;
	private String country;
	private String priceType;
	private String priceTypeDesc;
	
	private TicketModel ticketModel;
	private String ticketCode;
	
	private String orderStatus;
	private String dateFrom;
	private String dateTo;
	
	
	private Long userId;
	private Long userAddressId;
	
	private Double amount;
	
	private String name ;
	private int friendId;
	
	private int errorCode;
	private ErrorModel errorModel;
	
	private List<UserAddressModel> listUserAddress;
	private UserAddressModel userAddressModel;
	
	private String race;
	private String qrCodePath;
	
	public long getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getIdpsNumber() {
		return idpsNumber;
	}
	public void setIdpsNumber(String idpsNumber) {
		this.idpsNumber = idpsNumber;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getSick() {
		return sick;
	}
	public void setSick(String sick) {
		this.sick = sick;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getOrderDetailStatus() {
		return orderDetailStatus;
	}
	public void setOrderDetailStatus(String orderDetailStatus) {
		this.orderDetailStatus = orderDetailStatus;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getOrderStatusDesc() {
		return orderStatusDesc;
	}
	public void setOrderStatusDesc(String orderStatusDesc) {
		this.orderStatusDesc = orderStatusDesc;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getEventUrl() {
		return eventUrl;
	}
	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}
	public List<OrderObjectModel> getOrderObject() {
		return orderObject;
	}
	public void setOrderObject(List<OrderObjectModel> orderObject) {
		this.orderObject = orderObject;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public List<OrderRevisionHistoryModel> getListRevisionHistory() {
		return listRevisionHistory;
	}
	public void setListRevisionHistory(List<OrderRevisionHistoryModel> listRevisionHistory) {
		this.listRevisionHistory = listRevisionHistory;
	}
	public String getTicketNameTh() {
		return ticketNameTh;
	}
	public void setTicketNameTh(String ticketNameTh) {
		this.ticketNameTh = ticketNameTh;
	}
	public String getTicketNameEn() {
		return ticketNameEn;
	}
	public void setTicketNameEn(String ticketNameEn) {
		this.ticketNameEn = ticketNameEn;
	}
	public long getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(long ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public String getEventImageName() {
		return eventImageName;
	}
	public void setEventImageName(String eventImageName) {
		this.eventImageName = eventImageName;
	}
	public String getEventNameTh() {
		return eventNameTh;
	}
	public void setEventNameTh(String eventNameTh) {
		this.eventNameTh = eventNameTh;
	}
	public String getEventNameEn() {
		return eventNameEn;
	}
	public void setEventNameEn(String eventNameEn) {
		this.eventNameEn = eventNameEn;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getShirtSize() {
		return shirtSize;
	}
	public void setShirtSize(String shirtSize) {
		this.shirtSize = shirtSize;
	}
	public String getAgeGroup() {
		return ageGroup;
	}
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}
	public String getAgeGroupDesc() {
		return ageGroupDesc;
	}
	public void setAgeGroupDesc(String ageGroupDesc) {
		this.ageGroupDesc = ageGroupDesc;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGenderDesc() {
		return genderDesc;
	}
	public void setGenderDesc(String genderDesc) {
		this.genderDesc = genderDesc;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getRunnerName() {
		return runnerName;
	}
	public void setRunnerName(String runnerName) {
		this.runnerName = runnerName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPriceType() {
		return priceType;
	}
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}
	public String getPriceTypeDesc() {
		return priceTypeDesc;
	}
	public void setPriceTypeDesc(String priceTypeDesc) {
		this.priceTypeDesc = priceTypeDesc;
	}
	public TicketModel getTicketModel() {
		return ticketModel;
	}
	public void setTicketModel(TicketModel ticketModel) {
		this.ticketModel = ticketModel;
	}
	public String getTicketCode() {
		return ticketCode;
	}
	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getUserAddressId() {
		return userAddressId;
	}
	public void setUserAddressId(Long userAddressId) {
		this.userAddressId = userAddressId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getFriendId() {
		return friendId;
	}
	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public ErrorModel getErrorModel() {
		return errorModel;
	}
	public void setErrorModel(ErrorModel errorModel) {
		this.errorModel = errorModel;
	}
	public List<UserAddressModel> getListUserAddress() {
		return listUserAddress;
	}
	public void setListUserAddress(List<UserAddressModel> listUserAddress) {
		this.listUserAddress = listUserAddress;
	}
	public UserAddressModel getUserAddressModel() {
		return userAddressModel;
	}
	public void setUserAddressModel(UserAddressModel userAddressModel) {
		this.userAddressModel = userAddressModel;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public String getQrCodePath() {
		return qrCodePath;
	}
	public void setQrCodePath(String qrCodePath) {
		this.qrCodePath = qrCodePath;
	}
	
}
