package com.app.ptt.notification.model;

import java.util.List;

public class CampaignPointModel extends MessageModel{
	
	private int campaignPointId;
	private int shopId;
	private String campaignName;
	private String description;
	private String campaignType;
	private String customerType;
	private int point;
	private String campaignStartDate;
	private String campaignEndDate;
	private String status;
	private int customerProfileId;
	private String image;
	private String imageUrl;
	private String icon;
	private String qrImage;
	private List<CampaignConsentModel> listConsent;
	private String shopName;
	
	public int getCampaignPointId() {
		return campaignPointId;
	}
	public void setCampaignPointId(int campaignPointId) {
		this.campaignPointId = campaignPointId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCampaignType() {
		return campaignType;
	}
	public void setCampaignType(String campaignType) {
		this.campaignType = campaignType;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public String getCampaignStartDate() {
		return campaignStartDate;
	}
	public void setCampaignStartDate(String campaignStartDate) {
		this.campaignStartDate = campaignStartDate;
	}
	public String getCampaignEndDate() {
		return campaignEndDate;
	}
	public void setCampaignEndDate(String campaignEndDate) {
		this.campaignEndDate = campaignEndDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCustomerProfileId() {
		return customerProfileId;
	}
	public void setCustomerProfileId(int customerProfileId) {
		this.customerProfileId = customerProfileId;
	}
	public List<CampaignConsentModel> getListConsent() {
		return listConsent;
	}
	public void setListConsent(List<CampaignConsentModel> listConsent) {
		this.listConsent = listConsent;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getQrImage() {
		return qrImage;
	}
	public void setQrImage(String qrImage) {
		this.qrImage = qrImage;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	
}
