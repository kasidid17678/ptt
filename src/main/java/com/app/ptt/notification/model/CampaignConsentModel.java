package com.app.ptt.notification.model;

public class CampaignConsentModel extends MessageModel{
	
	private int campaignConsentId;
	private int campaignPointId;
	private int shopId;
	private String consent;
	private String status;
	
	public int getCampaignConsentId() {
		return campaignConsentId;
	}
	public void setCampaignConsentId(int campaignConsentId) {
		this.campaignConsentId = campaignConsentId;
	}
	public int getCampaignPointId() {
		return campaignPointId;
	}
	public void setCampaignPointId(int campaignPointId) {
		this.campaignPointId = campaignPointId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getConsent() {
		return consent;
	}
	public void setConsent(String consent) {
		this.consent = consent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
