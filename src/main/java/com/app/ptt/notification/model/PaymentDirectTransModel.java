package com.app.ptt.notification.model;

import java.math.BigDecimal;

public class PaymentDirectTransModel {

	private Integer paymentDirectTransId;
	private BigDecimal directTransNetAmount;
	private String remark;
	private String directTransDate;
	private String directTransTime;
	private String orderNumber;
	private String imageSlip;
	
	private String date;
	private String month;
	private String year;
	private String hour;
	private String minute;

  
	public Integer getPaymentDirectTransId() {
		return paymentDirectTransId;
	}
	public void setPaymentDirectTransId(Integer paymentDirectTransId) {
		this.paymentDirectTransId = paymentDirectTransId;
	}
	public BigDecimal getDirectTransNetAmount() {
		return directTransNetAmount;
	}
	public void setDirectTransNetAmount(BigDecimal directTransNetAmount) {
		this.directTransNetAmount = directTransNetAmount;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getDirectTransDate() {
		return directTransDate;
	}
	public void setDirectTransDate(String directTransDate) {
		this.directTransDate = directTransDate;
	}
	public String getDirectTransTime() {
		return directTransTime;
	}
	public void setDirectTransTime(String directTransTime) {
		this.directTransTime = directTransTime;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getImageSlip() {
		return imageSlip;
	}
	public void setImageSlip(String imageSlip) {
		this.imageSlip = imageSlip;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public String getMinute() {
		return minute;
	}
	public void setMinute(String minute) {
		this.minute = minute;
	}
	
	
	
}
