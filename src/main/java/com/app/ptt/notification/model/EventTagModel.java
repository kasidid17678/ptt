package com.app.ptt.notification.model;

public class EventTagModel {
	private int	eventTagId;
	private int	eventId;
	private String	tagName;
	private String	tagUrl;
	private String	tagIcon;
	private String	status;
	private	String createDate;
	private String	createBy;
	private String	updateDate;
	private String	updateBy;
	private String	tagStatus;
	
	private int tagId;
	
	public int getEventTagId() {
		return eventTagId;
	}
	public void setEventTagId(int eventTagId) {
		this.eventTagId = eventTagId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getTagUrl() {
		return tagUrl;
	}
	public void setTagUrl(String tagUrl) {
		this.tagUrl = tagUrl;
	}
	public String getTagIcon() {
		return tagIcon;
	}
	public void setTagIcon(String tagIcon) {
		this.tagIcon = tagIcon;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getTagStatus() {
		return tagStatus;
	}
	public void setTagStatus(String tagStatus) {
		this.tagStatus = tagStatus;
	}
	public int getTagId() {
		return tagId;
	}
	public void setTagId(int tagId) {
		this.tagId = tagId;
	}
	
	
	
}
