package com.app.ptt.notification.model;

public class ProvinceModel {

	private int provinceId;
	private String provinceCode;
	private String provinceName;
	private String provinceNameEng;
	private String geoId;
	
	public int getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getProvinceNameEng() {
		return provinceNameEng;
	}
	public void setProvinceNameEng(String provinceNameEng) {
		this.provinceNameEng = provinceNameEng;
	}
	public String getGeoId() {
		return geoId;
	}
	public void setGeoId(String geoId) {
		this.geoId = geoId;
	}
	
	
}
