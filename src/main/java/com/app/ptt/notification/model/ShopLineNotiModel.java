package com.app.ptt.notification.model;

import java.util.List;

public class ShopLineNotiModel {

	 private int lineNotiMessageId;
	 private String messageCode;
	 private int shopLineNotid;
	 private int shopId;
	 private String lineNotiStatus;
	 private String token ;
	 private String createDate;
	 private String createBy;
	 private int customerProfileId;
	 private List<LineNotiMessageModel> lineNotiMessageModel;
	 private int promotionId;
	 private int campaignPointId;
	 private int customerTransLogId;
	 private String status;
	 private int userId;
	 private String shopName;
	 
	public int getLineNotiMessageId() {
		return lineNotiMessageId;
	}
	public void setLineNotiMessageId(int lineNotiMessageId) {
		this.lineNotiMessageId = lineNotiMessageId;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public int getShopLineNotid() {
		return shopLineNotid;
	}
	public void setShopLineNotid(int shopLineNotid) {
		this.shopLineNotid = shopLineNotid;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getLineNotiStatus() {
		return lineNotiStatus;
	}
	public void setLineNotiStatus(String lineNotiStatus) {
		this.lineNotiStatus = lineNotiStatus;
	}
	public List<LineNotiMessageModel> getLineNotiMessageModel() {
		return lineNotiMessageModel;
	}
	public void setLineNotiMessageModel(List<LineNotiMessageModel> lineNotiMessageModel) {
		this.lineNotiMessageModel = lineNotiMessageModel;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public int getCustomerProfileId() {
		return customerProfileId;
	}
	public void setCustomerProfileId(int customerProfileId) {
		this.customerProfileId = customerProfileId;
	}
	public int getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	
	public int getCustomerTransLogId() {
		return customerTransLogId;
	}
	public void setCustomerTransLogId(int customerTransLogId) {
		this.customerTransLogId = customerTransLogId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCampaignPointId() {
		return campaignPointId;
	}
	public void setCampaignPointId(int campaignPointId) {
		this.campaignPointId = campaignPointId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	 
	 
}
