package com.app.ptt.notification.model;

public class OrderRevisionHistoryModel {
	private int orderRevisionHistoryId;
	private int orderId;
	private int orderDetailId;
	private String orderNumber;
	private String remark;
	private String createDate;
	private String createBy;
	
	public int getOrderRevisionHistoryId() {
		return orderRevisionHistoryId;
	}
	public void setOrderRevisionHistoryId(int orderRevisionHistoryId) {
		this.orderRevisionHistoryId = orderRevisionHistoryId;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(int orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	
	
}
