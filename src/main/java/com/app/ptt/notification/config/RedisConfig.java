package com.app.ptt.notification.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {
	
	
	@Value("${redis.host}") String redisHost;
	@Value("${redis.password}") String redisPassword;
	
	@Bean
    JedisConnectionFactory jedisConnectionFactory() {
		
		RedisStandaloneConfiguration r = new RedisStandaloneConfiguration();

		r.setPort(6379);
		r.setHostName(redisHost);
		r.setPassword(redisPassword);
		
        return new JedisConnectionFactory(r);
    }
	
    @Bean
    public RedisTemplate<String, Object> redisTemplate() {

    	
    	RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
        
    }
}
