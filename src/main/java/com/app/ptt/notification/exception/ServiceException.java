package com.app.ptt.notification.exception;

import java.util.Map;

public class ServiceException extends RuntimeException{
 
	private static final long serialVersionUID = 1348771109171435607L;
	public Map<String,String> errorParam;
	
	public ServiceException(String message) {
		super(message);
	}
	
	
	public ServiceException(String message, Map<String,String> errorParam) {
		super(message);
		this.errorParam = errorParam;
	}
	
	public ServiceException(String message, Throwable t) {
		super(message);
		t.printStackTrace();
	}
	
	public ServiceException(String message, Map<String,String> errorParam, Throwable t) {
		super(message);
		this.errorParam = errorParam;
		t.printStackTrace();
	}
}
