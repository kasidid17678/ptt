package com.app.ptt.notification.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.OrderDetailModel;
import com.app.ptt.notification.model.OrderModel;
import com.app.ptt.notification.service.EmailService;
import com.app.ptt.notification.utils.LatteUtil;
import com.google.gson.Gson;

@SpringBootApplication
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
//@RequestMapping("/EmailController")
public class EmailController {

	private static Logger logger = LogManager.getLogger(EmailController.class);

	@Autowired
	EmailService emailService;
	
	@RequestMapping(value = "sendEmail", method = RequestMethod.POST, consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public boolean sendEmail(@RequestBody EmailTemplateModel emailModel) {
		boolean result = false;
		System.out.println("emailModel :"+new Gson().toJson(emailModel));
		try {
			switch (emailModel.getEmailId()) {
//			case Constants.REMIND_PAYMENT:
//				result = emailService.sendMailRemindPayment(emailModel);
////				result = emailService.sendMailRemindPaymentRedis(emailModel);
//				
//				break;
//			case Constants.AFTER_PAYMENT:
//				result = emailService.sendMailAfterPayment(emailModel);
////				result = emailService.sendMailAfterPaymentRedis(emailModel);
//				break;
//			case Constants.RESET_PASSWORD:
//				result = emailService.sendMailResetPassword(emailModel);
//				break;
			case Constants.PTT:
				result = emailService.sendMailPtt(emailModel);
				break;
			case Constants.ADVERTISE:
				result = emailService.sendMailAdvertise(emailModel);
				break;
			default:
				return result;
			}
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "reSendEmail", method = RequestMethod.POST, consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public boolean reSendEmail(@RequestBody OrderModel orderModel) {
		boolean result = false;
		
		try {
			result = emailService.reSendEmail(orderModel);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "reSendSelectedEmail", method = RequestMethod.POST, consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public boolean reSendSelectedEmail(@RequestBody OrderModel orderModel) {
		boolean result = false;
		
		try {
			if(!LatteUtil.isEmpty(orderModel) && !LatteUtil.isEmpty(orderModel.getEmail())) {
				result = emailService.reSendSelectedEmail(orderModel);
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "listFilteredOrderDetail", method = RequestMethod.POST, consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public List<OrderDetailModel> listFilteredOrderDetail(@RequestBody EmailTemplateModel emailModel) {
		List<OrderDetailModel> listorderDetail = new ArrayList<OrderDetailModel>();
		
		try {
			listorderDetail = emailService.listFilteredOrderDetail(emailModel);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return listorderDetail;
	}
	
//	@RequestMapping(value = "sendEmailManual", method = RequestMethod.POST, consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
//	public boolean sendEmailManual() {
//		boolean result = false;
//		try {
//	int count = 0;
//			List<EmailTemplateModel>  listOrder =new ArrayList<EmailTemplateModel>();
//			listOrder = emailService.getListOrderNumber();
//			for(int i=0;i<listOrder.size();i++) {
//				listOrder.get(i).setEmailId("2");
//				result = emailService.sendMailAfterPayment(listOrder.get(i));
//				if(result) {
//					count++;
//				}
//			}
//				
//			System.out.println("count = "+count);
//
//		} catch (Exception e) {
//			logger.error(e);
//			e.printStackTrace();
//		}
//		return result;
//	}
	
	@RequestMapping(value="importShareLink", method=RequestMethod.POST) 
	public boolean importTracking(HttpServletRequest request) {
		boolean b = false;
		try {
			System.out.println("importShareLink");
			MultipartHttpServletRequest multiPartRequest = new DefaultMultipartHttpServletRequest(request);
			multiPartRequest = (MultipartHttpServletRequest) request;
			multiPartRequest.getParameterMap();
			int result = 0;
			Iterator<String> itr = multiPartRequest.getFileNames();
			while (itr.hasNext()) {
				MultipartFile mFile = multiPartRequest.getFile(itr.next());
				
				 b = emailService.importShareLink(mFile);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}
	
}
