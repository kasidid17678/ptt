package com.app.ptt.notification.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.NotificationModel;
import com.app.ptt.notification.model.ShopLineNotiModel;
import com.app.ptt.notification.model.lineRequestModel;
import com.app.ptt.notification.service.LineNotiService;

@SpringBootApplication
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
//@RequestMapping("/LineNotiController")
public class LineNotiController {

	private static Logger logger = LogManager.getLogger(LineNotiController.class);

	@Autowired
	LineNotiService lineNotiService;

	@PostMapping(value = "sendMessage", consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public ShopLineNotiModel sendMessage(@RequestBody ShopLineNotiModel model) {
		logger.info(new Gson().toJson(model));
		try {
			if (!model.getToken().isEmpty()) {
				model.setStatus("fail");
				switch (model.getMessageCode()) {
				case Constants.PROMOTION_APPLY:
					model = lineNotiService.promotionNoti(model);
					break;
				case Constants.REGISTER:
					model = lineNotiService.registerNoti(model);
					break;
				case Constants.ADD_POINT:
//					  model = lineNotiService.addPointNoti(model);
					break;
				case Constants.CAMPAIGN_NOTI:
					model = lineNotiService.campaignNoti(model);
					break;
				case Constants.NEW_SHOP:
					model = lineNotiService.newShopNoti(model);
					break;
				case Constants.NEW_PROMOTION:
					model = lineNotiService.newPromotionNoti(model);
					break;
				case Constants.EDIT_USER_PROFILE:
					model = lineNotiService.editUserNoti(model);
					break;
				default:
					return model;
				}
			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}

		return model;
	}

	@PostMapping(value = "linePush", consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public void linePush(@RequestBody lineRequestModel request) {

		try {

			lineNotiService.linePush(request);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

	@PostMapping(value = "lineBotTest", consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public void lineBotTest(@RequestBody NotificationModel request) {

		try {
			logger.info("Line bot ja" + new Gson().toJson(request));
			lineNotiService.lineBotNoti(request);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

}
