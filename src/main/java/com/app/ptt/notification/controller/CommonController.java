package com.app.ptt.notification.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.ResponseBase;
import com.app.ptt.notification.service.CommonService;

@SpringBootApplication
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/commonController")
public class CommonController {

	private static Logger logger = LogManager.getLogger(CommonController.class);

	@Autowired
	CommonService commonService;

	@PostMapping("notificationController")
	public ResponseEntity<ResponseBase<EmailTemplateModel>> eventController() {
		
		ResponseBase<EmailTemplateModel> resp = new ResponseBase<EmailTemplateModel>();
		EmailTemplateModel emailModel = new EmailTemplateModel();
		try {
			System.out.println("notification notificationController...");
			resp.setSuccessResponse("get emailModel detail success", emailModel);
	        
		} catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		return new ResponseEntity<ResponseBase<EmailTemplateModel>>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "sendEmail", method = RequestMethod.POST, consumes = Constants.APPLICATION_JSON_UTF8_VALUE)
	public boolean sendEmail(@RequestBody EmailTemplateModel emailModel) {
		boolean result = false;
		try {
			result = commonService.sendMail(emailModel);

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
}
