package com.app.ptt.notification.dao;

import com.app.ptt.notification.model.CampaignPointModel;
import com.app.ptt.notification.model.CustomerTransLogModel;
import com.app.ptt.notification.model.LineNotiMessageModel;
import com.app.ptt.notification.model.PromotionModel;
import com.app.ptt.notification.model.ShopLineNotiModel;
import com.app.ptt.notification.model.ShopModel;
import com.app.ptt.notification.model.UserProfileModel;


public interface LineNotiDAO {

	public LineNotiMessageModel getMessage( ShopLineNotiModel model);
	
	public UserProfileModel getUserProfile(int customerProfileId);
	public UserProfileModel getUserInfo(int userId);
	
	public CustomerTransLogModel getPromotionDetail(int customerTransLogId);
	public CustomerTransLogModel getCampaignDetail(int customerTransLogId);
	public ShopModel getShopInfo(int shopId);
	public PromotionModel getPromotionInfo(int proId);
	public CampaignPointModel getCampaignInfo(int camId);

}
