package com.app.ptt.notification.dao;

import java.util.List;

import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.OrderDetailModel;
import com.app.ptt.notification.model.OrderModel;
import com.app.ptt.notification.model.OrderObjectModel;
import com.app.ptt.notification.model.UserAddressModel;


public interface EmailDAO {

	public EmailTemplateModel getEmailBodyByID(String emailId);
	public EmailTemplateModel getShopEmailStatus(EmailTemplateModel model);
	
	public OrderModel getOrder(String orderNumber);
	public List<OrderDetailModel> getOrderDetail(String orderNumber);
	public List<OrderObjectModel> listOrderObject(long orderDetailId);
	public UserAddressModel getAddress(long userAddressId);
	public List<EmailTemplateModel> getListOrderNumber();
	public List<String> listDistance(long eventId);
	
	public List<OrderDetailModel> listFilteredOrderDetail(EmailTemplateModel model);
	
}
