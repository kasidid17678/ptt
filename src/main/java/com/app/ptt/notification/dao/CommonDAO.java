package com.app.ptt.notification.dao;

import java.util.List;

import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.ListboxModel;
import com.app.ptt.notification.model.pttModel;

public interface CommonDAO {
	
	public List<ListboxModel> listUserShop(int userId);
	public EmailTemplateModel getEmailBodyByID(String emailId);

	public void importShareLink (String id, String link);
	public List<pttModel> listPtt();
	public void clearLink ();
	
}
