package com.app.ptt.notification.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.app.ptt.notification.constants.Constants;
import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.OrderDetailModel;
import com.app.ptt.notification.model.OrderModel;
import com.app.ptt.notification.model.OrderObjectModel;
import com.app.ptt.notification.model.UserAddressModel;
import com.app.ptt.notification.utils.LatteUtil;

@Repository
public class EmailDAOImpl implements EmailDAO{

private static Logger logger = LogManager.getLogger(EmailDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public EmailTemplateModel getEmailBodyByID(String emailId) {
    	EmailTemplateModel result = new EmailTemplateModel();
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" SELECT email_id, ");
			sqlCmd.append(" name AS email_name, ");
			sqlCmd.append(" subject AS subject_email, "); 
			sqlCmd.append(" body_message AS body_message_email, ");
			sqlCmd.append(" type AS email_type ");
			sqlCmd.append(" FROM ms_email ");
			sqlCmd.append(" WHERE email_id = ? ");
		
			Object[] params = new Object[] {
					emailId
			};
			
			RowMapper<EmailTemplateModel> rowMapper = new BeanPropertyRowMapper<EmailTemplateModel>(EmailTemplateModel.class);
			result =  jdbcTemplate.queryForObject(sqlCmd.toString(), rowMapper,params);
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
    }

	

	@Override
	public OrderModel getOrder(String orderNumber) {
		OrderModel result = new OrderModel();
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" SELECT ");
			sqlCmd.append(" o.order_id, ");
			sqlCmd.append(" o.user_id, ");
			sqlCmd.append(" o.event_id, ");
			sqlCmd.append(" o.order_number, ");
			sqlCmd.append(" o.total_order, ");
			sqlCmd.append(" o.amount, ");
			sqlCmd.append(" o.discount_amount AS discountAmt, ");
			sqlCmd.append(" o.vat, ");
			sqlCmd.append(" o.payment_gateway_fee, ");
			sqlCmd.append(" o.shipping_status, ");
			sqlCmd.append(" ss.name AS shippingStatusDesc, ");
			sqlCmd.append(" o.shipping_amount, ");
			sqlCmd.append(" o.user_address_id, ");
			sqlCmd.append(" o.net_amount, ");
			sqlCmd.append(" o.payment_channel, ");
			sqlCmd.append(" lb.name AS orderStatusDesc, ");
			sqlCmd.append(" o.qr_code, ");
			sqlCmd.append(" u.email, ");
			sqlCmd.append(" u.first_name, ");
			sqlCmd.append(" u.last_name, ");
			sqlCmd.append(" o.image_slip, ");
			sqlCmd.append(" e.ticket_location, ");
			sqlCmd.append(" o.coupon_code, ");
			sqlCmd.append(" CONCAT(u.first_name , ' ' ,u.last_name) AS customerName, ");
			sqlCmd.append(" CASE WHEN o.coupon_discount IS NULL OR o.coupon_discount = '' THEN '-' ELSE o.coupon_discount END AS couponDiscount, ");
			sqlCmd.append(" o.qr_payment_code, ");
			sqlCmd.append(" tot.organization_name, ");
			sqlCmd.append(" mp.province_name, ");
			sqlCmd.append(" e.event_date, ");
			sqlCmd.append(" o.image_slip, ");
			sqlCmd.append(" o.payment_gateway_fee_amount ");
			sqlCmd.append(" FROM tb_order o LEFT JOIN ms_listbox lb ON o.order_status = lb.value_1 AND lb.list_box_group = 'ORDER_STATUS' ");
			sqlCmd.append(" LEFT JOIN ms_listbox ss ON o.shipping_status = ss.value_1 AND ss.list_box_group = 'SHIPPING_STATUS' ");
			sqlCmd.append(" LEFT JOIN ms_user u ON o.user_id = u.user_id  ");
			sqlCmd.append(" LEFT JOIN tb_event e ON o.event_id = e.event_id  ");
			sqlCmd.append(" LEFT JOIN tb_organization tot ON e.organization_id = tot.organization_id");
			sqlCmd.append(" LEFT JOIN ms_provinces mp ON e.province_code = mp.province_code");
			sqlCmd.append(" WHERE order_number = ? ");
			
			Object[] params = new Object[] {
					orderNumber
			};
			
			RowMapper<OrderModel> rowMapper = new BeanPropertyRowMapper<OrderModel>(OrderModel.class);
			result =  jdbcTemplate.queryForObject(sqlCmd.toString(), rowMapper,params);
			
				if(!LatteUtil.isEmpty(result.getQrPaymentCode()) ) {
				
				StringBuilder fileDownloadUri = new StringBuilder();
				fileDownloadUri.append(Constants.URL_IMAGE);
				fileDownloadUri.append(result.getEventId()+"/qr/");
				fileDownloadUri.append(result.getQrPaymentCode());
				result.setQrPaymentCode(fileDownloadUri.toString());
		        
			}
				
				if(!LatteUtil.isEmpty(result.getImageSlip()) ) {
					
					StringBuilder fileDownloadUri = new StringBuilder();
					fileDownloadUri.append(Constants.URL_IMAGE_QR);
					fileDownloadUri.append(result.getEventId()+"/");
					fileDownloadUri.append(result.getImageSlip());
					result.setImageSlip(fileDownloadUri.toString());
				}
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<OrderDetailModel> getOrderDetail(String orderNumber) {
		List<OrderDetailModel> result = new ArrayList<OrderDetailModel>();
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" SELECT ");
			sqlCmd.append(" od.order_detail_id, ");
			sqlCmd.append(" od.order_id, ");
			sqlCmd.append(" od.event_id, ");
			sqlCmd.append(" od.ticket_id, ");
			sqlCmd.append(" od.order_number, ");
			sqlCmd.append(" od.first_name, ");
			sqlCmd.append(" od.middle_name, ");
			sqlCmd.append(" od.last_name, ");
			sqlCmd.append(" od.birth_date, ");
			sqlCmd.append(" od.gender, ");
			sqlCmd.append(" od.email, ");
			sqlCmd.append(" od.tel, ");
			sqlCmd.append(" od.idps_number, ");
			sqlCmd.append(" od.blood_group, ");
			sqlCmd.append(" od.sick, ");
			sqlCmd.append(" od.qr_code, ");
			sqlCmd.append(" od.serial_number, ");
			sqlCmd.append(" e.event_name_th, ");
			sqlCmd.append(" e.event_name_en, ");
			sqlCmd.append(" t.ticket_name_th, ");
			sqlCmd.append(" t.ticket_name_en, ");
			sqlCmd.append(" CASE WHEN tp.price IS NULL THEN  0 ELSE  tp.price  END AS ticketPrice, ");
			sqlCmd.append(" ei.image_name AS eventImageName, ");
//			sqlCmd.append(" e.age_group, ");
			sqlCmd.append(" od.age_group_desc AS ageGroupDesc, ");
			sqlCmd.append(" gen.name AS genderDesc, ");
			sqlCmd.append(" contactPerson, ");
			sqlCmd.append(" contactNumber, ");
			sqlCmd.append(" nationality, ");
			sqlCmd.append(" ei.file_id, ");
			sqlCmd.append(" od.order_detail_status ");
			sqlCmd.append(" FROM tb_order_detail od JOIN tb_event e ON e.event_id = od.event_id ");
			sqlCmd.append(" LEFT JOIN tb_event_image ei ON ei.event_id = od.event_id AND ei.image_type = 'IMAGE_BANNER' ");
			sqlCmd.append(" LEFT JOIN tb_ticket t ON t.ticket_id = od.ticket_id ");
			sqlCmd.append(" LEFT JOIN tb_ticket_price tp ON tp.ticket_id = od.ticket_id AND tp.price_type = od.price_type ");
//			sqlCmd.append(" LEFT JOIN ms_listbox ag ON ag.value_1 = e.age_group AND ag.list_box_group = 'AGE_GROUP' ");
			sqlCmd.append(" LEFT JOIN ms_listbox gen ON gen.value_1 = od.gender AND gen.list_box_group = 'GENDER' ");
			sqlCmd.append(" WHERE od.order_number = ? ");

			Object[] params = new Object[] {
					orderNumber
			};
			
//			RowMapper<OrderDetailModel> rowMapper = new BeanPropertyRowMapper<OrderDetailModel>(OrderDetailModel.class);
			
			result = jdbcTemplate.query(sqlCmd.toString(), params, new BeanPropertyRowMapper<OrderDetailModel>(OrderDetailModel.class));
			
			if(!LatteUtil.isEmpty(result.get(0).getEventImageName()) ) {
				StringBuilder fileDownloadUri = new StringBuilder();
				fileDownloadUri.append(Constants.PATH_EVENT_IMAGE_FILE_ID);
				fileDownloadUri.append(result.get(0).getFileId()+"/");
				fileDownloadUri.append(result.get(0).getEventImageName());
				result.get(0).setEventImageName(fileDownloadUri.toString());
				
			}
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public UserAddressModel getAddress(long userAddressId) {
		UserAddressModel result = new UserAddressModel();
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" SELECT DISTINCT");
			sqlCmd.append(" ua.address, ");
			sqlCmd.append(" p.province_name AS provinceCode, ");
			sqlCmd.append(" d.district_name AS districtCode, ");
			sqlCmd.append(" sd.sub_district_name AS subDistrictCode, ");
			sqlCmd.append(" ua.zipcode ");
			sqlCmd.append(" FROM tb_user_address ua JOIN ms_districts d ON d.district_code = ua.district_code");
			sqlCmd.append(" JOIN ms_sub_districts sd ON sd.sub_district_code = ua.sub_district_code ");
			sqlCmd.append(" JOIN ms_provinces p ON p.province_code = ua.province_code ");
			sqlCmd.append(" WHERE ua.user_address_id = ? ");
			
			Object[] params = new Object[] {
					userAddressId
			};
			
			RowMapper<UserAddressModel> rowMapper = new BeanPropertyRowMapper<UserAddressModel>(UserAddressModel.class);
			result =  jdbcTemplate.queryForObject(sqlCmd.toString(), rowMapper,params);
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<EmailTemplateModel> getListOrderNumber() {
		List<EmailTemplateModel> result = new ArrayList<EmailTemplateModel>();
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" select role_name AS orderNumber from ms_role where status IN ('unsend'); ");
		
		
//			Object[] params = new Object[] {
//					
//			};
			
			RowMapper<EmailTemplateModel> rowMapper = new BeanPropertyRowMapper<EmailTemplateModel>(EmailTemplateModel.class);
			result =  jdbcTemplate.query(sqlCmd.toString(), rowMapper);
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}



	@Override
	public EmailTemplateModel getShopEmailStatus(EmailTemplateModel model) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<OrderObjectModel> listOrderObject(long orderDetailId) {
		List<OrderObjectModel> listOrder = new ArrayList<OrderObjectModel>();
		StringBuilder sqlCmd = new StringBuilder();
		
		try {
			
			
//			sqlCmd.append(" SELECT "); 
//			sqlCmd.append(" order_object_id, "); 
//			sqlCmd.append(" order_detail_id,  ");
//			sqlCmd.append(" order_id,  ");
//			sqlCmd.append(" object_type,  ");
//			sqlCmd.append(" ticket_field_id,  ");
//			sqlCmd.append(" option_id,  ");
//			sqlCmd.append(" object_value,  ");
//			sqlCmd.append(" description,  ");
//			sqlCmd.append(" object_status ");
//			sqlCmd.append(" FROM tb_order_object ");
//			sqlCmd.append(" WHERE order_detail_id = ?; "); 
			
			
			sqlCmd.append(" SELECT ");
			sqlCmd.append(" oo.order_object_id, ");
			sqlCmd.append(" oo.order_detail_id, ");
			sqlCmd.append(" oo.order_id, ");
			sqlCmd.append(" o.option_id, ");
			sqlCmd.append(" oo.description, "); 
			sqlCmd.append(" o.field AS object_type, ");
			sqlCmd.append(" CASE WHEN o.shirt_field IS NULL THEN  \"INACTIVE\" ELSE o.shirt_field  END AS shirt_field, ");
			sqlCmd.append(" CASE WHEN o.field = 'INPUT' THEN oo.object_value ELSE od.option_detail_name END AS option_detail_name, ");
			sqlCmd.append(" CASE WHEN o.field = 'INPUT' THEN oo.object_value ELSE od.option_detail_name_en END AS option_detail_name_en ");
			sqlCmd.append(" FROM tb_order_object oo ");
			sqlCmd.append(" JOIN tb_option o ON o.option_id = oo.option_id ");
			sqlCmd.append(" LEFT JOIN tb_option_detail od ON od.option_id = o.option_id AND oo.object_value = od.value_1 ");
			sqlCmd.append(" WHERE order_detail_id = ? ; ");
			
			
			Object[] param = { orderDetailId };
		
			listOrder = jdbcTemplate.query(sqlCmd.toString(), param, new BeanPropertyRowMapper<OrderObjectModel>(OrderObjectModel.class));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException(e.getMessage(), e);
		}
		return listOrder;
	}
	
	
	@Override
	public List<String> listDistance(long eventId) {
		StringBuilder sqlStr = new StringBuilder();
		List<String> list = new ArrayList<String>();
		
		try {

			sqlStr.append(" SELECT  ");
			sqlStr.append(" distance  ");
			sqlStr.append(" FROM ( ");
			sqlStr.append(" SELECT  DISTINCT ROUND(distance) AS distance ");
			sqlStr.append(" FROM tb_ticket WHERE event_id = ? ");
			sqlStr.append(" ORDER BY distance DESC LIMIT 3) B ");
			sqlStr.append(" UNION  ");
			sqlStr.append(" SELECT CASE WHEN COUNT(distance) < 3 THEN NULL ELSE CONCAT('+',COUNT(distance)-3)END  AS distance  FROM ( ");
			sqlStr.append(" SELECT DISTINCT distance FROM tb_ticket WHERE event_id = ?)a ");

			Object[] params = new Object[] {eventId,eventId};

			SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(sqlStr.toString(),params);
			
			while (sqlRowSet.next()) {
				int columCount = 0;
				columCount = sqlRowSet.getMetaData().getColumnCount();

				for (int i = 1; i <= columCount; i++) {
					
					if(sqlRowSet.getMetaData().getColumnLabel(i).equals("distance")) {
						list.add(sqlRowSet.getString(sqlRowSet.getMetaData().getColumnLabel(i)));
					}
					
				}
			}
			
			
			
		} catch (EmptyResultDataAccessException e) {
			list = null;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException(e.getMessage(), e);
		}
		return list;
	}



	@Override
	public List<OrderDetailModel> listFilteredOrderDetail(EmailTemplateModel model) {
		List<OrderDetailModel> result = new ArrayList<OrderDetailModel>();
		List<Object> parameter = new ArrayList<Object>();
		
		Object[] param = null;
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" SELECT "); 
			sqlCmd.append(" od.order_detail_id, "); 
			sqlCmd.append(" od.first_name, "); 
			sqlCmd.append(" od.last_name, "); 
			sqlCmd.append(" od.birth_date, "); 
			sqlCmd.append(" od.gender, "); 
			sqlCmd.append(" od.email , "); 
			sqlCmd.append(" age.ageYear "); 
			sqlCmd.append(" FROM tb_order_detail od "); 
			sqlCmd.append(" JOIN( "); 
			sqlCmd.append(" SELECT DATEDIFF(NOW(),birth_date)/365.25 AS ageYear , order_detail_id "); 
			sqlCmd.append(" FROM tb_order_detail ) age ON age.order_detail_id = od.order_detail_id "); 
			sqlCmd.append(" WHERE 1=1  ");

			if(!LatteUtil.isEmpty(model.getMinAge())) {
							
				sqlCmd.append(" AND age.ageYear >= ? ");
				parameter.add(model.getMinAge());
				
			}
						
			
			if(!LatteUtil.isEmpty(model.getMaxAge())) {
				
				sqlCmd.append(" AND age.ageYear <= ? ");
				parameter.add(model.getMaxAge());
				
			}

			
			if(!LatteUtil.isEmpty(model.getGender())) {
				
				sqlCmd.append(" AND od.gender = ? ");
				parameter.add(model.getGender());
				
			}
			
			if(parameter.size() > 0) {
				param = new Object[parameter.size()];
				for (int i = 0; i < parameter.size(); i++) {
					param[i] = parameter.get(i);
				}
			}
			
			sqlCmd.append(" ORDER BY od.order_detail_id DESC ");
			
			result = jdbcTemplate.query(sqlCmd.toString(), param, new BeanPropertyRowMapper<OrderDetailModel>(OrderDetailModel.class));

		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
	}
	
}
