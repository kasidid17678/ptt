package com.app.ptt.notification.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.CampaignPointModel;
import com.app.ptt.notification.model.CustomerTransLogModel;
import com.app.ptt.notification.model.LineNotiMessageModel;
import com.app.ptt.notification.model.PromotionModel;
import com.app.ptt.notification.model.ShopLineNotiModel;
import com.app.ptt.notification.model.ShopModel;
import com.app.ptt.notification.model.UserProfileModel;

@Repository
public class LineNotiDAOImpl implements LineNotiDAO{
private static Logger logger = LogManager.getLogger(LineNotiDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public LineNotiMessageModel getMessage(ShopLineNotiModel model) {
		LineNotiMessageModel lineNotiMessageModel = new LineNotiMessageModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" select m.line_noti_message_id, ");
			sqlCmd.append(" m.message_code, ");
			sqlCmd.append(" m.message_subject, ");
			sqlCmd.append(" m.message_desc, ");
			sqlCmd.append(" m.message, ");
			sqlCmd.append(" m.message_status, ");
			sqlCmd.append(" s.line_noti_status ");
			sqlCmd.append(" from tb_line_noti_message m join tb_shop_line_noti s on m.message_code = s.message_code ");
			sqlCmd.append(" where s.shop_id = ? AND s.message_code = ? ");
			
			Object[] params = new Object[] {model.getShopId(),model.getMessageCode()};
			
			lineNotiMessageModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<LineNotiMessageModel>(LineNotiMessageModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			lineNotiMessageModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return lineNotiMessageModel;
	}
//	
	@Override
	public UserProfileModel getUserProfile(int customerProfileId) {
		UserProfileModel userProfileModel = new UserProfileModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" select u.first_name, u.last_name, ");
			sqlCmd.append(" CASE WHEN u.tel IS NULL THEN '-' ELSE u.tel END AS tel , ");
			sqlCmd.append(" u.email, s.shop_name, c.update_by ");
			sqlCmd.append(" from tb_customer_profile c join tb_user u on u.user_id = c.user_id ");
			sqlCmd.append(" join tb_shop s on s.shop_id = c.shop_id ");
			sqlCmd.append(" where c.customer_profile_id = ? ");
			
			Object[] params = new Object[] {customerProfileId};
			
			userProfileModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<UserProfileModel>(UserProfileModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			userProfileModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return userProfileModel;
	}
	@Override
	public CustomerTransLogModel getPromotionDetail(int customerTransLogId) {
		CustomerTransLogModel customerTransLogModel = new CustomerTransLogModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" select p.promotion_name, l.serial_code,p.point ");
			sqlCmd.append(" from tb_customer_trans_log l join tb_promotion p on p.promotion_id = l.promotion_id ");
			sqlCmd.append(" where l.customer_trans_log_id = ? ");
			
			Object[] params = new Object[] {customerTransLogId};
			
			customerTransLogModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<CustomerTransLogModel>(CustomerTransLogModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			customerTransLogModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return customerTransLogModel;
	}
	@Override
	public CustomerTransLogModel getCampaignDetail(int customerTransLogId) {
		CustomerTransLogModel customerTransLogModel = new CustomerTransLogModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" select p.campaign_name,l.point_deposit, l.serial_code ");
			sqlCmd.append(" from tb_customer_trans_log l join tb_campaign_point p on p.campaign_point_id = l.campaign_point_id ");
			sqlCmd.append(" where l.customer_trans_log_id = ? ");
			
			Object[] params = new Object[] {customerTransLogId};
			
			customerTransLogModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<CustomerTransLogModel>(CustomerTransLogModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			customerTransLogModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return customerTransLogModel;
	}
	@Override
	public ShopModel getShopInfo(int shopId) {
		ShopModel shopModel = new ShopModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" select shop_name,qr_image,shop_banner,shop_icon ");
			sqlCmd.append(" from tb_shop  ");
			sqlCmd.append(" where shop_id = ? ");
			
			Object[] params = new Object[] { shopId};
			
			shopModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<ShopModel>(ShopModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			shopModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return shopModel;
	}
	@Override
	public PromotionModel getPromotionInfo(int proId) {
		PromotionModel proModel = new PromotionModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" select s.shop_name,p.promotion_name,p.qr_image,p.point,p.start_date,p.end_date ");
			sqlCmd.append(" from tb_promotion p join tb_shop s on s.shop_id = p.shop_id  ");
			sqlCmd.append(" where promotion_id = ? ");
			
			Object[] params = new Object[] { proId};
			
			proModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<PromotionModel>(PromotionModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			proModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return proModel;
	}
	@Override
	public CampaignPointModel getCampaignInfo(int camId) {
		CampaignPointModel camModel = new CampaignPointModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" select s.shop_name,p.campaign_name,p.qr_image,p.point ");
			sqlCmd.append(" from tb_campaign_point p join tb_shop s on s.shop_id = p.shop_id  ");
			sqlCmd.append(" where campaign_point_id = ? ");
			
			Object[] params = new Object[] { camId};
			
			camModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<CampaignPointModel>(CampaignPointModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			camModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return camModel;
	}
	@Override
	public UserProfileModel getUserInfo(int userId) {
		UserProfileModel userProfileModel = new UserProfileModel();
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" select u.first_name, u.last_name, ");
			sqlCmd.append(" CASE WHEN u.tel IS NULL THEN '-' ELSE u.tel END AS tel , ");
			sqlCmd.append(" u.email ");
			sqlCmd.append(" from tb_user u  ");
			sqlCmd.append(" where u.user_id = ? ");
			
			Object[] params = new Object[] {userId};
			
			userProfileModel = jdbcTemplate.queryForObject(sqlCmd.toString(), params, new BeanPropertyRowMapper<UserProfileModel>(UserProfileModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			userProfileModel = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return userProfileModel;
	}
}
