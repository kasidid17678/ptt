package com.app.ptt.notification.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.app.ptt.notification.exception.ServiceException;
import com.app.ptt.notification.model.EmailTemplateModel;
import com.app.ptt.notification.model.ListboxModel;
import com.app.ptt.notification.model.pttModel;

@Repository
public class CommonDAOImpl implements CommonDAO{
	
	private static Logger logger = LogManager.getLogger(CommonDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<ListboxModel> listUserShop(int userId) {
		List<ListboxModel> listBox = null;
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" SELECT s.shop_name AS name, ");
			sqlCmd.append(" s.shop_id AS value1 ");
			sqlCmd.append(" FROM tb_user_shop_admin usa "); 
			sqlCmd.append(" JOIN tb_shop s ON usa.shop_id = s.shop_id "); 
			sqlCmd.append(" WHERE usa.user_admin_id = ? ");
			
			Object[] params = new Object[] {userId};
			
			listBox = jdbcTemplate.query(sqlCmd.toString(), params, new BeanPropertyRowMapper<ListboxModel>(ListboxModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			listBox = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return listBox;
	}
	
	@Override
	public EmailTemplateModel getEmailBodyByID(String emailId) {
    	EmailTemplateModel result = new EmailTemplateModel();
		try {
		
			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" SELECT email_id, ");
			sqlCmd.append(" name AS email_name, ");
			sqlCmd.append(" subject AS subject_email, "); 
			sqlCmd.append(" body_message AS body_message_email, ");
			sqlCmd.append(" type AS email_type ");
			sqlCmd.append(" FROM ms_email ");
			sqlCmd.append(" WHERE email_id = ? ");
		
			Object[] params = new Object[] {
					emailId
			};
			
			RowMapper<EmailTemplateModel> rowMapper = new BeanPropertyRowMapper<EmailTemplateModel>(EmailTemplateModel.class);
			result =  jdbcTemplate.queryForObject(sqlCmd.toString(), rowMapper,params);
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return result;
    }

//	@Override
//	public void importShareLink(String id, String link) {
//		try {
//
//			StringBuffer sqlCmd = new StringBuffer();
//
//			sqlCmd.append(" UPDATE tmp_ptt_user SET google_drive_url = ? , status = 'DRIVE' ");
//			sqlCmd.append(" WHERE ptt_id = ? ");
//
//			Object[] params = new Object[] { link, id};
//			// RowMapper<CountryModel> rowMapper = new
//			// BeanPropertyRowMapper<CountryModel>(CountryModel.class);
//			int i = jdbcTemplate.update(sqlCmd.toString(), params);
//			// logger.info(vrId + " " + trackingNum);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	@Override
	public void importShareLink(String id, String link) {
		try {

			StringBuffer sqlCmd = new StringBuffer();

			sqlCmd.append(" UPDATE tmp_ptt_user SET google_drive_url = ? , status = 'DRIVE' ");
			sqlCmd.append(" WHERE ptt_id = ? ");

			Object[] params = new Object[] { link, id};
			// RowMapper<CountryModel> rowMapper = new
			// BeanPropertyRowMapper<CountryModel>(CountryModel.class);
			int i = jdbcTemplate.update(sqlCmd.toString(), params);
			// logger.info(vrId + " " + trackingNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<pttModel> listPtt() {
		List<pttModel> listBox = null;
		try {
			StringBuffer sqlCmd = new StringBuffer();
			
			sqlCmd.append(" SELECT id, ptt_id, first_name, last_name, email, google_drive_url, status, create_date, create_by ");
			sqlCmd.append(" FROM tmp_ptt_user "); 
			sqlCmd.append(" WHERE status = 'DRIVE' AND ");
			sqlCmd.append(" "
					+ " id IN (1766)  "
					+ " ; ");
			
			Object[] params = new Object[] {};
			
			listBox = jdbcTemplate.query(sqlCmd.toString(), params, new BeanPropertyRowMapper<pttModel>(pttModel.class));
			
		} catch (EmptyResultDataAccessException e) {
			listBox = null;
		} catch (Exception ex) {
        	throw new ServiceException(ex.getMessage(), ex);	
        }
		return listBox;
	}

	@Override
	public void clearLink() {
		StringBuilder sqlCmd = new StringBuilder();
		try {
			sqlCmd.append(" DELETE FROM tmp_ptt_user ; "); 
			  jdbcTemplate.update(sqlCmd.toString());
		} catch (EmptyResultDataAccessException e) {
			
		} catch (Exception e) {
			logger.info(e);
			throw new ServiceException(e.getMessage(), e);
		}
	}


	
}
