package com.app.ptt.notification.constants;

public class Constants {
	public static final String PATH_SHOP = "SHOP";
	public static final String PATH_PROFILE = "PROFILE";
	public static final String PATH_QR = "QR";
	
	public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";
	public static final String APPLICATION_FORM_URLENCODED_UTF8_VALUE = "application/x-www-form-urlencoded;charset=UTF-8";
	
	public static final String PROMOTION_APPLY = "0001";
	public static final String REGISTER = "0002";
	public static final String ADD_POINT = "0003";
	public static final String CAMPAIGN_NOTI = "0004";
	public static final String NEW_SHOP = "0005";
	public static final String NEW_PROMOTION = "0006";
	public static final String NEW_CAMPAIGN = "0007";
	public static final String EDIT_USER_PROFILE = "0008";
	
//	public static final String SERVER_URL = "http://localhost:9800";
//	public static final String SERVER_URL = "http://localhost:8003";
	public static final String SERVER_URL = "https://checkrace.com/";
	public static final String LINE_URL = "https://liff.line.me/1653812660-g7xexymE?";
	public static final String PARAM_SHOP_ID = "shopId=";
	public static final String PARAM_PROMOTION_ID = "promotionId=";
	public static final String PARAM_CAMPAIGN_ID = "campaignId=";
	
	
	public static final String DOWNLOAD_FILE_CONTROLLER = "/file/downloadFileImage/";
//	public static final String MAIL_USERNAME = "info@checkrace.com";
//	public static final String MAIL_PASSWORD = "jdjpauvuwtwntqti";
	public static final String MAIL_USERNAME = "orhappyannivesary@gmail.com";
	public static final String MAIL_PASSWORD = "beavbvhwxygqxepi";
	
	public static final String LINE_BOT_TOKEN = "fjUSckNvRwiIv83U1wmQDU/uWB2KZcDrrorhqSVjQz2bmYfEsSxckSEv7THCdcpRKGg1oavJVm9PNBDD1ZPkfru2Xwjh6IEyU6RkI+OQ0BTvHPBm/eNqn/Nw9mLLigkXX/VXGazeUidqCKy/nfACnQdB04t89/1O/w1cDnyilFU=";
	public static final String LINE_MESSAGE_API_ACCESS_TOKEN = "9LrlkCAP7KNJTdOc8fcNJlw4ibAhkRaxA5nB348feDMdn4NWA3dDDfDj4S1/vpFb6PTrbBJMMUNW6DYZ1vI4teR9+TPbb+TbBaynYkYvQ8m07eYXsU0pASaVsEHHbEV5+GbhmchE4+WAhntffnYO8wdB04t89/1O/w1cDnyilFU=";
	
	public static final String REMIND_PAYMENT = "1";
	public static final String AFTER_PAYMENT = "2";
	public static final String RESET_PASSWORD = "3";
	public static final String ADVERTISE = "4";
	public static final String PTT = "5";
	
//	public static final String URL_IMAGE = "https://checkrace.com/file/downloadFileImage/";
	public static final String URL_IMAGE = "ttps://race.checkrace.com/file/downloadFile/";
	
	public static final String URL_IMAGE_QR = "ttps://race.checkrace.com/file/downloadFileQR/";
	
	public static final String INACTIVE = "INACTIVE";
	public static final String ACTIVE = "ACTIVE";
	
	
	public static final String PATH_EVENT_IMAGE_FILE_ID = "https://race.checkrace.com/file/downloadFile/";// TODO: <fileId>/<imageName>

	public static final String URL_VISIT = "https://race.checkrace.com/";
	public static final String URL_USER_ORDER = "https://race.checkrace.com/user-order";
	
	public static final String ORDER_STATUS_PAID = "PAID";
	public static final String ORDER_STATUS_PENDING_PAYMENT = "PENDING_PAYMENT";
	
	public static final String SHIPPING_CONTENT_ACTIVE = "จัดส่งตามที่อยู่";
	public static final String SHIPPING_CONTENT_INACTIVE = "รับด้วยตัวเอง ณ";

	public static final String IMAGE_BANNER = "IMAGE_BANNER";
	
	public static final String MALE = "MALE";
	public static final String FEMALE = "FEMALE";
	
}
