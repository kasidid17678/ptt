package com.app.ptt.notification.redis;

import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserOrderRedisService {

	private final String TB_USER_ORDER = "TB_USER_ORDER";
	 
	 @Autowired
	    RedisTemplate<String, Object> redisTemplate;
	    private HashOperations<String, String, String> hashOperations;
	 
	    
	    @PostConstruct
	    private void intializeHashOperations() {
	        hashOperations = redisTemplate.opsForHash();
	    }
	    
	    public void save(String key, String order) {
	        hashOperations.put(TB_USER_ORDER, key, order);
	    }
		
	    public String findById(String key) {
	        return (String) hashOperations.get(TB_USER_ORDER, key);
	    }
	    
	    public Map<String, String> findAll() {
	        return hashOperations.entries(TB_USER_ORDER);
	    }
	    
	    public void delete(String key) {
	        hashOperations.delete(TB_USER_ORDER, key);
	    }
	    
	    public Set<String> hashGetAllKey() {
	    	return hashOperations.keys(TB_USER_ORDER);
	    }
	    
}
