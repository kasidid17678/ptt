package com.app.ptt.notification.redis;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class EventRedis {

	private final String TB_EVENT = "TB_EVENT";
	 
    @Autowired
    RedisTemplate<String, Object> redisTemplate;
    private HashOperations<String, String, String> hashOperations;
 
    
    @PostConstruct
    private void intializeHashOperations() {
        hashOperations = redisTemplate.opsForHash();
    }
    
    public void save(String eventUrl, String event) {
        hashOperations.put(TB_EVENT, eventUrl, event);
    }
	
    public String findById(String eventUrl) {
        return (String) hashOperations.get(TB_EVENT, eventUrl);
    }
    
    public Map<String, String> findAll() {
        return hashOperations.entries(TB_EVENT);
    }
    
    public void delete(String eventUrl) {
        hashOperations.delete(TB_EVENT, eventUrl);
    }
}
